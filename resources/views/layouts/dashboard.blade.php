<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Dental') }}</title>

    <!-- Fonts and icons -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />

    <!-- CSS Files -->
    <link href="{{ asset('vendor/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/datetimepicker.css') }}" rel="stylesheet" />

    @yield('stylesheets')

    <link href="{{ asset('css/theme.css') }}" rel="stylesheet" />
</head>

<body>
    <div class="wrapper">

        @section('sidebar')
            <div class="sidebar" data-color="blue" data-image="https://demos.creative-tim.com/light-bootstrap-dashboard-pro/assets/img/sidebar-5.jpg">
            <!--
            Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

            Tip 2: you can also add an image using data-image tag
            -->
            <div class="sidebar-wrapper">
                <div class="logo" style="margin-left: -22px;">
                    <a href="https://www.tigdesignsolutions.com/" target="_blank" class="logo-mini">
                        <img src="{{asset('images/clinicTIGIcon.png')}}" width="90" height="85"/>
                    </a></br></br></br>
                </div>
                <div class="user">
                    <div class="photo">
                        <img src="https://cdn.pixabay.com/photo/2020/04/15/17/32/medical-5047586_960_720.png" />
                    </div>
                    <div class="info ">
                        <a data-toggle="collapse" href="#" class="collapsed">
                                <span>{{ auth()->user()->name }}
                                    {{--<b class="caret"></b>--}}
                                </span>
                        </a>
                        {{--<div class="collapse" id="collapseExample">--}}
                            {{--<ul class="nav">--}}
                                {{--<li>--}}
                                    {{--<a class="profile-dropdown" href="#pablo">--}}
                                        {{--<span class="sidebar-mini">MP</span>--}}
                                        {{--<span class="sidebar-normal">My Profile</span>--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<a class="profile-dropdown" href="#pablo">--}}
                                        {{--<span class="sidebar-mini">EP</span>--}}
                                        {{--<span class="sidebar-normal">Edit Profile</span>--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<a class="profile-dropdown" href="#pablo">--}}
                                        {{--<span class="sidebar-mini">S</span>--}}
                                        {{--<span class="sidebar-normal">Settings</span>--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                    </div>
                </div>
                <ul class="nav">
                    {{--<li class="nav-item ">--}}
                        {{--<a class="nav-link" href="{{ url("/home") }}">--}}
                            {{--<i class="nc-icon nc-chart-pie-35"></i>--}}
                            {{--<p>Home</p>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                    <li class="nav-item {{request()->routeIs('appointments.create') ? 'active' : ''}}">
                        <a class="nav-link" href="{{ url('appointments/patientlist') }}">
                            <i class="nc-icon nc-single-copy-04"></i>
                            <p>Book an Appointment</p>
                        </a>
                    </li>
                    <li class="nav-item {{request()->routeIs('appointments.index') ? 'active' : ''}}">
                        <a class="nav-link" href="{{ url('appointments/index') }}">
                            <i class="nc-icon nc-watch-time"></i>
                            <p>Appointments</p>
                        </a>
                    </li>
                    <li class="nav-item {{request()->routeIs('appointments.calendar') ? 'active' : ''}}">
                        <a class="nav-link" href="{{ url('appointments/calendar') }}">
                            <i class="nc-icon nc-single-copy-04"></i>
                            <p>Calendar</p>
                        </a>
                    </li>
                    @if(auth()->user()->isAdministrator() || auth()->user()->isSuperUser())
                        <li class="nav-item {{request()->routeIs('appointments.patients') ? 'active' : ''}}">
                            <a class="nav-link" href="{{ url('appointments/patients') }}">
                                <i class="nc-icon nc-notes"></i>
                                <p>Patients</p>
                            </a>
                        </li>
                        @if(auth()->user()->isSuperUser())
                        <li class="nav-item {{request()->routeIs('appointments.superuser') ? 'active' : ''}}">
                            <a class="nav-link" href="{{ url('appointments/superuser') }}">
                                <i class="nc-icon nc-single-02"></i>
                                <p>User-Role</p>
                            </a>
                        </li>
                        @endif
                        <li class="nav-item">
                            <a class="nav-link"  href="http://medical.tigdesignsolutions.com/" target="_blank">
                                <i class="nc-icon nc-tv-2"></i>
                                <p>Demo</p>
                            </a>
                        </li>
                    @else
                        @if(request()->routeIs('appointments.create') || request()->route()->getName('appointment.index'))
                            <div class="container" style="background-color: #ccffff">
                                <div class="text-danger font-weight-bold text-center"> IMPORTANT REMINDERS </div>
                                <p></p>
                                <div class="text-dark" style="font-size: 12px; text-align:justify;">
                                    <strong><b>1.</b> Please fill-out all the required fields in the Patient Details.</strong>
                                </div>
                                <p></p>
                                <div class="text-dark" style="font-size: 12px; text-align:justify;">
                                    <strong><b>2.</b> Please describe the purpose/reason of your visit/consultation as clearly and completely  as possible.</strong>
                                </div>
                                <p></p>
                                <div class="text-dark" style="font-size: 12px; text-align:justify;">
                                    <strong><b>3.</b> Please sign in and fill the PATIENT’s SCREENING FORM if you wish to have a personal visit at the doctor’s clinic.  ONLY the PATIENT is allowed inside the doctor’s clinic.</strong>
                                </div>
                                <p></p>

                                <p><div class="text-dark" style="font-size: 13px; text-align:justify;"> <strong>Thank you very much for your very kind COOPERATION on this matter of preventive measures.</strong></div></p>
                                <p><div class="text-dark text-center" style="font-size: 15px"> <strong>Your <code>SAFETY</code> is our <code>PRIORITY</code></strong></div></p>
                            </div>
                        @endif
                    @endif
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" data-toggle="collapse" href="#records">--}}
                            {{--<i class="nc-icon nc-notes"></i>--}}
                            {{--<p>--}}
                                {{--Records--}}
                                {{--<b class="caret"></b>--}}
                            {{--</p>--}}
                        {{--</a>--}}
                        {{--<div class="collapse " id="records">--}}
                            {{--<ul class="nav">--}}
                                {{--<li class="nav-item ">--}}
                                    {{--<a class="nav-link" href="{{ url("/home") }}">--}}
                                        {{--<span class="sidebar-mini">Pt.</span>--}}
                                        {{--<span class="sidebar-normal">Patients</span>--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<li class="nav-item ">--}}
                                    {{--<a class="nav-link" href="{{ url("/home") }}">--}}
                                        {{--<span class="sidebar-mini">Dr.</span>--}}
                                        {{--<span class="sidebar-normal">Doctors</span>--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item ">--}}
                        {{--<a class="nav-link" href="{{ url("/home") }}">--}}
                            {{--<i class="nc-icon nc-credit-card"></i>--}}
                            {{--<p>Billing</p>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item ">--}}
                        {{--<a class="nav-link" href="{{ url("/home") }}">--}}
                            {{--<i class="nc-icon nc-layers-3"></i>--}}
                            {{--<p>Supplies Inventory</p>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item ">--}}
                        {{--<a class="nav-link" href="{{ url("/home") }}">--}}
                            {{--<i class="nc-icon nc-chat-round"></i>--}}
                            {{--<p>Messages</p>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                </ul>
            </div>
        </div>
        @show

        <div class="main-panel">
            @section('navbar')
                <nav class="navbar navbar-expand-lg ">
                <div class="container-fluid">
                    <div class="navbar-wrapper">
                        <div class="navbar-minimize">
                            <button id="minimizeSidebar" class="btn btn-primary btn-fill btn-round btn-icon d-none d-lg-block">
                                <i class="fa fa-ellipsis-v visible-on-sidebar-regular"></i>
                                <i class="fa fa-navicon visible-on-sidebar-mini"></i>
                            </button>
                        </div>
                        <a class="navbar-brand" href="#"> {{ isset($page) ? $page : '' }} </a>
                    </div>
                    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-bar burger-lines"></span>
                        <span class="navbar-toggler-bar burger-lines"></span>
                        <span class="navbar-toggler-bar burger-lines"></span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-end">
                        {{--<ul class="nav navbar-nav mr-auto">--}}
                            {{--<form class="navbar-form navbar-left navbar-search-form" role="search">--}}
                                {{--<div class="input-group">--}}
                                    {{--<i class="nc-icon nc-zoom-split"></i>--}}
                                    {{--<input type="text" value="" class="form-control" placeholder="Search...">--}}
                                {{--</div>--}}
                            {{--</form>--}}
                        {{--</ul>--}}
                        {{--<ul class="navbar-nav">--}}
                            {{--<li class="nav-item">--}}
                                {{--<a href="appointments/create" class="btn btn-success btn-wd">New Appointment</a>--}}
                            {{--</li>--}}
                            {{--<li class="nav-item">--}}
                                {{--<a href="https://example.com/" class="btn btn-default btn-wd">Block Time</a>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                        <ul class="navbar-nav">
                            {{--<li class="dropdown nav-item">--}}
                                {{--<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">--}}
                                    {{--<i class="nc-icon nc-planet"></i>--}}
                                {{--</a>--}}
                                {{--<ul class="dropdown-menu dropdown-menu-right">--}}
                                    {{--<a class="dropdown-item" href="{{ url("/appointments/create") }}">New Appointment</a>--}}
                                    {{--<a class="dropdown-item" href="{{ url("/schedules/block") }}">Block Time</a>--}}
                                {{--</ul>--}}
                            {{--</li>--}}
                            {{--<li class="dropdown nav-item">--}}
                                {{--<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">--}}
                                    {{--<i class="nc-icon nc-bell-55"></i>--}}
                                    {{--<span class="notification">5</span>--}}
                                    {{--<span class="d-lg-none">Notification</span>--}}
                                {{--</a>--}}
                                {{--<ul class="dropdown-menu">--}}
                                    {{--<a class="dropdown-item" href="#">Notification 1</a>--}}
                                    {{--<a class="dropdown-item" href="#">Notification 2</a>--}}
                                    {{--<a class="dropdown-item" href="#">Notification 3</a>--}}
                                    {{--<a class="dropdown-item" href="#">Notification 4</a>--}}
                                    {{--<a class="dropdown-item" href="#">Notification 5</a>--}}
                                {{--</ul>--}}
                            {{--</li>--}}
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="https://example.com/" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="nc-icon nc-bullet-list-67"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                    {{--<a class="dropdown-item" href="#">--}}
                                        {{--<i class="nc-icon nc-email-85"></i> Messages--}}
                                    {{--</a>--}}
                                    {{--<a class="dropdown-item" href="#">--}}
                                        {{--<i class="nc-icon nc-umbrella-13"></i> Help Center--}}
                                    {{--</a>--}}
                                    {{--<a class="dropdown-item" href="#">--}}
                                        {{--<i class="nc-icon nc-settings-90"></i> Settings--}}
                                    {{--</a>--}}
                                    <a class="dropdown-item" href="{{ url('/appointments/create') }}">Book an Appointment</a>
                                    <a class="dropdown-item" href="{{ url ('appointments/screeningform') }}">COVID-19 Screening Form</a>
                                    {{--<a class="dropdown-item" href="{{ url ('appointments/consentform') }}">Dental Consent Form</a>--}}
                                    <a class="dropdown-item" href="{{ url ('/changePassword') }}">Change Password</a>
                                    <div class="divider"></div>
                                    <a class="dropdown-item text-danger" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i class="nc-icon nc-button-power"></i> {{ __('Log out') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            @show

            @yield('content')

            @section('footer')
                <footer class="footer">
                    <div class="container">
                        <nav>
                            <ul class="footer-menu">

                            </ul>
                            <p class="copyright text-center">
                                ©
                                <script>
                                    document.write(new Date().getFullYear())
                                </script>
                                <a href="https://www.tigdesignsolutions.com/" target="_blank">TIG Design and Solutions</a>
                            </p>
                        </nav>
                    </div>
                </footer>
            @show
        </div>
    </div>
</body>

<!--   Core JS Files   -->
<script src="{{ asset('vendor/jquery/dist/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendor/popper/popper.js/dist/umd/popper.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendor/bootstrap/dist/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendor/moment/moment.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/datetimepicker.js') }}" type="text/javascript"></script>

@yield('javascript')

<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="{{ asset('js/theme.js') }}" type="text/javascript"></script>
</html>
