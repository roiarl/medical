@extends('layouts.app')

@section('content')
<div class="wrapper wrapper-full-page">
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute">
        <div class="container">
            <div class="navbar-collapse justify-content-end" id="navbar">
                <ul class="navbar-nav">
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->
    <!--   you can change the color of the filter page using: data-color="blue | azure | green | orange | red | purple" -->
    <div class="full-page register-page section-image" data-color="blue" data-image="https://demos.creative-tim.com/light-bootstrap-dashboard-pro/assets/img/bg5.jpg">
        <div class="content">
            <div class="container">
                <div class="card card-register card-plain">
                    <div class="card-header ">
                        <div class="row  justify-content-center">
                            <div class="col-md-8 text-center">
                                <div class="header-text">
                                    <h2 class="card-title">Change Password</h2>
                                    <hr />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-md-5 ml-auto">
                                <div class="media">
                                    <div class="media-left">
                                        <div class="icon">
                                            <i class="nc-icon nc-badge"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <h4>{{ env('APP_OWNER') }}</h4>
                                        <table class="table table-borderless text-white table-condensed">
                                            <tbody >
                                                <?php
                                                    $titles = explode(',',env('OWNER_TITLE'));
                                                    $contacts = explode(',',env('OWNER_CONTACT'));
                                                ?>
                                                @foreach ($titles as $title)
                                                <tr>
                                                    <td style="font-size:17px">{{ $title }}</td>
                                                </tr>
                                                @endforeach
                                                @foreach ($contacts as $contact)
                                                <tr>
                                                    <td style="font-size:15px">{{ $contact }}</td>
                                                </tr>
                                                @endforeach
                                                <tr>
                                                    <td style="font-size:17px"><a href="mailto:dentalcares@tigdental.com">{{ env('OWNER_EMAIL') }}</a></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left">
                                        <div class="icon">
                                            <i class="nc-icon nc-settings-90"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <h4>Services Offered</h4>

                                        <?php $services = explode(',',env('OWNER_SERVICE_OFFER'));?>
                                        <ul>
                                            @foreach ($services as $service)
                                            <li>{{ $service }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left">
                                        <div class="icon">
                                            <i class="nc-icon nc-watch-time"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <h4>Clinic Schedules</h4>
                                        <table class="table table-borderless text-white table-condensed">
                                            <tbody>
                                            <tr>
                                                <td>Monday</td>
                                                <td>{{ env('OWNER_MON_SCHED') }}</td>
                                            </tr>
                                            <tr>
                                                <td>Tuesday</td>
                                                <td>{{ env('OWNER_TUE_SCHED') }}</td>
                                            </tr>
                                            <tr>
                                                <td>Wednesday</td>
                                                <td>{{ env('OWNER_WED_SCHED') }}</td>
                                            </tr>
                                            <tr>
                                                <td>Thursday</td>
                                                <td>{{ env('OWNER_THU_SCHED') }}</td>
                                            </tr>
                                            <tr>
                                                <td>Friday</td>
                                                <td>{{ env('OWNER_FRI_SCHED') }}</td>
                                            </tr>
                                            <tr>
                                                <td>Saturday</td>
                                                <td>{{ env('OWNER_SAT_SCHED') }} <br> {{ env('OWNER_SAT_SCHED_EXT') }}</td>
                                            </tr>
                                            <tr>
                                                <td>Sunday</td>
                                                <td>{{ env('OWNER_SUN_SCHED') }}</td>
                                            </tr>
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left">
                                        <div class="icon">
                                            <i class="nc-icon nc-square-pin"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <h4>Clinic Address</h4>

                                        <?php $addresses = explode('/',env('OWNER_CLINIC_ADDRESS'));?>
                                        <div class="ml-3 ">
                                            @foreach ($addresses as $address)
                                            {{ $address }} <br>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 mr-auto">
                                <form method="POST" action="{{ route('changePassword') }}">
                                    {{ csrf_field() }}

                                    <div class="card card-plain">
                                        <div class="content">
                                            @if (session('error'))
                                                <div class="alert alert-danger">
                                                    {{ session('error') }}
                                                </div>
                                            @endif
                                            @if (session('success'))
                                                <div class="alert alert-success">
                                                    {{ session('success') }}
                                                </div>
                                            @endif
                                            <div class="form-group{{ $errors->has('current-password') ? ' has-error' : '' }}">
                                                <input id="current-password" placeholder="Current Password" type="password" class="form-control" name="current-password" required>

                                                @if ($errors->has('current-password'))
                                                    <span class="help-block">
                                                        <strong style="color: red;">{{ $errors->first('current-password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>

                                            <div class="form-group{{ $errors->has('new-password') ? ' has-error' : '' }}">
                                                <input id="new-password" placeholder="New Password" type="password" class="form-control" name="new-password" required>

                                                @if ($errors->has('new-password'))
                                                    <span class="help-block">
                                                        <strong style="color: red;">{{ $errors->first('new-password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>

                                            <div class="form-group">
                                                <input id="new-password-confirm" placeholder="Confirm New Password" type="password" class="form-control" name="new-password_confirmation" required>
                                            </div>
                                        </div>
                                        <div class="footer text-center">
                                            <button type="submit" class="btn btn-primary">Change Password</button>
                                            @if (session('success'))
                                                <a href="{{ url('appointments/create') }}" class="btn btn-fill btn-success">Done</a>
                                            @else
                                                <a href="{{ url('appointments/create') }}" class="btn btn-fill btn-danger">Cancel</a>
                                            @endif
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer"></footer>
</div>
@endsection
