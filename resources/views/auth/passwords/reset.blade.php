@extends('layouts.app')

@section('content')
{{--<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>--}}
<div class="wrapper wrapper-full-page">
    <div class="full-page section-image" data-color="blue" data-image="https://demos.creative-tim.com/light-bootstrap-dashboard-pro/assets/img/full-screen-image-2.jpg" ;>
        <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
        <div class="content">
            <div class="container">
                <div class="card card-register card-plain">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-md-5 ml-auto">
                                <div class="media">
                                    <div class="media-left">
                                        <div class="icon">
                                            <i class="nc-icon nc-badge"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <h4>{{ env('APP_OWNER') }}</h4>
                                        <table class="table table-borderless text-white table-condensed">
                                            <tbody >
                                                <?php
                                                    $titles = explode(',',env('OWNER_TITLE'));
                                                    $contacts = explode(',',env('OWNER_CONTACT'));
                                                ?>
                                                @foreach ($titles as $title)
                                                <tr>
                                                    <td style="font-size:17px">{{ $title }}</td>
                                                </tr>
                                                @endforeach
                                                @foreach ($contacts as $contact)
                                                <tr>
                                                    <td style="font-size:15px">{{ $contact }}</td>
                                                </tr>
                                                @endforeach
                                                <tr>
                                                    <td style="font-size:17px"><a href="mailto:dentalcares@tigdental.com">{{ env('OWNER_EMAIL') }}</a></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left">
                                        <div class="icon">
                                            <i class="nc-icon nc-settings-90"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <h4>Services Offered</h4>

                                        <?php $services = explode(',',env('OWNER_SERVICE_OFFER'));?>
                                        <ul>
                                            @foreach ($services as $service)
                                            <li>{{ $service }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left">
                                        <div class="icon">
                                            <i class="nc-icon nc-watch-time"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <h4>Clinic Schedules</h4>
                                        <table class="table table-borderless text-white table-condensed">
                                            <tbody>
                                                <tr>
                                                    <td>Monday</td>
                                                    <td>{{ env('OWNER_MON_SCHED') }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Tuesday</td>
                                                    <td>{{ env('OWNER_TUE_SCHED') }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Wednesday</td>
                                                    <td>{{ env('OWNER_WED_SCHED') }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Thursday</td>
                                                    <td>{{ env('OWNER_THU_SCHED') }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Friday</td>
                                                    <td>{{ env('OWNER_FRI_SCHED') }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Saturday</td>
                                                    <td>{{ env('OWNER_SAT_SCHED') }} <br> {{ env('OWNER_SAT_SCHED_EXT') }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Sunday</td>
                                                    <td>{{ env('OWNER_SUN_SCHED') }}</td>
                                                </tr>
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left">
                                        <div class="icon">
                                            <i class="nc-icon nc-square-pin"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <h4>Clinic Address</h4>

                                        <?php $addresses = explode('/',env('OWNER_CLINIC_ADDRESS'));?>
                                        <div class="ml-3 ">
                                            @foreach ($addresses as $address)
                                            {{ $address }} <br>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 mr-auto">
                                <form method="POST" action="{{ route('password.update') }}">
                                    @csrf

                                    <div class="card card-login">
                                        <div class="card-header ">
                                            <h3 class="header text-center">
                                                <img src="{{asset('images/clinicTIGIcon.png')}}" width="80" height="80"/>
                                            </h3>
                                        </div>

                                        <input type="hidden" name="token" value="{{ $token }}">

                                        <div class="card-body">
                                            <div class="form-group">
                                                <label for="email">{{ __('E-Mail Address') }}</label>
                                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                                                @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label for="password">{{ __('Password') }}</label>
                                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                                @error('password')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label for="password-confirm">{{ __('Confirm Password') }}</label>
                                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                            </div>
                                        </div>
                                        <div class="card-footer ml-auto mr-auto">
                                            <button type="submit" class="btn btn-primary">{{ __('Reset Password') }}</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer"></footer>
</div>
@endsection
