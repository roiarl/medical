@extends('layouts.dashboard')

<style>
    tr[data-href] {
        cursor: pointer;
    }
</style>

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">

                @if(session()->has('message'))
                    <div class="alert alert-success">
                        <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                            <i class="nc-icon nc-simple-remove"></i>
                        </button>
                        <span>
                            <b>
                                {{ session('message') }}
                            </b>
                        </span>
                    </div>
                @endif
                    @if($prescriptions->count() > 0)
                        <div class="card table-with-links">
                            @if(session()->has('createprescription'))
                                <div class="alert alert-success">
                                    <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                                        <i class="nc-icon nc-simple-remove"></i>
                                    </button>
                                    <span>
                                        <b>
                                            {{ session('createprescription') }}
                                        </b>
                                    </span>
                                </div>
                            @endif
                            <div class="card-header text-center" style="background-color:blue">
                                <h4 class="card-title text-light pb-2" ><strong>Medical Prescription History<strong></h4>
                            </div>
                            <div class="card-body table-responsive">

                                <table class="table table-hover table-striped table-bordered">
                                    <thead>
                                        <tr class="success d-flex">
                                        <th class="text-center col-1">No.</th>
                                        <th class="text-center col-2">Date</th>
                                        <th class="text-center col-6">RX - Doctor's Prescription</th>
                                        <th class="text-center col-3">Sig</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($prescriptions as $key => $prescription)
                                            <tr class="d-flex" data-href="{{ url('appointments/showprescription', ['id' => $prescription->id, 'referer' => $referer]) }}">
                                                <td class="text-center col-1">{{ $key + 1 }}</td>
                                                <td class="text-center col-2">{{ date('M-d-Y', strtotime($prescription->date)) }} </td>
                                                <td class="col-6">{{ $prescription->rx }} </td>
                                                <td class="col-3">{{ $prescription->sig }} </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            @if(!auth()->user()->isAdministrator() && !auth()->user()->isSuperUser())
                            <div class="card-footer">
                                @if($referer == "index")
                                    <a href="{{ url('appointments/index') }}" class="btn btn-fill btn-default">Back</a>
                                @else
                                    <a href="{{ url('appointments/patients') }}" class="btn btn-fill btn-default">Back</a>
                                @endif
                            </div>
                            @else
                            <div class="card-footer">
                                <a href="{{ url ('appointments/prescriptionform', ['id' => $prescription->patient->id, 'referer' => $referer]) }}" class="btn btn-fill btn-success">Create</a>
                            </div>
                            @endif
                        </div>
                    @else
                        <div class="card">
                            <div class="card-header text-center" style="background-color:blue">
                                <h4 class="card-title text-light pb-2" ><strong>Medical Prescription History<strong></h4>
                            </div>
                            <div class="card-body">
                                <strong>No Medical Prescriptions</strong>
                                @if(auth()->user()->isAdministrator() || auth()->user()->isSuperUser())
                                <p>Click <a href="{{ url('appointments/prescriptionform', ['id' => $patient->id, 'referer' => $referer]) }}"><strong>HERE</strong></a> to create medical prescription.</p>
                                @endif
                            </div>
                            @if(!auth()->user()->isAdministrator() && !auth()->user()->isSuperUser())
                            <div class="card-footer justify-content-right">
                                @if($referer == "index")
                                    <a href="{{ url('appointments/index') }}" class="btn btn-fill btn-default">Back</a>
                                @else
                                    <a href="{{ url('appointments/patients') }}" class="btn btn-fill btn-default">Back</a>
                                @endif
                            </div>
                            @endif
                        </div>
                    @endif
                    
                    @if(auth()->user()->isAdministrator() || auth()->user()->isSuperUser())
                        @if($patientRecords->count() > 0)
                            <div class="card table-with-links">
                            @if(session()->has('createpatientrecord'))
                                <div class="alert alert-success">
                                    <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                                        <i class="nc-icon nc-simple-remove"></i>
                                    </button>
                                    <span>
                                        <b>
                                            {{ session('createpatientrecord') }}
                                        </b>
                                    </span>
                                </div>
                            @endif
                                <div class="card-header text-center" style="background-color:blue">
                                    <h4 class="card-title text-light pb-2" ><strong>Patient Record<strong></h4>
                                </div>
                                <div class="card-body table-responsive">

                                <table class="table table-hover table-striped table-bordered">
                                    <thead>
                                        <tr class="success d-flex">
                                        <th class="text-center col-1">No.</th>
                                        <th class="text-center col-2">Date</th>
                                        <th class="text-center col-3">Reason/Purpose of Visit</th>
                                        <th class="text-center col 6">Doctor's Notes</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($patientRecords as $key => $patientRecord)
                                            <tr class="d-flex" data-href="{{ url('appointments/showpatientrecord', ['id' => $patientRecord->id, 'referer' => $referer]) }}">
                                                <td class="text-center col-1">{{ $key + 1 }}</td>
                                                <td class="text-center col-2"> {{ date('M-d-Y', strtotime($patientRecord->date)) }} </td>
                                                <td class="col-3"> {{ $patientRecord->reason }} </td>
                                                <td class="col-6"> {{ $patientRecord->notes }} </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                </div>
                                @if(auth()->user()->isAdministrator() || auth()->user()->isSuperUser())
                                    <div class="card-footer justify-content-right">
                                        <a href="{{ url('appointments/patientrecord', ['id' => $patient->id, 'referer' => $referer]) }}" class="btn btn-fill btn-success">Create</a>
                                        @if($referer == "index")
                                            <a href="{{ url('appointments/index') }}" class="btn btn-fill btn-default">Back</a>
                                        @else
                                            <a href="{{ url('appointments/patients') }}" class="btn btn-fill btn-default">Back</a>
                                        @endif
                                    </div>
                                @else
                                    <div class="card-footer justify-content-right">
                                        @if($referer == "index")
                                            <a href="{{ url('appointments/index') }}" class="btn btn-fill btn-default">Back</a>
                                        @else
                                            <a href="{{ url('appointments/patients') }}" class="btn btn-fill btn-default">Back</a>
                                        @endif
                                    </div>
                                @endif
                            </div>
                        @else
                            <div class="card">
                                <div class="card-header text-center" style="background-color:blue">
                                    <h4 class="card-title text-light pb-2" ><strong>Patient Record<strong></h4>
                                </div>
                                <div class="card-body">
                                    <strong>No Patient Record</strong>
                                    @if(auth()->user()->isAdministrator() || auth()->user()->isSuperUser())
                                    <p>Click <a href="{{ url('appointments/patientrecord', ['id' => $patient->id, 'referer' => $referer]) }}"><strong>HERE</strong></a> to create patient record.</p>
                                    @endif
                                </div>
                                <div class="card-footer">
                                    @if($referer == "index")
                                        <a href="{{ url('appointments/index') }}" class="btn btn-fill btn-default">Back</a>
                                    @else
                                        <a href="{{ url('appointments/patients') }}" class="btn btn-fill btn-default">Back</a>
                                    @endif
                                </div>
                            </div>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection


@section('javascript')
    <script type="text/javascript">
    jQuery.fn.extend({
      autoHeight: function () {
        function autoHeight_(element) {
          return jQuery(element).css({
            'height': 'auto',
            'overflow-y': 'hidden'
          }).height(element.scrollHeight);
        }
        return this.each(function () {
          autoHeight_(this).on('input', function () {
            autoHeight_(this);
          });
        });
      }
    });
    $('#covid_form').autoHeight();

    document.addEventListener("DOMContentLoaded", () => {
        const rows = document.querySelectorAll("tr[data-href]");

        rows.forEach(row => {
            row.addEventListener("click", () => {
                window.location.href = row.dataset.href;
            });
        });
    });
    </script>
@endsection