@extends('layouts.dashboard')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                                <i class="nc-icon nc-simple-remove"></i>
                            </button>
                            <span>
                                <b>
                                    {{ session('message') }}
                                </b>
                            </span>
                        </div>
                    @endif
                    <form method="GET" action="{{ url('appointments/createprescription',['referer' => $referer]) }}">
                        @csrf
                        <input type="hidden" id="birthdate" name="birthdate" value="{{ $appointment->patient->birthday }}">
                        <input type="hidden" name="id" value="{{ $appointment->patient->id }}">
                        <div class="card stacked-form">
                            <div class="card-header text-center" style="background-color:purple">
                                <h4 class="card-title text-light pb-2" ><strong>Medical Prescription<strong></h4>
                            </div>
                            <div class="card-body ">
                                <div class="row">
                                    @if($doctors->count() > 0)
                                        @foreach($doctors as $doctor)
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label class="text-dark"><strong>Doctor's Name</strong> </label>
                                                    <input type="text" placeholder="Doctor's Name" name="doctor_name" value="{{ old('doctor_name', $doctor->name ?? '') }}" class="form-control @error('doctor_name') is-invalid @enderror" readonly>
                                                    @error('doctor_name')
                                                    <p class="text-danger">{{ $message }}</p>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label class="text-dark"><strong>License No.</strong> </label>
                                                    <input type="number" placeholder="License No." name="license_no" value="{{ old('license_no', $doctor->license ?? '') }}" class="form-control @error('license_no') is-invalid @enderror" readonly>
                                                    @error('license_no')
                                                    <p class="text-danger">{{ $message }}</p>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-3"></div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="text-dark"><strong>Signature</strong> </label>
                                                    <!-- <input type="text" placeholder="Signature" name="signature" value="{{ old('signature', $doctor->signature ?? '') }}" class="form-control"> -->
                                                    <img src="{{ asset('storage/'.$doctor->signature) }}" alt="" class="img-thumbnail">
                                                </div>
                                            </div>
                                        @endforeach
                                    @else
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label class="text-dark"><strong>Doctor's Name</strong> </label>
                                                <input type="text" placeholder="Doctor's Name" name="doctor_name" class="form-control @error('doctor_name') is-invalid @enderror" disabled>
                                                @error('doctor_name')
                                                <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-dark"><strong>License No.</strong> </label>
                                                <input type="number" placeholder="License No." name="license_no" class="form-control @error('license_no') is-invalid @enderror" disabled>
                                                @error('license_no')
                                                <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="text-dark"><strong>Signature</strong> </label>
                                                <input type="text" placeholder="Signature" name="signature" class="form-control" disabled>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            
                            <div class="card-body ">
                                <hr style="border: 3px solid green;border-radius: 5px;">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-dark"><strong>First Name</strong> </label>
                                            <input type="text" placeholder="First Name" name="first_name" value="{{ old('first_name', $appointment->patient->first_name) }}" class="form-control @error('first_name') is-invalid @enderror" readonly <?php echo ((app('request')->input('first_name')) != '' ? 'readonly' : '');?>>
                                            @error('first_name')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-dark"><strong>Last Name</strong> </label>
                                            <input type="text" placeholder="Last Name" name="last_name" value="{{ old('last_name', $appointment->patient->last_name) }}" class="form-control @error('last_name') is-invalid @enderror" readonly <?php echo ((app('request')->input('last_name')) != '' ? 'readonly' : '');?>>
                                            @error('last_name')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-dark"><strong>Age</strong></label>
                                            <input type="number" placeholder="Age" id="age" name="age" value="{{ old('age', $appointment->patient->age) }}" class="form-control @error('age') is-invalid @enderror" readonly>
                                            @error('age')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                            <label for="date" class="text-body">Date </label>
                                            <div class='input-group date'>
                                                <input type='text' value="{{ old('date', app('request')->input('date')) }}" id="date-picker" class="form-control datepicker @error('date') is-invalid @enderror" name="date" placeholder="YYYY-MM-DD" required/>
                                                <label class="input-group-append input-group-text" for="date-picker" style="margin:inherit;border-radius:1px;">
                                                    <span class="fa fa-calendar"></span>
                                                </label>
                                            </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="text-dark"><strong>Patient Address</strong></label>
                                            <input type="text" placeholder="Address" name="address" value="{{ old('address',$appointment->patient->address) }}" class="form-control @error('address') is-invalid @enderror" readonly <?php echo ((app('request')->input('address')) != '' ? 'readonly' : '');?>>
                                            @error('address')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body ">
                                <hr style="border: 3px solid green;border-radius: 5px;">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" style="font-size: 40px;">Rx
                                            <textarea rows="15" class="form-control @error('rx') is-invalid @enderror" name="rx"></textarea>
                                            @error('rx')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" style="font-size: 20px;">Sig
                                            <textarea rows="5" class="form-control @error('sig') is-invalid @enderror" name="sig"></textarea>
                                            @error('sig')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" style="font-size: 20px;">Dispensing Instructions
                                            <textarea rows="5" class="form-control @error('dispensing_instructions') is-invalid @enderror" name="dispensing_instructions"></textarea>
                                            @error('dispensing_instructions')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer justify-content-right">
                                <button type="submit" name="action" class="btn btn-fill btn-primary" value="savePrescriptionForm">Save</button>
                                <a href="javascript:history.back()" class="btn btn-fill btn-danger">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">

        document.addEventListener('DOMContentLoaded', function() {
            var birthdatePicker = $("#date-picker");
            var nowDate = new Date().getFullYear();
            var bDate = new Date($('#birthdate').val()).getFullYear();
            var a = nowDate - bDate;
            $('#age').val(a);

            
            birthdatePicker.datetimepicker({
                viewMode: "years",
                format: 'YYYY-MM-DD'
            });

            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear(); 

            today = yyyy + '-' + mm + '-' + dd;
            $('#date-picker').val(today);
        });

    </script>
@endsection