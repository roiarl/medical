@extends('layouts.dashboard')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card table-with-links">
                        <div class="card-body table-responsive">
                            @if ($errors->any())
                            <ul> 
                                @foreach ($errors->all() as $message)
                                    <li> {{ $message }}</li>
                                @endforeach
                            </ul>
                            @endif
                            {{--<form method="GET" action="{{ url('appointments/create') }}">--}}
                                {{--@csrf--}}
                            @if($patients->count() > 0)
                            
                                To book as an existing Patient, please select and click on the Patient list below:
                                <p></p>
                                <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr class="success">
                                            <th class="text-left">Fullname</th>
                                            <th class="text-left">Contact Number</th>
                                            <th class="text-left">Email</th>
                                            <th class="text-left">Birthday</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($patients as $patient)
                                        <tr class="patient-row" data-href="{{ url ('appointments/patientdetail', ['id' => $patient->id]) }}">
                                            <td class="text-left">&nbsp;&nbsp;{{ $patient->full_name }} </td>
                                            <td class="text-left">&nbsp;&nbsp;{{ $patient->contact_number }} </td>
                                            <td class="text-left">&nbsp;&nbsp;{{ $patient->email }} </td>
                                            <td class="text-left">&nbsp;&nbsp;{{  date('M-d-Y', strtotime($patient->birthday)) }} </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                    <!-- @if ($patientsCount < 2 && $patientsCount > 0)
                                        <p><strong>{{ $patientsCount }} record found.</strong></p>
                                    @else
                                        <p><strong>{{ $patientsCount }} records found.</strong></p>
                                    @endif -->
                            @else
                                Is this your first time to book an appointment?
                            @endif
                                <p>Click <a href="{{ url('appointments/create') }}"><strong>HERE</strong></a> to book as a new Patient.</p>
                            {{--</form>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('stylesheets')
    <style>
        .patient-row {
            cursor: pointer;
        }

    </style>
@endsection


@section('javascript')
    <script type="text/javascript">

    document.addEventListener("DOMContentLoaded", () => {
        const rows = document.querySelectorAll("tr[data-href]");

        rows.forEach(row => {
            row.addEventListener("click", () => {
                window.location.href = row.dataset.href;
            });
        });

        sessionStorage.removeItem('UVWXYZ');
        sessionStorage.removeItem('PQRST');
        sessionStorage.removeItem('KLMNO');
        sessionStorage.removeItem('FGHIJ');
        sessionStorage.removeItem('ABCDE');

        sessionStorage.removeItem('bdate');
    });
    </script>
@endsection
