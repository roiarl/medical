@extends('layouts.dashboard')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="POST" action="{{ url('appointments/update', ['id' => $appointment->id]) }}" enctype="multipart/form-data">
                        @csrf
                        <div class="card">
                            <div class="card-header ">
                                <h4 class="card-title">Patient Details</h4>
                            </div>
                            <div class="card-body ">
                                <fieldset <?php echo (($appointment->status == 'Cancelled' || $appointment->status == 'Completed') ? 'disabled="disabled"' : '');?>>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>First Name <star class="star">*</star></label>
                                                <input type="text" placeholder="First Name" value="{{ old('first_name', $appointment->patient->first_name) }}" name="first_name" class="form-control @error('first_name') is-invalid @enderror">
                                                @error('first_name')
                                                <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Last Name <star class="star">*</star></label>
                                                <input type="text" placeholder="Last Name" value="{{ old('last_name', $appointment->patient->last_name) }}" name="last_name" class="form-control @error('last_name') is-invalid @enderror">
                                                @error('last_name')
                                                <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Middle Name </label>
                                                <input type="text" placeholder="Middle Name" value="{{ old('middle_name', $appointment->patient->middle_name) }}" name="middle_name" class="form-control @error('middle_name') is-invalid @enderror">
                                                @error('middle_name')
                                                <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Email <star class="star">*</star></label>
                                                <input type="email" placeholder="Email" value="{{ old('email', $appointment->patient->email) }}" name="email" class="form-control @error('email') is-invalid @enderror">
                                                @error('email')
                                                <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Contact Number <star class="star">*</star></label>
                                                <input type="number" placeholder="Contact No." oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength = "11" value="{{ old('contact_number', $appointment->patient->contact_number) }}" name="contact_number" class="form-control @error('contact_number') is-invalid @enderror">
                                                @error('contact_number')
                                                <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="birthdate">Birthdate <star class="star">*</star></label>
                                                <div class='input-group date' id='birthdate'>
                                                    <input type='text' value="{{ old('birthdate', $appointment->patient->birthday) }}" id="bd-picker" class="form-control datepicker @error('birthdate') is-invalid @enderror" name="birthdate" placeholder="YYYY-MM-DD"/>
                                                    <label class="input-group-append input-group-text" for="bd-picker" style="margin:inherit;border-radius:1px;">
                                                        <span class="fa fa-calendar"></span>
                                                    </label>
                                                </div>
                                                @error('birthdate')
                                                <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="gender">Gender <star class="star">*</star></label>
                                                <select class="form-control @error('gender') is-invalid @enderror" name="gender" id="gender">
                                                    <option value="">-- Select --</option>
                                                    <option value="Male" {{ old('gender', $appointment->patient->gender) == 'Male' ? 'selected' : '' }}>Male</option>
                                                    <option value="Female" {{ old('gender', $appointment->patient->gender) == 'Female' ? 'selected' : '' }}>Female</option>
                                                </select>
                                                @error('gender')
                                                <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="text-body">Insurance </label>
                                                <input type="text" placeholder="Insurance" value="{{ old('insurance', $appointment->patient->insurance) }}" name="insurance" class="form-control @error('insurance') is-invalid @enderror">
                                                @error('insurance')
                                                    <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="form-group">
                                                <label>Address <star class="star">*</star></label>
                                                <input type="text" placeholder="Address" value="{{ old('address', $appointment->patient->address) }}" name="address" class="form-control @error('address') is-invalid @enderror">
                                                @error('address')
                                                <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label class="text-body">Company Name </label>
                                                <input type="text" placeholder="Company Name" value="{{ old('company_name', $appointment->patient->company_name) }}" name="company_name" class="form-control @error('company_name') is-invalid @enderror">
                                                @error('company_name')
                                                <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="form-group">
                                                <label class="text-body">Company Address </label>
                                                <input type="text" placeholder="Company Address" value="{{ old('company_address',$appointment->patient->company_address) }}" name="company_address" class="form-control @error('company_address') is-invalid @enderror">
                                                @error('company_address')
                                                <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="requirements">Describe the reason/purpose of your visit/consultation<star class="star">*</star></label>
                                                <textarea class="form-control @error('requirements') is-invalid @enderror" id="requirements" name="requirements" rows="3">{{ old('requirements', $appointment->requirements) }}</textarea>
                                                @error('requirements')
                                                <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="medical_history">Medical History </label>
                                            <div class="row" id="medical_history">
                                                <div class="form-check checkbox-inline">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" name="medical_history[]" value="1" <?php echo (($appointment->patient->medical_history&1) == 1 ? 'checked' : '');?>>
                                                        <span class="form-check-sign"></span>
                                                        Diabetes
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" name="medical_history[]" value="8" <?php echo (($appointment->patient->medical_history&8) == 8 ? 'checked' : '');?>>
                                                        <span class="form-check-sign"></span>
                                                        Heart Condition
                                                    </label>
                                                </div>
                                                <div class="form-check checkbox-inline">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" name="medical_history[]" value="32" <?php echo (($appointment->patient->medical_history&32) == 32 ? 'checked' : '');?>>
                                                        <span class="form-check-sign"></span>
                                                        Blood Pressure
                                                    </label>
                                                </div>
                                                {{--<div class="form-check" style="font-size:12px"> Allergies: 
                                                    <input type="text" value="{{ old('allergies',$appointment->patient->allergies) }}" name="allergies"  id="allergies" style="border-style: none none solid none;">
                                                </div>--}}
                                                <div class="form-check checkbox-inline">
                                                    <input type="text" placeholder="Others" name="medical_history_others" value="{{ old('medical_history_others', $appointment->patient->medical_history_others) }}" class="form-control">
                                                </div>
                                            </div>
                                            <div class="row" id="medical_history">
                                                <div class="form-check" style="font-size:12px;"> &nbsp;&nbsp;&nbsp;&nbsp;Allergies: 
                                                    <input type='text' style="border-style: none none solid none;width:236px;" value="{{ old('allergies',$appointment->patient->allergies) }}" id="allergies" name="allergies"/>
                                                </div>
                                            </div><br>
                                            <div class="row" id="medical_history">
                                                <div class="form-check" style="font-size:12px;"> &nbsp;&nbsp;&nbsp;&nbsp;Date of Last Consultation: 
                                                    <input type='text' style="border-style: none none solid none;width:146px;" value="{{ old('last_consultation',$appointment->patient->last_consultation) }}" id="consultation_picker" name="last_consultation" placeholder="YYYY-MM-DD"/>
                                                </div>
                                                <div class="form-check" style="font-size:12px"> Purpose/Reason: 
                                                    <input type='text' style="border-style: none none solid none;width:165px;" value="{{ old('last_consultation_reason',$appointment->patient->last_consultation_reason) }}" name="last_consultation_reason"/>
                                                </div>
                                            </div><br>
                                            <div class="row" id="medical_history">
                                                <div class="form-check" style="font-size:12px;"> &nbsp;&nbsp;&nbsp;&nbsp;Date of Last Hospitalization: 
                                                    <input type='text' style="border-style: none none solid none;" value="{{ old('last_hospitalization',$appointment->patient->last_hospitalization) }}" id="hospitalization_picker" name="last_hospitalization" placeholder="YYYY-MM-DD"/>
                                                </div>
                                                <div class="form-check" style="font-size:12px"> Purpose/Reason: 
                                                    <input type='text' style="border-style: none none solid none;width:165px;" value="{{ old('last_hospitalization_reason',$appointment->patient->last_hospitalization_reason) }}" name="last_hospitalization_reason"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div><br>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="medical_history">Medicines Taken, if any: </label>
                                            <table class="table table-hover bg-light">
                                                <thead>
                                                <th>Medicine Name</th>
                                                <th>Date</th>
                                                <th>Time</th>
                                                </thead>
                                                
                                                <?php 
                                                    $med_name = explode(',',$appointment->patient->med_name);
                                                    $med_take_date = explode(',',$appointment->patient->med_take_date);
                                                    $med_take_time = explode(',',$appointment->patient->med_take_time);

                                                    $maintain_med_name = explode(',',$appointment->patient->maintain_med_name);
                                                    $maintain_frequency = explode(',',$appointment->patient->maintain_frequency);
                                                ?>
                                                <tbody>
                                                    <tr>
                                                        <td><input type="text" placeholder="Name" value="{{ old('med_name.0', $med_name[0]) }}" name="med_name[]" class="form-control"></td>
                                                        <td><input type="date" placeholder="YYYY-MM-DD e.g. 2020-04-12" value="{{ old('med_take_date.0', $med_take_date[0]) }}" name="med_take_date[]" class="form-control"></td>
                                                        <td><input type="time" placeholder="HH::MM AM/PM e.g. 11:00 AM" value="{{ old('med_take_time.0', $med_take_time[0]) }}" name="med_take_time[]" class="form-control"></td>
                                                    </tr>
                                                    <tr>
                                                        <td><input type="text" placeholder="Name" value="{{ old('med_name.1', $med_name[1]) }}" name="med_name[]" class="form-control"></td>
                                                        <td><input type="date" placeholder="YYYY-MM-DD e.g. 2020-04-12" value="{{ old('med_take_date.1', $med_take_date[1]) }}" name="med_take_date[]" class="form-control"></td>
                                                        <td><input type="time" placeholder="HH::MM AM/PM e.g. 11:00 AM" value="{{ old('med_take_time.1', $med_take_time[1]) }}" name="med_take_time[]" class="form-control"></td>
                                                    </tr>
                                                    <tr>
                                                        <td><input type="text" placeholder="Name" value="{{ old('med_name.2', $med_name[2]) }}" name="med_name[]" class="form-control"></td>
                                                        <td><input type="date" placeholder="YYYY-MM-DD e.g. 2020-04-12" value="{{ old('med_take_date.2', $med_take_date[2]) }}" name="med_take_date[]" class="form-control"></td>
                                                        <td><input type="time" placeholder="HH::MM AM/PM e.g. 11:00 AM" value="{{ old('med_take_time.2', $med_take_time[2]) }}" name="med_take_time[]" class="form-control"></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="medical_history" class="text-body">Maintenance Medicines, if any: </label>
                                            <table class="table table-hover bg-light">
                                                <thead>
                                                    <th>Medicine Name</th>
                                                    <th>Frequency</th>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <tr>
                                                        <td><input type="text" placeholder="Name" value="{{ old('maintain_med_name.0', $maintain_med_name[0]) }}" name="maintain_med_name[]" class="form-control"></td>
                                                        <td><input type="text" placeholder="Frequency" value="{{ old('maintain_frequency.0', $maintain_frequency[0]) }}" name="maintain_frequency[]" class="form-control"></td>
                                                    </tr>
                                                    <tr>
                                                        <td><input type="text" placeholder="Name" value="{{ old('maintain_med_name.1', $maintain_med_name[1]) }}" name="maintain_med_name[]" class="form-control"></td>
                                                        <td><input type="text" placeholder="Frequency" value="{{ old('maintain_frequency.1', $maintain_frequency[1]) }}" name="maintain_frequency[]" class="form-control"></td>
                                                    </tr>
                                                    <tr>
                                                        <td><input type="text" placeholder="Name" value="{{ old('maintain_med_name.2', $maintain_med_name[2]) }}" name="maintain_med_name[]" class="form-control"> </td>
                                                        <td><input type="text" placeholder="Frequency" value="{{ old('maintain_frequency.2', $maintain_frequency[2]) }}" name="maintain_frequency[]" class="form-control"></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <hr>
                                    <h4 class="card-title">Appointment Schedule</h4>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="schedule-date">Date of appointment <star class="star">*</star></label>
                                                <div class='input-group date' id='schedule-date'>
                                                    <input type='text' value="{{ old('schedule_date', $appointment->timeSlot->slot_date) }}" id="scheduledate-picker" class="form-control datepicker @error('schedule_date') is-invalid @enderror" name="schedule_date" placeholder="YYYY-MM-DD"/>
                                                    <label class="input-group-append input-group-text" for="scheduledate-picker" style="margin:inherit;border-radius:1px;">
                                                        <span class="fa fa-calendar"></span>
                                                    </label>
                                                </div>
                                                @error('schedule_date')
                                                    <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="time-slots">Time <star class="star">*</star></label>
                                            <div class="row" id="time-slots"></div>
                                            <div class="row" id="time-slots-pm"></div>
                                            @error('schedule_time')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        @error('schedule_time')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="image">Upload an image</label>
                                        <div class="input-group">
                                            <input type="file" name="image">
                                        </div>
                                        @error('image')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </fieldset>
                            </div>
                            <div class="card-footer ">
                            @if($appointment->status != 'Cancelled' && $appointment->status != 'Completed')
                                <button type="submit" class="btn btn-fill btn-primary" name="action" value="update">Update</button>
                            @endif
                                <a href="{{ url()->previous() }}" class="btn btn-fill btn-default">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">

        document.addEventListener('DOMContentLoaded', function() {

            // Date picker set
            var scheduledatePicker = $("#scheduledate-picker");
            var birthdatePicker = $("#bd-picker");
            var defaultDate = "{{ $appointment->timeSlot->slot_date }}";
            var ageObj = $("#age");

            // initialize time slots
            displayTimeSlots(scheduledatePicker.val());

            scheduledatePicker.datetimepicker({
                format: 'YYYY-MM-DD',
                minDate: moment().format("YYYY-MM-DD")
            });

            scheduledatePicker.on('dp.change', function() {
                displayTimeSlots(scheduledatePicker.val());
            });

            birthdatePicker.datetimepicker({
                viewMode: "years",
                format: 'YYYY-MM-DD'
            });

            var date = new Date();
            var bDate = new Date(birthdatePicker.val()).getFullYear();
            var nowDate = date.getFullYear();
            calcAge = nowDate-bDate;
            ageObj.val(calcAge);

            birthdatePicker.on('dp.change', function() {
                var selDate = new Date(birthdatePicker.val()).getFullYear();
                a = nowDate-selDate;
                ageObj.val(a);
            });
        });

        function displayTimeSlots(date)
        {
            $('#time-slot-title').html("Time Slots for " + date);

            var oldScheduleTime = "{{ old('schedule_time', $appointment->timeSlot->id) }}";
            var today = new Date();
            var year = today.getFullYear();
            var month = today.getMonth();
            var day = today.getDate();
            var afternoon = new Date(year, month, day, "12", 0, 0, 0);

            $.get("{{ url('appointments/slots') }}", { date: date }).done(function( data ) {
                var timeSlots = "";
                var timeSlotsPm = "";

                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        var isChecked = "";
                        var isDisabled = "";
                        var textColor = "";
                        var slotTime = data[i].slot_time;
                        var tmpAmPm = slotTime.split(' ');
                        var tmpTime = slotTime.split(':');
                        var timeSet = parseInt(tmpTime[0]);

                        if (tmpAmPm[1] == "PM" && timeSet != 12) {
                            timeSet += 12;
                        }
                        var temp_slot = new Date(year, month, day, timeSet, 0, 0, 0);

                        if (data[i].slot_count <= 1) {
                            isDisabled = "disabled";
                            textColor = "text-danger";
                            slotTime = "<s>" + slotTime + "</s>";
                        } else {
                            isDisabled = "";
                            textColor = "text-success";
                        }

                        if (oldScheduleTime == data[i].id) {
                            isChecked = "checked";
                            isDisabled = "";
                        }

                        if (isDisabled != "disabled") {
                            if (temp_slot >= afternoon) {
                                timeSlotsPm += '<div class="form-check form-check-radio" ' + isDisabled + '>' +
                                        '<label class="form-check-label ' + textColor + '">' +
                                        '<input class="form-check-input" type="radio" name="schedule_time" value="' + data[i].id + '" ' + isChecked + ' ' + isDisabled + '>' +
                                        '<span class="form-check-sign"></span>' +
                                        slotTime +
                                        '</label>' +
                                        '</div>';
                            } else {
                                timeSlots += '<div class="form-check form-check-radio" ' + isDisabled + '>' +
                                        '<label class="form-check-label ' + textColor + '">' +
                                        '<input class="form-check-input" type="radio" name="schedule_time" value="' + data[i].id + '" ' + isChecked + ' ' + isDisabled + '>' +
                                        '<span class="form-check-sign"></span>' +
                                        slotTime +
                                        '</label>' +
                                        '</div>';
                            }
                        }
                    }
                } else {
                    timeSlots = "<div class='col-md-12'><p class='text-danger'>No available time slots</p></div>";
                }

                $('#time-slots').html(timeSlots);
                $('#time-slots-pm').html(timeSlotsPm);
            });
        }

    </script>

@endsection