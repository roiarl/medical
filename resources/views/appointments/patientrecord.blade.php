@extends('layouts.dashboard')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="POST" action="{{ url('appointments/createpatientrecord', ['referer' => $referer]) }}">
                        @csrf
                        <input type="hidden" id="birthdate" name="birthdate" value="{{ $appointment->patient->birthday }}">
                        <input type="hidden" name="id" value="{{ $appointment->patient->id }}">
                        <div class="card stacked-form">
                            <div class="card-header text-center" style="background-color:blue">
                                <h4 class="card-title text-light pb-2" ><strong>Patient Record<strong></h4>
                            </div>
                            
                            <div class="card-body ">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-dark"><strong>First Name</strong> </label>
                                            <input type="text" placeholder="First Name" name="first_name" value="{{ old('first_name', $appointment->patient->first_name) }}" class="form-control" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-dark"><strong>Last Name</strong> </label>
                                            <input type="text" placeholder="Last Name" name="last_name" value="{{ old('last_name', $appointment->patient->last_name) }}" class="form-control" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-dark"><strong>Age</strong></label>
                                            <input type="number" placeholder="Age" id="age" name="age" value="{{ old('age', $appointment->patient->age) }}" class="form-control" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                            <label for="date" class="text-body">Date </label>
                                            <div class='input-group date'>
                                                <input type='text' value="{{ old('date', app('request')->input('date')) }}" id="date-picker" class="form-control datepicker @error('date') is-invalid @enderror" name="date" placeholder="YYYY-MM-DD" required/>
                                                <label class="input-group-append input-group-text" for="date-picker" style="margin:inherit;border-radius:1px;">
                                                    <span class="fa fa-calendar"></span>
                                                </label>
                                            </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="text-dark"><strong>Reason of last visit</strong></label>
                                            <textarea name="reason" value="{{ old('reason',$appointment->requirements) }}" class="form-control" readonly>{{ $appointment->requirements }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body ">
                                <hr style="border: 3px solid green;border-radius: 5px;">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">Doctor's Notes
                                            <textarea rows="15" class="form-control" name="notes"></textarea>
                                        </div>
                                    </div>
                                </div>
                                @if($appointment->patient->prescription->count() > 0)
                                <div class="row">
                                    <div class="col-md-6">
                                        Click <a href="{{ url('appointments/showprescription', ['id' => $appointment->patient->prescription->last()->id, 'referer' => $referer]) }}"><strong>HERE</strong></a> to view Patient's prescription
                                    </div>
                                </div>
                                @endif
                            </div>
                            <div class="card-footer justify-content-right">
                                <button type="submit" name="action" class="btn btn-fill btn-primary" value="savePatientRecord">Save</button>
                                <a href="{{ url()->previous() }}" class="btn btn-fill btn-danger">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">

        document.addEventListener('DOMContentLoaded', function() {
            var birthdatePicker = $("#date-picker");
            var nowDate = new Date().getFullYear();
            var bDate = new Date($('#birthdate').val()).getFullYear();
            var a = nowDate - bDate;
            $('#age').val(a);

            
            birthdatePicker.datetimepicker({
                viewMode: "years",
                format: 'YYYY-MM-DD'
            });

            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear(); 

            today = yyyy + '-' + mm + '-' + dd;
            $('#date-picker').val(today);
        });

    </script>
@endsection