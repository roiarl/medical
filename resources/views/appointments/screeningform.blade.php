@extends('layouts.dashboard')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="GET" action="{{ url('appointments/create') }}">
                        @csrf
                        <input type="hidden" name="schedule_date">
                        <input type="hidden" name="schedule_time">
                        <input type="hidden" name="requirements" value="{{ app('request')->input('requirements') }}">
                        <input type="hidden" name="middle_name" value="{{ app('request')->input('middle_name') }}">
                        <input type="hidden" name="id" value="{{ app('request')->input('id') }}">
                        <input type="hidden" id="medical_history" name="medical_history" value="">
                        <input type="hidden" name="medical_history_others" value="{{ app('request')->input('medical_history_others') }}">
                        <input type="hidden" name="allergies" value="{{ app('request')->input('allergies') }}">
                        <input type="hidden" name="last_consultation_reason" value="{{ app('request')->input('last_consultation_reason') }}">
                        <input type="hidden" name="last_hospitalization_reason" value="{{ app('request')->input('last_hospitalization_reason') }}">
                        <div class="card stacked-form">
                            <div class="card-header text-center" style="background-color:orange">
                                <h4 class="card-title text-light pb-2" ><strong>COVID-19 Screening Form<strong></h4>
                            </div>
                            <div class="card-body ">
                                <div class="row" style="background-color:silver">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-dark"><strong>First Name</strong> </label>
                                            <input type="text" placeholder="First Name" name="first_name" value="{{ old('first_name', app('request')->input('first_name')) }}" class="form-control @error('full_name') is-invalid @enderror" required <?php echo ((app('request')->input('first_name')) != '' ? 'readonly' : '');?>>
                                            @error('first_name')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-dark"><strong>Last Name</strong> </label>
                                            <input type="text" placeholder="Last Name" name="last_name" value="{{ old('last_name', app('request')->input('last_name')) }}" class="form-control @error('last_name') is-invalid @enderror" required required <?php echo ((app('request')->input('last_name')) != '' ? 'readonly' : '');?>>
                                            @error('last_name')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4" style="background-color:silver">
                                        <div class="form-group">
                                            <label class="text-dark"><strong>Contact Number</strong> </label>
                                            <input type="number" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength = "11" placeholder="Contact Number" name="contact_number" value="{{ old('contact_number', app('request')->input('contact_number')) }}" class="form-control @error('contact_number') is-invalid @enderror" required <?php echo ((app('request')->input('contact_number')) != '' ? 'readonly' : '');?>>
                                            @error('contact_number')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12" style="background-color:silver">
                                        <div class="form-group">
                                            <label class="text-dark"><strong>Address</strong></label>
                                            <input type="text" placeholder="Address" name="address" value="{{ old('address', app('request')->input('address')) }}" class="form-control @error('address') is-invalid @enderror" required <?php echo ((app('request')->input('address')) != '' ? 'readonly' : '');?>>
                                            @error('address')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3" style="background-color:silver">
                                        <div class="form-group">
                                            <label class="text-dark"><strong>Age</strong></label>
                                            <input type="number" placeholder="Age" name="age" id="age" value="{{ old('age') }}"class="form-control @error('age') is-invalid @enderror" readonly>
                                            @error('age')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="background-color:silver">
                                        <div class="form-group">
                                            <label class="text-dark"><strong>Email</strong></label>
                                            <input type="text" placeholder="Email" name="email" value="{{ old('email', app('request')->input('email')) }}" class="form-control @error('email') is-invalid @enderror" required <?php echo ((app('request')->input('email')) != '' ? 'readonly' : '');?>>
                                            @error('email')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="background-color:silver">
                                        <div class="form-group">
                                            <label class="text-dark"><strong>Gender</strong></label>
                                            <select class="form-control @error('gender') is-invalid @enderror" name="gender" id="gender" required>
                                            @if(app('request')->input('gender') == "Male")
                                                <option value="" disabled>-- Select --</option>
                                                <option value="Male" selected >Male</option>
                                                <option value="Female" disabled>Female</option>
                                            @elseif( app('request')->input('gender') == "Female")
                                                <option value="" disabled>-- Select --</option>
                                                <Male value="Male" disabled>Male</option>
                                                <option value="Female" selected>Female</option>
                                            @else
                                                <option value="">-- Select --</option>
                                                <option value="Male">Male</option>
                                                <option value="Female">Female</option>
                                            @endif
                                            </select>
                                            @error('gender')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="background-color:silver">
                                        <div class="form-group">
                                            <label class="text-dark"><strong>Body Temperature: </strong></label>
                                            <input type="number" placeholder="Body Temperature" name="body_temperature" value="{{ old('body_temperature') }}" step="0.01" class="form-control @error('body_temperature') is-invalid @enderror">
                                            @error('body_temperature')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row font-weight-normal" style="background-color:#fffacd">
                                    <div class="col-md-6 form-check-inline bg-warning text-left pl-3">
                                        <p>1.  Have you visited any affected geographic area/contry within the last 30 days prior to your schedules appointment?</p>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="custom-control custom-checkbox custom-control-inline">
                                            <input type="checkbox" class="custom-control-input @error('q1') is-invalid @enderror" name="q1" id="q1n" value="0">
                                            <label class="custom-control-label" for="q1n">No</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-control-inline">
                                            <input type="checkbox" class="custom-control-input @error('q1') is-invalid @enderror" name="q1" id="q1y" value="1">
                                            <label class="custom-control-label" for="q1y">Yes</label>
                                        </div>
                                        @error('q1')
                                                <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                        <div class="form-group pl-3" style="font-size:15px"> If yes, What Country? 
                                            <input type="text" placeholder="" value="" name="q1_country" id="q1_country" style="border-style: none none solid none;background-color:#fffacd">
                                        </div>
                                    </div>
                                </div>
                                <div class="row font-weight-normal" style="background-color:#fffacd">
                                    <div class="col-md-6 form-check-inline bg-warning text-left pl-3">
                                        <p>2.  Have you been in close contact with someone whoe arrived form abroad within the last 30 days?</p>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="custom-control custom-checkbox custom-control-inline">
                                            <input type="checkbox" class="custom-control-input" name="q2" id="q2n" value="0">
                                            <label class="custom-control-label" for="q2n">No</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-control-inline">
                                            <input type="checkbox" class="custom-control-input" name="q2" id="q2y" value="2">
                                            <label class="custom-control-label" for="q2y">Yes</label>
                                        </div>
                                        @error('q2')
                                                <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                        <div class="form-group pl-3" style="font-size:15px"> If yes, What Country? 
                                            <input type="text" placeholder="" value="" name="q2_country"  id="q2_country" style="border-style: none none solid none;background-color:#fffacd">
                                        </div>
                                    </div>
                                </div>
                                <div class="row font-weight-normal" style="background-color:#fffacd">
                                    <div class="col-md-6 form-check-inline bg-warning text-left pl-3">
                                        <p>3.  Have you attended a mass gathering, reunion with relatives/friends or parties within a month prior to this visit?</p>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="custom-control custom-checkbox custom-control-inline">
                                            <input type="checkbox" class="custom-control-input" name="q3" id="q3n" value="0">
                                            <label class="custom-control-label" for="q3n">No</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-control-inline">
                                            <input type="checkbox" class="custom-control-input" name="q3" id="q3y" value="4">
                                            <label class="custom-control-label" for="q3y">Yes</label>
                                        </div>
                                        @error('q3')
                                                <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row font-weight-normal" style="background-color:#fffacd">
                                    <div class="col-md-6 form-check-inline bg-warning text-left pl-3">
                                        <p>4.  Have you been in close contact with a COVID-19 positive patient?</p>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="custom-control custom-checkbox custom-control-inline">
                                            <input type="checkbox" class="custom-control-input" name="q4" id="q4n" value="0">
                                            <label class="custom-control-label" for="q4n">No</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-control-inline">
                                            <input type="checkbox" class="custom-control-input" name="q4" id="q4y" value="8">
                                            <label class="custom-control-label" for="q4y">Yes</label>
                                        </div>
                                        @error('q4')
                                                <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row font-weight-normal" style="background-color:#fffacd">
                                    <div class="col-md-6 form-check-inline bg-warning text-left pl-3">
                                        <p>5.  Have you been in close contact with a person under investigation? (PUI)</p>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="custom-control custom-checkbox custom-control-inline">
                                            <input type="checkbox" class="custom-control-input" name="q5" id="q5n" value="0">
                                            <label class="custom-control-label" for="q5n">No</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-control-inline">
                                            <input type="checkbox" class="custom-control-input" name="q5" id="q5y" value="16">
                                            <label class="custom-control-label" for="q5y">Yes</label>
                                        </div>
                                        @error('q5')
                                                <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row font-weight-normal" style="background-color:#fffacd">
                                    <div class="col-md-6 form-check-inline bg-warning text-left pl-3">
                                        <p>6.  Have you been in close contact with a person under monitoring? (PUM)</p>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="custom-control custom-checkbox custom-control-inline">
                                            <input type="checkbox" class="custom-control-input" name="q6" id="q6n" value="0">
                                            <label class="custom-control-label" for="q6n">No</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-control-inline">
                                            <input type="checkbox" class="custom-control-input" name="q6" id="q6y" value="32">
                                            <label class="custom-control-label" for="q6y">Yes</label>
                                        </div>
                                        @error('q6')
                                                <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row font-weight-normal" style="background-color:#fffacd">
                                    <div class="col-md-6 form-check-inline bg-warning text-left pl-3">
                                        <p>7.  Do you have any flu-like/respiratory symptoms? Ex: Fever, cough, runny nose, sore throat, headache, shortness of breath, chills, general malaise, diarrhea</p>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="custom-control custom-checkbox custom-control-inline">
                                            <input type="checkbox" class="custom-control-input" name="q7" id="q7n" value="0">
                                            <label class="custom-control-label" for="q7n">No</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-control-inline">
                                            <input type="checkbox" class="custom-control-input" name="q7" id="q7y" value="64">
                                            <label class="custom-control-label" for="q7y">Yes</label>
                                        </div>
                                        @error('q7')
                                                <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                        <div class="form-group pl-3" style="font-size:15px"> If yes, What Symptoms? 
                                            <input type="text" placeholder="" value="" name="q7_country" id="q7_country" style="border-style: none none solid none;background-color:#fffacd">
                                        </div>
                                    </div>
                                </div>
                                <div class="row font-weight-normal" style="background-color:#fffacd">
                                    <div class="col-md-6 form-check-inline bg-warning text-left pl-3">
                                        <p>8.  Is there anything else we should know before treating you?</p>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="custom-control custom-checkbox custom-control-inline">
                                            <input type="checkbox" class="custom-control-input" name="q8" id="q8n" value="0">
                                            <label class="custom-control-label" for="q8n">No</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-control-inline">
                                            <input type="checkbox" class="custom-control-input" name="q8" id="q8y" value="128">
                                            <label class="custom-control-label" for="q8y">Yes</label>
                                        </div>
                                        @error('q8')
                                                <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row font-weight-normal" style="background-color:#fffacd">
                                    <div class="col-md-6 form-check-inline bg-warning text-left pl-3">
                                        <p>9.  Are you currently experiencing a Dental Emergency?</p>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="custom-control custom-checkbox custom-control-inline">
                                            <input type="checkbox" class="custom-control-input" name="q9" id="q9n" value="0">
                                            <label class="custom-control-label" for="q9n">No</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-control-inline">
                                            <input type="checkbox" class="custom-control-input" name="q9" id="q9y" value="256">
                                            <label class="custom-control-label" for="q9y">Yes</label>
                                        </div>
                                        @error('q9')
                                                <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row font-weight-normal" style="background-color:#fffacd">
                                    <div class="col-md-12 form-check-inline bg-warning text-left pl-3">
                                        <p>By submitting this form, I hereby declare that the above statements are true, accurate and complete.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer justify-content-right">
                                <button type="submit" name="action" class="btn btn-fill btn-primary" value="saveScreeningForm">Save</button>
                                <a href="javascript:history.back()" class="btn btn-fill btn-danger">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">
    
        document.addEventListener('DOMContentLoaded', function() {
            //var nowDate = new Date().getFullYear();
            var today = new Date();

            var birthDateValue = sessionStorage.getItem('bdate');
            if (birthDateValue === null || birthDateValue === "") {
                // do nothing
            } else {
                var birthDate = new Date(birthDateValue);

                var age = today.getFullYear() - birthDate.getFullYear();
                var m = today.getMonth() - birthDate.getMonth();

                if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                    age--;
                }

                $('#age').val(age);
            }

            var medHist = sessionStorage.getItem('medical_history');
            $('#medical_history').val(medHist);
        });

        $('#q1n').change(function(){
            if($('#q1n').is(':checked')){
                $('#q1y').prop("checked", false);
            }else{
                $('#q1y').prop("checked", true);
            }
        })
        $('#q1y').change(function(){
            if($('#q1y').is(':checked')){
                $('#q1n').prop("checked", false);
            }else{
                $('#q1n').prop("checked", true);
            }
        })
        $('#q2n').change(function(){
            if($('#q2n').is(':checked')){
                $('#q2y').prop("checked", false);
            }else{
                $('#q2y').prop("checked", true);
            }
        })
        $('#q2y').change(function(){
            if($('#q2y').is(':checked')){
                $('#q2n').prop("checked", false);
            }else{
                $('#q2n').prop("checked", true);
            }
        })
        $('#q3n').change(function(){
            if($('#q3n').is(':checked')){
                $('#q3y').prop("checked", false);
            }else{
                $('#q3y').prop("checked", true);
            }
        })
        $('#q3y').change(function(){
            if($('#q3y').is(':checked')){
                $('#q3n').prop("checked", false);
            }else{
                $('#q3n').prop("checked", true);
            }
        })
        $('#q4n').change(function(){
            if($('#q4n').is(':checked')){
                $('#q4y').prop("checked", false);
            }else{
                $('#q4y').prop("checked", true);
            }
        })
        $('#q4y').change(function(){
            if($('#q4y').is(':checked')){
                $('#q4n').prop("checked", false);
            }else{
                $('#q4n').prop("checked", true);
            }
        })
        $('#q5n').change(function(){
            if($('#q5n').is(':checked')){
                $('#q5y').prop("checked", false);
            }else{
                $('#q5y').prop("checked", true);
            }
        })
        $('#q5y').change(function(){
            if($('#q5y').is(':checked')){
                $('#q5n').prop("checked", false);
            }else{
                $('#q5n').prop("checked", true);
            }
        })
        $('#q6n').change(function(){
            if($('#q6n').is(':checked')){
                $('#q6y').prop("checked", false);
            }else{
                $('#q6y').prop("checked", true);
            }
        })
        $('#q6y').change(function(){
            if($('#q6y').is(':checked')){
                $('#q6n').prop("checked", false);
            }else{
                $('#q6n').prop("checked", true);
            }
        })
        $('#q7n').change(function(){
            if($('#q7n').is(':checked')){
                $('#q7y').prop("checked", false);
            }else{
                $('#q7y').prop("checked", true);
            }
        })
        $('#q7y').change(function(){
            if($('#q7y').is(':checked')){
                $('#q7n').prop("checked", false);
            }else{
                $('#q7n').prop("checked", true);
            }
        })
        $('#q8n').change(function(){
            if($('#q8n').is(':checked')){
                $('#q8y').prop("checked", false);
            }else{
                $('#q8y').prop("checked", true);
            }
        })
        $('#q8y').change(function(){
            if($('#q8y').is(':checked')){
                $('#q8n').prop("checked", false);
            }else{
                $('#q8n').prop("checked", true);
            }
        })
        $('#q9n').change(function(){
            if($('#q9n').is(':checked')){
                $('#q9y').prop("checked", false);
            }else{
                $('#q9y').prop("checked", true);
            }
        })
        $('#q9y').change(function(){
            if($('#q9y').is(':checked')){
                $('#q9n').prop("checked", false);
            }else{
                $('#q9n').prop("checked", true);
            }
        })
    </script>
@endsection