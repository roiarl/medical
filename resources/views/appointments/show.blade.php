@extends('layouts.dashboard')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                                <i class="nc-icon nc-simple-remove"></i>
                            </button>
                            <span>
                                <b>
                                    {{ session('message') }}
                                </b>
                            </span>
                        </div>
                    @endif
                    <div class="card stacked-form">
                        <div class="card-header ">
                            <h4 class="card-title">Appointment Details</h4>
                        </div>
                        <div class="card-body ">
                            <form>
                                <?php
                                    $var = [];
                                    $medHistory = [];

                                    if (!empty($appointment->patient->screening)) {
                                        if (($appointment->patient->screening->questions&1) == 1) {
                                            $var['#1'] = 'Yes';
                                        }
                                        if (($appointment->patient->screening->questions&2) == 2) {
                                            $var['#2'] = 'Yes';
                                        }
                                        if (($appointment->patient->screening->questions&4) == 4) {
                                            $var['#3'] = 'Yes';
                                        }
                                        if (($appointment->patient->screening->questions&8) == 8) {
                                            $var['#4'] = 'Yes';
                                        }
                                        if (($appointment->patient->screening->questions&16) == 16) {
                                            $var['#5'] = 'Yes';
                                        }
                                        if (($appointment->patient->screening->questions&32) == 32) {
                                            $var['#6'] = 'Yes';
                                        }
                                        if (($appointment->patient->screening->questions&64) == 64) {
                                            $var['#7'] = 'Yes';
                                        }
                                        if (($appointment->patient->screening->questions&128) == 128) {
                                            $var['#8'] = 'Yes';
                                        }
                                        if (($appointment->patient->screening->questions&256) == 256) {
                                            $var['#9'] = 'Yes';
                                        }

                                        $var = json_encode($var);
                                        $myJsonString = str_replace(['{', '}', '"'], '', $var);
                                        $finalString = str_replace(':', ' - ', $myJsonString);
                                        $finalString = str_replace(',', ',  ', $finalString);
                                    }

                                    if (($appointment->patient->medical_history&1) == 1) {
                                        array_push($medHistory,"Diabetes");
                                    }
                                    if (($appointment->patient->medical_history&2) == 2) {
                                        array_push($medHistory,"Bleeding Tendency");
                                    }
                                    if (($appointment->patient->medical_history&4) == 4) {
                                        array_push($medHistory,"Drug Sensitivity");
                                    }
                                    if (($appointment->patient->medical_history&8) == 8) {
                                        array_push($medHistory,"Heart Condition");
                                    }
                                    if (($appointment->patient->medical_history&16) == 16) {
                                        array_push($medHistory,"Rheumatic Fever");
                                    }
                                    if (($appointment->patient->medical_history&32) == 32) {
                                        array_push($medHistory,"Blood Pressure");
                                    }

                                    if (($appointment->patient->medical_history_others) != '') {
                                        $others = $appointment->patient->medical_history_others;
                                        array_push($medHistory, $others);
                                    }

                                    $medHistory = json_encode($medHistory);
                                    $medHis = str_replace(['[', ']', '"'], '', $medHistory);

                                ?>
                                <div class="form-group row">
                                    <label for="patient" class="col-sm-2 col-form-label">Patient Name</label>
                                    <div class="col-sm-10">
                                        <span>{{ $appointment->patient->full_name }}</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="date" class="col-sm-2 col-form-label">Date</label>
                                    <div class="col-sm-10">
                                        <span>{{ $appointment->timeSlot->slot_date }}</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="time" class="col-sm-2 col-form-label">Time</label>
                                    <div class="col-sm-10">
                                        <span>{{ $appointment->timeSlot->slot_time }}</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="time" class="col-sm-2 col-form-label">Contact Number</label>
                                    <div class="col-sm-10">
                                        <p>
                                            <span>{{ $appointment->patient->contact_number }}</span>
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="time" class="col-sm-2 col-form-label">reason/purpose of visit/consultation</label>
                                    <div class="col-sm-10">
                                        <p>
                                            <span>{{ $appointment->requirements }}</span>
                                        </p>
                                    </div>
                                </div>
                                @if(auth()->user()->isAdministrator())
                                <div class="form-group row">
                                    <label for="time" class="col-sm-2 col-form-label">Medical History</label>
                                    <div class="col-sm-10">
                                        <p>
                                            <span>{{ $medHis }}</span>
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="time" class="col-sm-2 col-form-label">Medicine Taken</label>
                                    <div class="col-sm-10">
                                        <p>
                                            <span>{{ $appointment->patient->med_name == ',,' ?  '' : $appointment->patient->med_name }}</span>
                                        </p>
                                    </div>
                                </div>
                                @endif
                                <div class="form-group row">
                                    <label for="time" class="col-sm-2 col-form-label">Status</label>
                                    <div class="col-sm-10">
                                        <p>
                                            <span>{{ $appointment->status }}</span>
                                        </p>
                                    </div>
                                </div>
                                @if($appointment->patient->image)
                                <div class="form-group row">
                                    <label for="time" class="col-sm-2 col-form-label">Uploaded Image</label>
                                    <div class="col-sm-10">
                                        <a href="{{ url('appointments/download', ['id' => $appointment->patient->id]) }}">{{ $appointment->patient->image }} </a>
                                    </div>
                                </div>
                                @endif
                            </form>
                        </div>
                        <div class="card-footer ">
                            <a href="{{ url('appointments/edit', ['id' => $appointment->id]) }}" class="btn btn-fill btn-primary" rel="tooltip" title="Edit Appointment">Edit</a>
                            <a href="{{ url('appointments/showscreen', ['id' => $appointment->id]) }}" class="btn btn-fill btn-primary" rel="tooltip" title="View/Edit COVID-19 Screening Form">Edit Screening Form</a>
                            {{--<a href="{{ url('appointments/showconsent', ['id' => $appointment->id]) }}" class="btn btn-fill btn-primary" rel="tooltip" title="View/Edit COVID-19 Dental Consent Form">Consent</a>--}}
                            <a href="{{ url('appointments/index') }}" class="btn btn-fill btn-default">Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection