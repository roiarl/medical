@extends('layouts.dashboard')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card table-with-links">
                        <div class="card-body table-responsive">
                            @if ($errors->any())
                            <ul> 
                                @foreach ($errors->all() as $message)
                                    <li> {{ $message }}</li>
                                @endforeach
                            </ul>
                            @endif
                            <form method="GET" action="{{ url('appointments/patients') }}">
                                @csrf
                                <button type="submit" name="action" onclick="changeColor(this)" id="UVWXYZ" class="btn btn-fill btn-primary float-right" value="UVWXYZ">U - Z</button>&nbsp;
                                <button type="submit" name="action" onclick="changeColor(this)" id="PQRST" class="btn btn-fill btn-primary float-right" value="PQRST" style="margin-right:5px;">P - T</button>&nbsp;
                                <button type="submit" name="action" onclick="changeColor(this)" id="KLMNO" class="btn btn-fill btn-primary float-right" value="KLMNO" style="margin-right:5px;">K - O</button>&nbsp;
                                <button type="submit" name="action" onclick="changeColor(this)" id="FGHIJ" class="btn btn-fill btn-primary float-right" value="FGHIJ" style="margin-right:5px;">F - J</button>&nbsp;
                                <button type="submit" name="action" onclick="changeColor(this)" id="ABCDE" class="btn btn-fill btn-primary float-right" value="ABCDE" style="margin-right:5px;">A - E</button>&nbsp;
                                <input type="text" class="form-control" placeholder="Enter Search Keyword" id="search" name="search"> <br/>
                                <table class="table table-hover table-striped table-bordered">
                                    <thead>
                                        <tr class="success d-flex">
                                            <th class="text-center col-2">Fullname</th>
                                            <th class="text-center col-2">Contact Number</th>
                                            <th class="text-center col-3">Email</th>
                                            <th class="text-center col-1">Gender</th>
                                            <th class="text-center col-3">Date of Last Appointment</th>
                                            <th class="text-center col-1">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody id="search_table">
                                        <!-- Body of AJAX table here -->
                                    </tbody>
                                    
                                    <tbody id="initial_table">
                                        @foreach($patients as $patient)
                                            <tr class="d-flex">
                                                <td class="text-center col-2">{{ $patient->last_name }}, {{ $patient->first_name }} </td>
                                                <td class="text-center col-2">{{ $patient->contact_number }} </td>
                                                <td class="text-center col-3">{{ $patient->email }} </td>
                                                <td class="text-center col-1">{{ $patient->gender }} </td>
                                                <td class="text-center col-3">{{ date('M-d-Y h:m A', strtotime($patient->updated_at)) }} </td>
                                                <td class="text-center  col-1">
                                                    <a href="{{ url ('appointments/patienthistory', ['id' => $patient->id, 'referer' => 'patients']) }}" rel="tooltip" title="Show Patient History" class="btn btn-success btn-link btn-xs">
                                                        <i class="fa fa-history"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function() {
        sessionStorage.removeItem('selectedDate');

        var UVWXYZ = sessionStorage.getItem('UVWXYZ');
        var PQRST = sessionStorage.getItem('PQRST');
        var KLMNO = sessionStorage.getItem('KLMNO');
        var FGHIJ = sessionStorage.getItem('FGHIJ');
        var ABCDE = sessionStorage.getItem('ABCDE');

        sessionStorage.removeItem('bdate');

        if (UVWXYZ != null) {
            document.getElementById("UVWXYZ").style.backgroundColor ="green";
        }

        if (PQRST != null) {
            document.getElementById("PQRST").style.backgroundColor ="green";
        }

        if (KLMNO != null) {
            document.getElementById("KLMNO").style.backgroundColor ="green";
        }

        if (FGHIJ != null) {
            document.getElementById("FGHIJ").style.backgroundColor ="green";
        }

        if (ABCDE != null) {
            document.getElementById("ABCDE").style.backgroundColor ="green";
        }

    });

    function changeColor(btn) {
        if (btn.id == 'UVWXYZ') {
            sessionStorage.setItem('UVWXYZ', 'changecolor');
            sessionStorage.removeItem('PQRST');
            sessionStorage.removeItem('KLMNO');
            sessionStorage.removeItem('FGHIJ');
            sessionStorage.removeItem('ABCDE');
        }
        if (btn.id == 'PQRST') {
            sessionStorage.setItem('PQRST', 'changecolor');
            sessionStorage.removeItem('UVWXYZ');
            sessionStorage.removeItem('KLMNO');
            sessionStorage.removeItem('FGHIJ');
            sessionStorage.removeItem('ABCDE');
        }
        if (btn.id == 'KLMNO') {
            sessionStorage.setItem('KLMNO', 'changecolor');
            sessionStorage.removeItem('PQRST');
            sessionStorage.removeItem('UVWXYZ');
            sessionStorage.removeItem('FGHIJ');
            sessionStorage.removeItem('ABCDE');
        }
        if (btn.id == 'FGHIJ') {
            sessionStorage.setItem('FGHIJ', 'changecolor');
            sessionStorage.removeItem('PQRST');
            sessionStorage.removeItem('KLMNO');
            sessionStorage.removeItem('UVWXYZ');
            sessionStorage.removeItem('ABCDE');
        }
        if (btn.id == 'ABCDE') {
            sessionStorage.setItem('ABCDE', 'changecolor');
            sessionStorage.removeItem('PQRST');
            sessionStorage.removeItem('KLMNO');
            sessionStorage.removeItem('FGHIJ');
            sessionStorage.removeItem('UVWXYZ');
        }
    }

    $('#search').on('keyup', function(){
        var value = $(this).val();
        $.ajax({
            type : 'get',
            url : "{{ url('appointments/search') }}",
            data : {
                search: value,
            },

            success:function(data){
                $('#initial_table').hide();
                $('#search_table').html( data );
            },

            error:function(err, textStatus, errorThrown) {
                console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
            },
        });
    });

    $.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
</script>
@endsection
