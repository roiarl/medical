@extends('layouts.dashboard')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8">
                    <div class="card card-calendar">
                        <div class="card-body ">
                            <div id="fullCalendar"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <form method="GET" action="{{ url('/appointments/create') }}">
                        @csrf
                        <input type="hidden" name="date-slot" id="date-slot" value="{{ now()->format('Y-m-d') }}">
                        <input type="hidden" name="selectedDateTimeSlot" id="selectedDateTimeSlot"></input>
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title" id="time-slot-title">Available appointment times: </h4>
                            </div>
                            <div class="card-body ">
                                <div class="row">
                                    <div class="col-sm-5" id="time-slots"></div>
                                    <div class="col-sm-5" id="time-slots-pm"></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12" id="time-slots-status"></div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" id="book-now-submit" class="btn btn-fill btn-primary">Book Now</button>
                            </div>
                        </div>
                    </form>
                    @if(auth()->user()->isAdministrator() || auth()->user()->isSuperUser())
                        <form method="GET" action="{{ url('/appointments/calendar') }}">
                            @csrf
                            <div class="card">
                                <div class="card-header ">
                                    <h5 class="card-title">Manage Date/Time</h5>
                                </div>
                                <input type="hidden" name="selectedDateToTimeSlot" id="selectedDateToTimeSlot" value="{{ $selectedDate }}"></input>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label for="timeSlotDate" class="text-body">Start date:</label>
                                            <input type='text' id="timeSlotDate" class="form-control" name="timeSlotDate"></input>
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="endDate" class="text-body">End date:</label>
                                            <input type='text' id="endDate" class="form-control datepicker" name="endDate" placeholder="YYYY-MM-DD"></input>
                                        </div>
                                    </div>
                                </div>
                                <hr style="height:1px;border-width:0;color:gray;background-color:gray">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label for="dateFrom" class="text-body">Start time:</label>
                                            <select class="form-control" name="dateFrom" id="dateFrom">
                                                <option value="">-- Select --</option>
                                                <option value="08:00">08:00 AM</option>
                                                <option value="08:30">08:30 AM</option>
                                                <option value="09:00">09:00 AM</option>
                                                <option value="09:30">09:30 AM</option>
                                                <option value="10:00">10:00 AM</option>
                                                <option value="10:30">10:30 AM</option>
                                                <option value="11:00">11:00 AM</option>
                                                <option value="11:30">11:30 AM</option>
                                                <option value="12:00">12:00 PM</option>
                                                <option value="12:30">12:30 PM</option>
                                                <option value="13:00">01:00 PM</option>
                                                <option value="13:30">01:30 PM</option>
                                                <option value="14:00">02:00 PM</option>
                                                <option value="14:30">02:30 PM</option>
                                                <option value="15:00">03:00 PM</option>
                                                <option value="15:30">03:30 PM</option>
                                                <option value="16:00">04:00 PM</option>
                                                <option value="16:30">04:30 PM</option>
                                                <option value="17:00">05:00 PM</option>
                                                <option value="17:30">05:30 PM</option>
                                                <option value="18:00">06:00 PM</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="dateTo" class="text-body">End time:</label>
                                            <select class="form-control" name="dateTo" id="dateTo">
                                                <option value="">-- Select --</option>
                                                <option value="08:00">08:00 AM</option>
                                                <option value="08:30">08:30 AM</option>
                                                <option value="09:00">09:00 AM</option>
                                                <option value="09:30">09:30 AM</option>
                                                <option value="10:00">10:00 AM</option>
                                                <option value="10:30">10:30 AM</option>
                                                <option value="11:00">11:00 AM</option>
                                                <option value="11:30">11:30 AM</option>
                                                <option value="12:00">12:00 PM</option>
                                                <option value="12:30">12:30 PM</option>
                                                <option value="13:00">01:00 PM</option>
                                                <option value="13:30">01:30 PM</option>
                                                <option value="14:00">02:00 PM</option>
                                                <option value="14:30">02:30 PM</option>
                                                <option value="15:00">03:00 PM</option>
                                                <option value="15:30">03:30 PM</option>
                                                <option value="16:00">04:00 PM</option>
                                                <option value="16:30">04:30 PM</option>
                                                <option value="17:00">05:00 PM</option>
                                                <option value="17:30">05:30 PM</option>
                                                <option value="18:00">06:00 PM</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                {{--<div class="container">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="timeslots" name="timeslots[]" value="08:00">&nbsp;&nbsp;8:00 AM
                                    </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="timeslots" name="timeslots[]" value="08:30">&nbsp;&nbsp;8:30 AM
                                    </label>&nbsp;&nbsp;&nbsp;&nbsp;
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="timeslots" name="timeslots[]" value="09:00">&nbsp;&nbsp;9:00 AM
                                    </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="timeslots" name="timeslots[]" value="09:30">&nbsp;&nbsp;9:30 AM
                                    </label>&nbsp;&nbsp;&nbsp;
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="timeslots" name="timeslots[]" value="10:00">&nbsp;&nbsp;10:00 AM
                                    </label>&nbsp;&nbsp;&nbsp;
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="timeslots" name="timeslots[]" value="10:30">&nbsp;&nbsp;10:30 AM
                                    </label>&nbsp;&nbsp;&nbsp;
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="timeslots" name="timeslots[]" value="11:00">&nbsp;&nbsp;11:00 AM
                                    </label>&nbsp;&nbsp;&nbsp;
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="timeslots" name="timeslots[]" value="11:30">&nbsp;&nbsp;11:30 AM
                                    </label>&nbsp;&nbsp;&nbsp;
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="timeslots" name="timeslots[]" value="12:00">&nbsp;&nbsp;12:00 PM
                                    </label>&nbsp;&nbsp;&nbsp;
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="timeslots" name="timeslots[]" value="12:30">&nbsp;&nbsp;12:30 PM
                                    </label>&nbsp;&nbsp;&nbsp;
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="timeslots" name="timeslots[]" value="01:00">&nbsp;&nbsp;01:00 PM
                                    </label>&nbsp;&nbsp;&nbsp;
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="timeslots" name="timeslots[]" value="01:30">&nbsp;&nbsp;01:30 PM
                                    </label>&nbsp;&nbsp;&nbsp;
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="timeslots" name="timeslots[]" value="02:00">&nbsp;&nbsp;02:00 PM
                                    </label>&nbsp;&nbsp;&nbsp;
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="timeslots" name="timeslots[]" value="02:30">&nbsp;&nbsp;02:30 PM
                                    </label>&nbsp;&nbsp;&nbsp;
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="timeslots" name="timeslots[]" value="03:00">&nbsp;&nbsp;03:00 PM
                                    </label>&nbsp;&nbsp;&nbsp;
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="timeslots" name="timeslots[]" value="03:30">&nbsp;&nbsp;03:30 PM
                                    </label>&nbsp;&nbsp;&nbsp;
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="timeslots" name="timeslots[]" value="04:00">&nbsp;&nbsp;04:00 PM
                                    </label>&nbsp;&nbsp;&nbsp;
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="timeslots" name="timeslots[]" value="04:30">&nbsp;&nbsp;04:30 PM
                                    </label>&nbsp;&nbsp;&nbsp;
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="timeslots" name="timeslots[]" value="05:00">&nbsp;&nbsp;05:00 PM
                                    </label>&nbsp;&nbsp;&nbsp;
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="timeslots" name="timeslots[]" value="05:30">&nbsp;&nbsp;05:30 PM
                                    </label>&nbsp;&nbsp;&nbsp;
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="timeslots" name="timeslots[]" value="06:00">&nbsp;&nbsp;06:00 PM
                                    </label>
                                </div>--}}
                                <div class="card-footer">
                                    <button type="submit" name="action" value="ADD" class="btn btn-fill btn-primary">Add Time Slot</button>&nbsp;&nbsp;
                                    <button type="submit" name="action" value="REMOVE" class="btn btn-fill btn-primary">Block-off Time</button>
                                </div>
                                @if ($errors)
                                    <p></p>
                                    <h6 class="error text-danger ml-3" >{{ $errors->first() }}</h6>
                                    <p></p>
                                @endif
                            </div>
                        </form>
                    @endif
                </div>
            </div>
            @if(auth()->user()->isAdministrator() || auth()->user()->isSuperUser())
                <div class="row">
                    <div class="col-md-12">
                        <div class="card table-with-links">
                            <div class="card-header text-center" style="background-color:blue">
                                <h4 class="card-title text-center text-light pb-2" id="appointments-title">Scheduled appointments</h4>
                            </div>
                            <div class="card-body table-responsive" id="appointments-table">
                                {{-- JS Table Generated --}}
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection

@section('stylesheets')
    <link href="{{ asset('vendor/calendar/core/main.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('vendor/calendar/daygrid/main.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('vendor/calendar/bootstrap/main.min.css') }}" rel="stylesheet" />
    <style>
        .fc-other-month .fc-day-number {
            opacity: 1 !important;
            filter: alpha(opacity=100) !important;
        }

    </style>
@endsection

@section('javascript')
    <script src="{{ asset('vendor/calendar/core/main.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('vendor/calendar/daygrid/main.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('vendor/calendar/interaction/main.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('vendor/calendar/bootstrap/main.min.js') }}" type="text/javascript"></script>

    <script type="text/javascript">

        document.addEventListener('DOMContentLoaded', function() {

            var calendarEl = document.getElementById('fullCalendar');

            sessionStorage.removeItem('selectedDate');

            sessionStorage.removeItem('UVWXYZ');
            sessionStorage.removeItem('PQRST');
            sessionStorage.removeItem('KLMNO');
            sessionStorage.removeItem('FGHIJ');
            sessionStorage.removeItem('ABCDE');

            sessionStorage.removeItem('bdate');

            var calendar = new FullCalendar.Calendar(calendarEl, {
                plugins: [ 'dayGrid', 'interaction' ],
                defaultView: 'dayGridMonth',
                eventLimit: true,
                events: "{{ route('appointments.eventList') }}",
                // events: [
                //     {
                //         start : '2020-10-20',
                //         title : 'this is a test'
                //     }
                // ],
                showNonCurrentDates: true,
                validRange: {
                    start: moment().format("YYYY-MM-DD")
                },
                dateClick: function(info) {

                    console.log(info);

                    calendar.gotoDate(info.date);

                    $('td[data-date]').attr('style', '');
                    $('td[data-date]').removeClass('fc-today');

                    $('#date-slot').val(info.dateStr);

                    sessionStorage.setItem('selectedDate', info.dateStr);

                    $('#selectedDateToTimeSlot').val(info.dateStr);

                    $('#selectedDateTimeSlot').val(info.dateStr);

                    displayTimeSlots(info.dateStr);
                    displayAppointments(info.dateStr);

                    info.dayEl.style.backgroundColor = "#66cc00";

                    $('.fc-day[data-date="' + info.dateStr + '"]').css('background-color', "#66cc00");

                },
                datesRender: function(view, element) {

                    var selectedDate = $('#selectedDateToTimeSlot').val();
                    if (selectedDate == '' || selectedDate == 'undefined' || selectedDate == null) {
                        var selectedDate = $('#date-slot').val();
                    }
                    displayTimeSlots(selectedDate);
                    displayAppointments(selectedDate);

                    $('td[data-date]').attr('style', '');
                    $('td[data-date]').removeClass('fc-today');

                    $('.fc-day[data-date="' + selectedDate + '"]').css('background-color', "#66cc00");
                }
            });

            var date = $('#date-slot').val();

            // initialize time slots
            displayTimeSlots(date);
            displayAppointments(date);

            calendar.render();
        });

        function displayTimeSlots(date)
        {
            var d = moment(date).format('MMM-DD-YYYY');
            var dayOfWeek = moment(date).format('ddd');
            $('#time-slot-title').html("Available appointment times: <p><b>" + d + "</b></p>");
            $('#timeSlotDate').val(date);
            var today = new Date();
            var year = today.getFullYear();
            var month = today.getMonth();
            var day = today.getDate();
            var afternoon = new Date(year, month, day, "12", 0, 0, 0);

            $.get("{{ url('appointments/slots') }}", { date: date }).done(function( data ) {
                var timeSlots = "";
                var timeSlotsPm = "";
                var timeSlotsSt = "";
                var counter = 0;

                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {

                        var isDisabled = "";
                        var textColor = "";
                        var slotTime = data[i].slot_time;
                        var tmpAmPm = slotTime.split(' ');
                        var tmpTime = slotTime.split(':');
                        var timeSet = parseInt(tmpTime[0]);

                        if (tmpAmPm[1] == "PM" && timeSet != 12) {
                            timeSet += 12;
                        }
                        var temp_slot = new Date(year, month, day, timeSet, 0, 0, 0);

                        if (data[i].slot_count <= 1) {
                            isDisabled = "disabled";
                            textColor = "text-danger";
                            slotTime = "<s>" + slotTime + "</s>";
                        } else {
                            isDisabled = "";
                            textColor = "text-success";
                        }
                        @if(auth()->user()->isAdministrator() || auth()->user()->isSuperUser())
                            if (temp_slot >= afternoon) {
                                if (isDisabled != "disabled") {
                                    timeSlotsPm += '<div class="form-check form-check-radio" ' + isDisabled + '>' +
                                            '<label class="form-check-label ' + textColor + '">' +
                                            '<input class="form-check-input" type="radio" name="time-slot" value="' + data[i].id + '" ' + isDisabled + '>' +
                                            '<span class="form-check-sign"></span>' +
                                            slotTime +
                                            '</label>' +
                                            '</div>';

                                    counter = 1;
                                }
                            } else {
                                if (isDisabled != "disabled") {
                                    timeSlots += '<div class="form-check form-check-radio" ' + isDisabled + '>' +
                                            '<label class="form-check-label ' + textColor + '">' +
                                            '<input class="form-check-input" type="radio" name="time-slot" value="' + data[i].id + '" ' + isDisabled + '>' +
                                            '<span class="form-check-sign"></span>' +
                                            slotTime +
                                            '</label>' +
                                            '</div>';

                                    counter = 1;
                                }
                            }
                        @else
                            if (temp_slot >= afternoon){
                                if (isDisabled != "disabled") {
                                    timeSlotsPm += '<div class="form-check form-check-radio" ' + isDisabled + '>' +
                                            '<label class="form-check-label ' + textColor + '">' +
                                            '<input class="form-check-input" type="radio" name="time-slot" value="' + data[i].id + '" ' + isDisabled + '>' +
                                            '<span class="form-check-sign"></span>' +
                                            slotTime +
                                            '</label>' +
                                            '</div>';

                                    counter = 1;
                                    
                                }
                            } else {
                                if (isDisabled != "disabled") {
                                    timeSlots += '<div class="form-check form-check-radio" ' + isDisabled + '>' +
                                            '<label class="form-check-label ' + textColor + '">' +
                                            '<input class="form-check-input" type="radio" name="time-slot" value="' + data[i].id + '" ' + isDisabled + '>' +
                                            '<span class="form-check-sign"></span>' +
                                            slotTime +
                                            '</label>' +
                                            '</div>';

                                    counter = 1;
                                    
                                }
                            }
                        @endif
                    }
                    if (counter > 0) {
                        $("#book-now-submit").show();
                    } else {
                        $("#book-now-submit").hide();
                        if (dayOfWeek == "Sun") {
                            timeSlotsSt = "<div class='col-md-12'><p class='text-danger'>Closed</p></div>";
                        } else {
                            timeSlotsSt = "<div class='col-md-12'><p class='text-danger'>No available time slots</p></div>";
                        }
                    }
                } else {
                    $("#book-now-submit").hide();
                    if (dayOfWeek == "Sun") {
                        timeSlotsSt = "<div class='col-md-12'><p class='text-danger'>Closed</p></div>";
                    } else {
                        timeSlotsSt = "<div class='col-md-12'><p class='text-danger'>No available time slots</p></div>";
                    }
                }

                $('#time-slots').html(timeSlots);
                $('#time-slots-pm').html(timeSlotsPm);
                $('#time-slots-status').html(timeSlotsSt);
            });
        }

        function displayAppointments(date)
        {
            var d = moment(date).format('MMM-DD-YYYY');
            $('#appointments-title').html("Scheduled appointments: <b>" + d + "</b>");

            if ($('#appointments-table').length > 0) {
                $.get("{{ url('appointments/byDate') }}", {date: date}).done(function (data) {
                    var appointments = '<table class="table table-hover table-striped table-bordered ">' +
                            '<thead>' +
                            '<tr class="success d-flex">' +
                            '<th class="text-center col-1">#</th>' +
                            '<th class="text-center col-2">Name</th>' +
                            '<th class="text-center col-2">Date</th>' +
                            '<th class="text-center col-2">Time</th>' +
                            '<th class="text-center col-2">Contact No.</th>' +
                            '<th class="text-center col-2">Status</th>' +
                            '<th class="text-right col-1">Actions</th>' +
                            '</tr>' +
                            '</thead>' +
                            '<tbody>';


                    if (data.length > 0) {
                        for (var i = 0; i < data.length; i++) {

                            var appEditLink = "{{ url('appointments/edit') }}" + '/' + data[i].id;
                            var appShowScreen = "{{ url('appointments/showscreen') }}" + '/' + data[i].id;
                            var formattedDate = moment(data[i].time_slot.slot_date).format('MMM-DD-YYYY');
                            appointments += '<tr class="d-flex">' +
                                    '<td class="text-center col-1">' + (i + 1) + '</td>' +
                                    '<td class="text-center align-middle col-2">' + data[i].patient.first_name + '</td>' +
                                    '<td class="text-center col-2">' + formattedDate + '</td>' +
                                    '<td class="text-center col-2">' + data[i].time_slot.slot_time + '</td>' +
                                    '<td class="text-center col-2">' + data[i].patient.contact_number + '</td>' +
                                    '<td class="text-center col-2">' + data[i].status + '</td>' +
                                    '<td class="td-actions text-center col-1">' +
                                        '<a href="' + appEditLink + '" rel="tooltip" title="Edit Appointment" class="btn btn-info btn-link btn-xs">' +
                                            '<i class="fa fa-user"></i>' +
                                        '</a>' +
                                        '<a href="' + appShowScreen + '" rel="tooltip" title="View COVID-19 Screening Form" class="btn btn-success btn-link btn-xs">' +
                                            '<i class="fa fa-newspaper-o"></i>' +
                                        '</a>' +
                                    '</td>' +
                                    '</tr>';
                        }

                        appointments += '</tbody></table>';
                    } else {
                        appointments = "<div class='col-md-12'><p class='text-danger'>You have no appointments</p></div>";
                    }

                    $('#appointments-table').html(appointments);
                    $("[rel='tooltip']").tooltip();
                });
            }

        }

        $('.timepicker').datetimepicker({
            format: 'hh:mm',
        });

        $('#selectall').click(function(){
            $('input[id=timeslots]').prop('checked', true);
            $('input[id=deselectall]').prop('checked', false);  
        });
        $('#deselectall').click(function(){      
            $('input[id=timeslots]').prop('checked', false);
            $('input[id=selectall]').prop('checked', false);  
        });

        var endDatePicker = $("#endDate");
        endDatePicker.datetimepicker({
                viewMode: "years",
                format: 'YYYY-MM-DD'
        });

    </script>
@endsection