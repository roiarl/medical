@extends('layouts.dashboard')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @if ($errors->any())
                        <ul>
                        @foreach ($errors->all() as $message)
                            <li> {{ $message }} </li>
                        @endforeach
                        </ul>
                    @endif
                    <div class="card stacked-form">
                        <div class="card-header text-center" style="background-color:purple">
                            <h4 class="card-title text-light pb-2" ><strong>Medical Prescription<strong></h4>
                        </div>
                        @if($prescription)
                        <form method="POST" action="{{ url('appointments/prescriptionupdate', ['id' => $prescription->id, 'referer' => $referer]) }}">
                            @csrf
                            <input type="hidden" id="birthdate" name="birthdate" value="{{ $prescription->patient->birthday }}">
                            @if(!auth()->user()->isAdministrator() && !auth()->user()->isSuperUser())
                            <fieldset disabled="disabled">
                            @endif
                                <div class="card-body ">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label class="text-dark"><strong>Doctor's Name</strong> </label>
                                                <input type="text" placeholder="Doctor's Name" value="{{ $prescription->doctor_name }}" name="doctor_name" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-dark"><strong>License No.</strong> </label>
                                                <input type="text" placeholder="License No." value="{{ $prescription->license_no }}" name="license_no" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="text-dark"><strong>Signature</strong> </label>
                                                <input type="text" placeholder="Signature" value="{{ $prescription->signature }}" name="signature" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body ">
                                    <hr style="border: 3px solid green;border-radius: 5px;">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="text-dark"><strong>First Name</strong> </label>
                                                <input type="text" placeholder="First Name" value="{{ old('first_name', $prescription->patient->first_name) }}" name="first_name" class="form-control @error('first_name') is-invalid @enderror" readonly>
                                                @error('first_name')
                                                    <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="text-dark"><strong>Last Name</strong> </label>
                                                <input type="text" placeholder="Last Name" value="{{ old('last_name', $prescription->patient->last_name) }}" name="last_name" class="form-control @error('last_name') is-invalid @enderror" readonly>
                                                @error('last_name')
                                                    <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="text-dark"><strong>Age</strong></label>
                                                <input type="text" placeholder="Age" value="" id="age" name="age" class="form-control @error('age') is-invalid @enderror" readonly>
                                                @error('age')
                                                    <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="text-dark"><strong>Date</strong> </label>
                                                <input type="date" placeholder="date" value="{{ old('date', $prescription->date) }}" name="date" class="form-control @error('date') is-invalid @enderror" readonly>
                                                @error('date')
                                                    <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="text-dark"><strong>Patient Address</strong></label>
                                                <input type="text" placeholder="Address" value="{{ old('address', $prescription->patient->address) }}" name="address" class="form-control @error('address') is-invalid @enderror" readonly>
                                                @error('address')
                                                    <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <hr style="border: 3px solid green;border-radius: 5px;">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group" style="font-size: 40px;">Rx
                                                <textarea rows="15" class="form-control"  name="rx" required>{{ $prescription->rx }} </textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group" style="font-size: 20px;">Sig
                                                <textarea rows="5" class="form-control" name="sig" required>{{ $prescription->sig }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group" style="font-size: 20px;">Dispensing Instructions
                                                <textarea rows="5" class="form-control" name="dispensing_instructions">{{ $prescription->dispensing_instructions }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @if(!auth()->user()->isAdministrator() && !auth()->user()->isSuperUser())
                            </fieldset>
                            @endif
                            <div class="card-footer justify-content-right">
                                @if(auth()->user()->isAdministrator() || auth()->user()->isSuperUser())
                                    <button type="submit" class="btn btn-fill btn-primary" name="action" value="updateConsent">Update</button>
                                @endif
                                <a href="javascript:history.back()" class="btn btn-fill btn-default">Back</a>
                            </div>
                        </form>
                        @else
                            <div class="card-body ">
                                No Prescription Form. <p>Click <a href="{{ url ('appointments/prescriptionform', ['id' => $patient->id, 'referer' => $referer]) }}" data-form-screen> here </a>to fill-out the form</p>
                            </div>
                            <div class="card-footer justify-content-right">
                                <a href="javascript:history.back()" class="btn btn-fill btn-default">Back</a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">

        document.addEventListener('DOMContentLoaded', function() {
            //var nowDate = new Date().getFullYear();
            var bDate = new Date($('#birthdate').val()).getFullYear();
            var today = new Date();

            if(isNaN(bDate)) {
                sessionStorage.removeItem('bdate');
            } else {
                var birthDateValue = sessionStorage.getItem('bdate');
                var birthDate = new Date(birthDateValue);

                var age = today.getFullYear() - birthDate.getFullYear();
                var m = today.getMonth() - birthDate.getMonth();

                if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                    age--;
                }

                $('#age').val(age);
            }
            //var a = nowDate - bDate;
            //$('#age').val(a);
        });
    </script>
@endsection