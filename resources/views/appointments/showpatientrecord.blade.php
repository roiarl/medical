@extends('layouts.dashboard')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @if ($errors->any())
                        <ul>
                        @foreach ($errors->all() as $message)
                            <li> {{ $message }} </li>
                        @endforeach
                        </ul>
                    @endif
                    <div class="card stacked-form">
                        <div class="card-header text-center" style="background-color:purple">
                            <h4 class="card-title text-light pb-2" ><strong>Patient Record<strong></h4>
                        </div>
                        <form method="POST" action="{{ url('appointments/patientrecordupdate', ['id' => $patientRecord->id, 'referer' => $referer]) }}">
                            @csrf
                        @if($patientRecord)
                            <input type="hidden" id="birthdate" name="birthdate" value="{{ $patientRecord->patient->birthday }}">
                            @if(!auth()->user()->isAdministrator() && !auth()->user()->isSuperUser())
                            <fieldset disabled="disabled">
                            @endif
                                <div class="card-body ">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="text-dark"><strong>First Name</strong> </label>
                                                <input type="text" placeholder="First Name" value="{{ old('first_name', $patientRecord->patient->first_name) }}" name="first_name" class="form-control" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="text-dark"><strong>Last Name</strong> </label>
                                                <input type="text" placeholder="Last Name" value="{{ old('last_name', $patientRecord->patient->last_name) }}" name="last_name" class="form-control" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="text-dark"><strong>Age</strong></label>
                                                <input type="text" placeholder="Age" value="" id="age" name="age" class="form-control" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="text-dark"><strong>Date</strong> </label>
                                                <input type="date" placeholder="date" value="{{ old('date', $patientRecord->date) }}" name="date" class="form-control" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="text-dark"><strong>Reason of last visit</strong></label>
                                                <textarea type="text" placeholder="Reason of last visit" value="" name="reason" class="form-control" readonly>{{ $patientRecord->reason }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <hr style="border: 3px solid green;border-radius: 5px;">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">Doctor's Notes
                                                <textarea rows="15" class="form-control"  name="notes" required>{{ $patientRecord->notes }} </textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @if(!auth()->user()->isAdministrator() && !auth()->user()->isSuperUser())
                            </fieldset>
                            @endif
                            <div class="card-footer justify-content-right">
                                @if(auth()->user()->isAdministrator() || auth()->user()->isSuperUser())
                                    <button type="submit" class="btn btn-fill btn-primary" name="action" value="updateConsent">Update</button>
                                @endif
                                <a href="{{ url()->previous() }}" class="btn btn-fill btn-default">Back</a>
                            </div>
                        @else
                            <div class="card-body ">
                                No Prescription Form. <p>Click <a href="{{ url ('appointments/prescriptionform', ['id' => $patient->id]) }}" data-form-screen> here </a>to fill-out the form</p>
                            </div>
                            <div class="card-footer justify-content-right">
                                <a href="{{ url()->previous() }}" class="btn btn-fill btn-default">Back</a>
                            </div>
                        @endif
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">

        document.addEventListener('DOMContentLoaded', function() {
            //var nowDate = new Date().getFullYear();
            var bDate = new Date($('#birthdate').val()).getFullYear();
            var today = new Date();

            if(isNaN(bDate)) {
                sessionStorage.removeItem('bdate');
            } else {
                var birthDateValue = sessionStorage.getItem('bdate');
                var birthDate = new Date(birthDateValue);

                var age = today.getFullYear() - birthDate.getFullYear();
                var m = today.getMonth() - birthDate.getMonth();

                if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                    age--;
                }

                $('#age').val(age);
            }
            //var a = nowDate - bDate;
            //$('#age').val(a);
        });
    </script>
@endsection