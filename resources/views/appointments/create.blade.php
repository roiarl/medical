@extends('layouts.dashboard')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="POST" action="{{ url('appointments/store') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="card">
                            @if ($errors)
                            <div class="card-header">
                                <p class="error" style="color:red">{{ $errors->first() }}</p>
                            </div>
                            @endif
                            @if(session()->has('doubleBooking'))
                                <div class="alert alert-success">
                                    <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                                        <i class="nc-icon nc-simple-remove"></i>
                                    </button>
                                    <span style="color:red;">
                                        <b>
                                            {{ session('doubleBooking') }}
                                        </b>
                                    </span>
                                </div>
                            @endif
                            <div class="card-header ">
                                <h4 class="card-title">Patient Details</h4>
                            </div>
                            <div class="card-body ">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">First Name <star class="star">*</star></label>
                                            <input type="text" placeholder="First Name" value="{{ old('first_name', app('request')->input('first_name')) }}" name="first_name" class="form-control @error('first_name') is-invalid @enderror" required>
                                            @error('first_name')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Last Name <star class="star">*</star></label>
                                            <input type="text" placeholder="Last Name" value="{{ old('last_name', app('request')->input('last_name')) }}" name="last_name" class="form-control @error('last_name') is-invalid @enderror" required>
                                            @error('last_name')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Middle Name </label>
                                            <input type="text" placeholder="Middle Name" value="{{ old('middle_name', app('request')->input('middle_name')) }}" name="middle_name" class="form-control @error('middle_name') is-invalid @enderror">
                                            @error('middle_name')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Email <star class="star">*</star></label>
                                            <input type="email" placeholder="Email" value="{{ old('email', app('request')->input('email')) }}" name="email" class="form-control @error('email') is-invalid @enderror" required>
                                            @error('email')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-body">Contact Number <star class="star">*</star></label>
                                            <input type="number" placeholder="Contact No." oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength = "11" value="{{ old('contact_number', app('request')->input('contact_number')) }}" name="contact_number" class="form-control @error('contact_number') is-invalid @enderror" required>
                                            @error('contact_number')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="birth_date" class="text-body">Birthdate <star class="star">*</star></label>
                                            <div class='input-group date' id='birth_date'>
                                                <input type='text' value="{{ old('birthdate', app('request')->input('birthdate')) }}" id="bd-picker" class="form-control datepicker @error('birthdate') is-invalid @enderror" name="birthdate" placeholder="YYYY-MM-DD" required/>
                                                <label class="input-group-append input-group-text" for="bd-picker" style="margin:inherit;border-radius:1px;">
                                                    <span class="fa fa-calendar"></span>
                                                </label>
                                            </div>
                                            @error('birthdate')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="gender" class="text-body">Gender <star class="star">*</star></label>
                                            <select class="form-control @error('gender') is-invalid @enderror" name="gender" id="gender" required>
                                            @if(app('request')->input('gender') == "Male")
                                                <option value="">-- Select --</option>
                                                <option value="Male" selected>Male</option>
                                                <option value="Female" >Female</option>
                                            @elseif( app('request')->input('gender') == "Female")
                                                <option value="">-- Select --</option>
                                                <Male value="Male">Male</option>
                                                <option value="Female" selected>Female</option>
                                            @else
                                                <option value="">-- Select --</option>
                                                <option value="Male">Male</option>
                                                <option value="Female">Female</option>
                                            @endif
                                            </select>
                                            @error('gender')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-body">Insurance</label>
                                            <input type="text" placeholder="Insurance" value="{{ old('insurance', app('request')->input('insurance')) }}" name="insurance" class="form-control @error('insurance') is-invalid @enderror">
                                            @error('insurance')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <label class="text-body">Address <star class="star">*</star></label>
                                            <input type="text" placeholder="Address" value="{{ old('address', app('request')->input('address')) }}" name="address" class="form-control @error('address') is-invalid @enderror" required>
                                            @error('address')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label class="text-body">Company Name </label>
                                            <input type="text" placeholder="Company Name" value="{{ old('company_name', app('request')->input('company_name')) }}" name="company_name" class="form-control @error('company_name') is-invalid @enderror">
                                            @error('company_name')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <label class="text-body">Company Address </label>
                                            <input type="text" placeholder="Company Address" value="{{ old('company_address', app('request')->input('company_address')) }}" name="company_address" class="form-control @error('company_address') is-invalid @enderror">
                                            @error('company_address')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="requirements" class="text-body">Describe the reason/purpose of your visit/consultation<star class="star">*</star></label>
                                            <textarea class="form-control @error('requirements') is-invalid @enderror" id="requirements" name="requirements" value="{{ old('requirements', app('request')->input('requirements')) }}" rows="3" required></textarea>
                                            @error('requirements')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="medical_history" class="text-body">Medical History </label>
                                        <div class="row" id="medical_history">
                                            <div class="form-check checkbox-inline">
                                                <label class="form-check-label text-body">
                                                    <input class="form-check-input" type="checkbox" name="medical_history[]" value="1" <?php echo (((app('request')->input('medical_history')&1) == 1) ? 'checked' : '');?>>
                                                    <span class="form-check-sign"></span>
                                                    Diabetes
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="form-check-label text-body">
                                                    <input class="form-check-input" type="checkbox" name="medical_history[]" value="8" <?php echo (((app('request')->input('medical_history')&8) == 8) ? 'checked' : '');?>>
                                                    <span class="form-check-sign"></span>
                                                    Heart Condition
                                                </label>
                                            </div>
                                            <div class="form-check checkbox-inline">
                                                <label class="form-check-label text-body">
                                                    <input class="form-check-input" type="checkbox" name="medical_history[]" value="32" <?php echo (((app('request')->input('medical_history')&32) == 32) ? 'checked' : '');?>>
                                                    <span class="form-check-sign"></span>
                                                    Blood Pressure
                                                </label>
                                            </div>
                                            {{--<div class="form-check" style="font-size:12px"> Allergies: 
                                                <input type="text" value="" name="allergies"  id="allergies" style="border-style: none none solid none;">
                                            </div>--}}
                                            <div class="form-check checkbox-inline">
                                                <input type="text" placeholder="Others" value="" name="medical_history_others" class="form-control">
                                            </div>
                                        </div>
                                        <div class="row" id="medical_history">
                                            <div class="form-check" style="font-size:12px;"> &nbsp;&nbsp;&nbsp;&nbsp;Allergies: 
                                                <input type='text' style="border-style: none none solid none;width:236px;" id="allergies" value="{{ old('allergies', app('request')->input('allergies')) }}" name="allergies"/>
                                            </div>
                                        </div><br>
                                        <div class="row" id="medical_history">
                                            <div class="form-check" style="font-size:12px;"> &nbsp;&nbsp;&nbsp;&nbsp;Date of Last Consultation: 
                                                <input type='text' style="border-style: none none solid none;width:146px;" id="consultation_picker" name="last_consultation" placeholder="YYYY-MM-DD"/>
                                            </div>
                                            <div class="form-check" style="font-size:12px"> Purpose/Reason: 
                                                <input type='text' style="border-style: none none solid none;width:165px;" value="{{ old('last_consultation_reason', app('request')->input('last_consultation_reason')) }}" name="last_consultation_reason"/>
                                            </div>
                                        </div><br>
                                        <div class="row" id="medical_history">
                                            <div class="form-check" style="font-size:12px;"> &nbsp;&nbsp;&nbsp;&nbsp;Date of Last Hospitalization: 
                                                <input type='text' style="border-style: none none solid none;" id="hospitalization_picker" name="last_hospitalization" placeholder="YYYY-MM-DD"/>
                                            </div>
                                            <div class="form-check" style="font-size:12px"> Purpose/Reason: 
                                                <input type='text' style="border-style: none none solid none;width:165px;" value="{{ old('last_hospitalization_reason', app('request')->input('last_hospitalization_reason')) }}" name="last_hospitalization_reason"/>
                                            </div>
                                        </div>
                                    </div>
                                </div><br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="medical_history" class="text-body">Medicines Taken, if any: </label>
                                        <table class="table table-hover bg-light">
                                            <thead>
                                                <th>Medicine Name</th>
                                                <th>Date</th>
                                                <th>Time</th>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><input type="text" placeholder="Name" id="med_name1" value="{{ old('med_name.0', app('request')->input('med_name1')) }}" name="med_name[]" class="form-control">
                                                    <td><input type="date" placeholder="YYYY-MM-DD" id="med_take_date1" value="{{ old('med_take_date.0', app('request')->input('med_take_date1')) }}" name="med_take_date[]" id="med_taken_date1" class="form-control"></td>
                                                    <td><input type="time" placeholder="HH::MM AM/PM e.g. 11:00 AM" id="med_take_time1" value="{{ old('med_take_time.0', app('request')->input('med_take_time1')) }}" name="med_take_time[]" class="form-control"></td>
                                                </tr>
                                                <tr>
                                                    <td><input type="text" placeholder="Name" id="med_name2" value="{{ old('med_name.1', app('request')->input('med_name2')) }}" name="med_name[]" class="form-control"></td>
                                                    <td><input type="date" placeholder="YYYY-MM-DD" id="med_take_date2" value="{{ old('med_take_date.1', app('request')->input('med_take_date2')) }}" name="med_take_date[]" id="med_taken_date2" class="form-control"></td>
                                                    <td><input type="time" placeholder="HH::MM AM/PM e.g. 11:00 AM" id="med_take_time2" value="{{ old('med_take_time.1', app('request')->input('med_take_time2')) }}" name="med_take_time[]" class="form-control"></td>
                                                </tr>
                                                <tr>
                                                    <td><input type="text" placeholder="Name" id="med_name3" value="{{ old('med_name.2', app('request')->input('med_name3')) }}" name="med_name[]" class="form-control"></td>
                                                    <td><input type="date" placeholder="YYYY-MM-DD" id="med_take_date3" value="{{ old('med_take_date.2', app('request')->input('med_take_date3')) }}" name="med_take_date[]" id="med_taken_date3" class="form-control"></td>
                                                    <td><input type="time" placeholder="HH::MM AM/PM e.g. 11:00 AM" id="med_take_time3" value="{{ old('med_take_time.2', app('request')->input('med_take_time3')) }}" name="med_take_time[]" class="form-control"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="medical_history" class="text-body">Maintenance Medicines, if any: </label>
                                        <table class="table table-hover bg-light">
                                            <thead>
                                                <th>Medicine Name</th>
                                                <th>Frequency</th>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><input type="text" placeholder="Name" id="maintain_med_name1" value="{{ old('maintain_med_name.0') }}" name="maintain_med_name[]" class="form-control"></td>
                                                    <td><input type="text" placeholder="Frequency" id="frequency1" value="{{ old('maintain_frequency.0') }}" name="maintain_frequency[]" class="form-control"></td>
                                                </tr>
                                                <tr>
                                                    <td><input type="text" placeholder="Name" id="maintain_med_name2" value="{{ old('maintain_med_name.1') }}" name="maintain_med_name[]" class="form-control"></td>
                                                    <td><input type="text" placeholder="Frequency" id="frequency2" value="{{ old('maintain_frequency.1') }}" name="maintain_frequency[]" class="form-control"></td>
                                                </tr>
                                                <tr>
                                                    <td><input type="text" placeholder="Name" id="maintain_med_name3" value="{{ old('maintain_med_name.2') }}" name="maintain_med_name[]" class="form-control"> </td>
                                                    <td><input type="text" placeholder="Frequency" id="frequency3" value="{{ old('maintain_frequency.2') }}" name="maintain_frequency[]" class="form-control"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                {{--<div class="row">--}}
                                    {{--<div class="col-md-4">--}}
                                        {{--<div class="form-group">--}}
                                            {{--<label for="doctors">Assigned Doctor <star class="star">*</star></label>--}}
                                            {{--<select class="form-control @error('doctor_id') is-invalid @enderror" name="doctor_id" id="doctors">--}}
                                                {{--<option value="">-- Select --</option>--}}
                                                {{--@if($doctors)--}}
                                                    {{--@foreach($doctors as $doctor)--}}
                                                        {{--<option value="{{ $doctor->id }}" {{ old('doctor_id') == $doctor->id ? 'selected' : '' }}>{{ $doctor->first_name . ' ' . $doctor->last_name }}</option>--}}
                                                    {{--@endforeach--}}
                                                {{--@endif--}}
                                            {{--</select>--}}
                                            {{--@error('doctor_id')--}}
                                                {{--<p class="text-danger">{{ $message }}</p>--}}
                                            {{--@enderror--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-4">--}}
                                        {{--<div class="form-group">--}}
                                            {{--<label>Referred by</label>--}}
                                            {{--<input type="text" placeholder="Name" value="{{ old('referred_by') }}" name="referred_by" class="form-control">--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="row">--}}
                                    {{--<div class="col-md-4">--}}
                                        {{--<div class="form-group">--}}
                                            {{--<label for="services">Service <star class="star">*</star></label>--}}
                                            {{--<select class="form-control @error('service_id') is-invalid @enderror" name="service_id" id="services">--}}
                                                {{--<option value="">-- Select --</option>--}}
                                                {{--@if($services)--}}
                                                    {{--@foreach($services as $service)--}}
                                                        {{--<option value="{{ $service->id }}" {{ old('service_id') == $service->id ? 'selected' : '' }}>{{ $service->name }}</option>--}}
                                                    {{--@endforeach--}}
                                                {{--@endif--}}
                                            {{--</select>--}}
                                            {{--@error('service_id')--}}
                                                {{--<p class="text-danger">{{ $message }}</p>--}}
                                            {{--@enderror--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="row">--}}
                                    {{--<div class="form-group stacked-form">--}}
                                        {{--<div class="form-check">--}}
                                            {{--<label class="form-check-label">--}}
                                                {{--<input class="form-check-input" type="checkbox" name="is_waitlist" value="1" {{ old('is_waitlist') == 1 ? 'checked' : '' }}>--}}
                                                {{--<span class="form-check-sign"></span>--}}
                                                {{--<label>Add to Waitlist</label>--}}
                                            {{--</label>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                <hr>
                                <h4 class="card-title">Appointment Schedule</h4>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="schedule-date">Date of appointment. <star class="star">*</star></label>
                                            <div class='input-group date' id='schedule-date'>
                                                <input type='text' value="{{ old('schedule_date', $date) }}" id="scheduledate-picker" class="form-control datepicker @error('schedule_date') is-invalid @enderror" name="schedule_date" placeholder="YYYY-MM-DD" required/>
                                                <label class="input-group-append input-group-text" for="scheduledate-picker" style="margin:inherit;border-radius:1px;">
                                                    <span class="fa fa-calendar"></span>
                                                </label>
                                            </div>
                                            <div class='input-group date'>
                                                <input type='hidden' value="{{ $date }}" id="selectedDate" class="form-control datepicker" name="selectedDate"/>
                                            </div>
                                            @error('schedule_date')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                    </div>
                                    <div class="col-md-6" >
                                        <div class="card w-75 h-100  border border-danger" style="width: 18rem; background-color: #ffffb3">
                                            <div class="text-center font-weight-bold mt-2"> Please read these forms and</div>
                                            <div class="text-center"><p class="font-weight-bold">indicate your confirmation.</p></div>
                                            <div class="text-center">
                                            @if(session()->has('screeningFormData'))
                                                <a class="btn" href="{{ url ('appointments/screeningform') }}" data-form-screen style="border:none; background-color: #ffffb3; color:green">
                                                    <strong>COVID-19 Screening Form</strong>        
                                                </a><i class="fa fa-check-square-o"></i>
                                            @else
                                                <a class="btn" href="{{ url ('appointments/screeningform') }}" data-form-screen style="border:none; background-color: #ffffb3; color:blue">
                                                    <strong>COVID-19 Screening Form</strong>
                                                </a>
                                            @endif
                                            </div>
                                            {{--<div class="text-center">
                                            @if(session()->has('consentFormData'))
                                                <a class="btn" href="{{ url('appointments/consentform') }}" data-form-consent style="border:none; background-color: #ffffb3; color:green">
                                                    <strong>COVID-19 Pandemic Dental Consent Form</strong>
                                                </a><i class="fa fa-check-square-o"></i>
                                            @else
                                                <a class="btn" href="{{ url('appointments/consentform') }}" data-form-consent style="border:none; background-color: #ffffb3; color:blue">
                                                    <strong>COVID-19 Pandemic Dental Consent Form</strong>
                                                </a>
                                            @endif
                                            </div>--}}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="time-slots">Time <star class="star">*</star></label>
                                        <div class="row" id="time-slots" required></div>
                                        <div class="row" id="time-slots-pm"></div>
                                        @error('schedule_time')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="image">Upload an image</label>
                                    <div class="input-group">
                                        <input type="file" name="image">
                                    </div>
                                    @error('image')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="card-footer ">
                                <button type="submit" name="action" id="create" class="btn btn-fill btn-success" value="create">Create</button>
                                <a href="{{ url('appointments/index') }}" class="btn btn-fill btn-danger">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">

        document.addEventListener('DOMContentLoaded', function() {

            // date today
            var date = new Date();
            var dateNowFormatted = formatDate(date);

            sessionStorage.removeItem('UVWXYZ');
            sessionStorage.removeItem('PQRST');
            sessionStorage.removeItem('KLMNO');
            sessionStorage.removeItem('FGHIJ');
            sessionStorage.removeItem('ABCDE');
            
            // Date picker set
            var scheduledatePicker = $("#scheduledate-picker");
            var birthdatePicker = $("#bd-picker");
            var consultationPicker = $("#consultation_picker");
            var hospitalizationPicker = $("#hospitalization_picker");

            if (sessionStorage.getItem('selectedDate') === null || sessionStorage.getItem('selectedDate') === "") {
                var selectedDate = dateNowFormatted;
            } else {
                var selectedDate = sessionStorage.getItem('selectedDate');
            }

            // check if selected date is before today's date
            if (selectedDate < dateNowFormatted) {
                var selectedDate = dateNowFormatted;
            }

            // set scheduledate-picker latest selected date
            $('#scheduledate-picker').val(selectedDate);

            // initialize time slots
            //displayTimeSlots(scheduledatePicker.val());
            displayTimeSlots(selectedDate);

            // remove selectedDate in sessionStorage
            //sessionStorage.removeItem('selectedDate');

            scheduledatePicker.datetimepicker({
                format: 'YYYY-MM-DD',
                minDate: moment().format("YYYY-MM-DD")
            });

            scheduledatePicker.on('dp.change', function() {
                // assign scheduledatePicker.val() to selectedDate field
                var selectedDate = $("#selectedDate").val(scheduledatePicker.val());

                // set selectedDate to sessionStorage
                sessionStorage.setItem('selectedDate', selectedDate.val());

                // get selectedDate to sessionStorage
                var selectedDate = sessionStorage.getItem('selectedDate');
                
                //displayTimeSlots(scheduledatePicker.val());
                displayTimeSlots(selectedDate);
            });

            birthdatePicker.datetimepicker({
                viewMode: "years",
                format: 'YYYY-MM-DD'
            });

            consultationPicker.datetimepicker({
                viewMode: "years",
                format: 'YYYY-MM-DD'
            });

            hospitalizationPicker.datetimepicker({
                viewMode: "years",
                format: 'YYYY-MM-DD'
            });

            if (sessionStorage.getItem('bdate') === null || sessionStorage.getItem('bdate') === "") {
                var bdatePicker = birthdatePicker.val();
            } else {
                var value = sessionStorage.getItem('bdate');
                var bdatePicker = birthdatePicker.val(value);
            }

            var value = sessionStorage.getItem('med_name1');
            $('#med_name1').val(value);
            value = sessionStorage.getItem('med_name2');
            $('#med_name2').val(value);
            value = sessionStorage.getItem('med_name3');
            $('#med_name3').val(value);
            value = sessionStorage.getItem('med_take_date1');
            $('#med_take_date1').val(value);
            value = sessionStorage.getItem('med_take_date2');
            $('#med_take_date2').val(value);
            value = sessionStorage.getItem('med_take_date3');
            $('#med_take_date3').val(value);
            value = sessionStorage.getItem('med_take_time1');
            $('#med_take_time1').val(value);
            value = sessionStorage.getItem('med_take_time2');
            $('#med_take_time2').val(value);
            value = sessionStorage.getItem('med_take_time3');
            $('#med_take_time3').val(value);

            value = sessionStorage.getItem('maintain_med_name1');
            $('#maintain_med_name1').val(value);
            value = sessionStorage.getItem('maintain_med_name2');
            $('#maintain_med_name2').val(value);
            value = sessionStorage.getItem('maintain_med_name3');
            $('#maintain_med_name3').val(value);
            value = sessionStorage.getItem('frequency1');
            $('#frequency1').val(value);
            value = sessionStorage.getItem('frequency2');
            $('#frequency2').val(value);
            value = sessionStorage.getItem('frequency3');
            $('#frequency3').val(value);

            value = sessionStorage.getItem('bdate');
            $('#bd-picker').val(value);
            value = sessionStorage.getItem('last_consultation');
            $('#consultation_picker').val(value);
            value = sessionStorage.getItem('last_hospitalization');
            $('#hospitalization_picker').val(value);
        });

        function handleClick(myRadio) {
            sessionStorage.setItem('selectedtime', myRadio.value);
        }

        function formatDate(date) {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) 
                month = '0' + month;

            if (day.length < 2) 
                day = '0' + day;

            return [year, month, day].join('-');
        }

        function displayTimeSlots(date)
        {
            $('#time-slot-title').html("Time Slots for " + date);

            var oldScheduleTime = "{{ old('schedule_time', $time) }}";
            var today = new Date();
            var year = today.getFullYear();
            var month = today.getMonth();
            var day = today.getDate();
            var afternoon = new Date(year, month, day, "12", 0, 0, 0);

            $.get("{{ url('appointments/slots') }}", { date: date }).done(function( data ) {
                var timeSlots = "";
                var timeSlotsPm = "";
                var counter = 0;

                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        var isChecked = "";
                        var isDisabled = "";
                        var textColor = "";
                        var slotTime = data[i].slot_time;
                        var tmpAmPm = slotTime.split(' ');
                        var tmpTime = slotTime.split(':');
                        var timeSet = parseInt(tmpTime[0]);

                        if (tmpAmPm[1] == "PM" && timeSet != 12) {
                            timeSet += 12;
                        }
                        var temp_slot = new Date(year, month, day, timeSet, 0, 0, 0);

                        if (oldScheduleTime == data[i].id) {
                            isChecked = "checked";
                        }

                        if (sessionStorage.getItem('selectedtime') == data[i].id) {
                            isChecked = "checked";
                        }

                        if (data[i].slot_count <= 1) {
                            isDisabled = "disabled";
                            textColor = "text-danger";
                            slotTime = "<s>" + slotTime + "</s>";
                        } else {
                            isDisabled = "";
                            textColor = "text-success";
                        }
                    @if(auth()->user()->isAdministrator() || auth()->user()->isSuperUser())
                        if (isDisabled != "disabled") {
                            if (temp_slot >= afternoon) {
                                timeSlotsPm += '<div class="form-check form-check-radio" ' + isDisabled + '>' +
                                '<label class="form-check-label ' + textColor + '">' +
                                '<input class="form-check-input" type="radio" onclick="handleClick(this)" name="schedule_time" value="' + data[i].id + '" ' + isChecked + ' ' + isDisabled + '>' +
                                '<span class="form-check-sign"></span>' +
                                slotTime +
                                '</label>' +
                                '</div>';
                            } else {
                                timeSlots += '<div class="form-check form-check-radio" ' + isDisabled + '>' +
                                '<label class="form-check-label ' + textColor + '">' +
                                '<input class="form-check-input" type="radio" onclick="handleClick(this)" name="schedule_time" value="' + data[i].id + '" ' + isChecked + ' ' + isDisabled + '>' +
                                '<span class="form-check-sign"></span>' +
                                slotTime +
                                '</label>' +
                                '</div>';
                            }
                            counter = 1;
                        }
                    @else
                        if (isDisabled != "disabled") {
                            if (temp_slot >= afternoon) {

                                timeSlotsPm += '<div class="form-check form-check-radio" ' + isDisabled + '>' +
                                '<label class="form-check-label ' + textColor + '">' +
                                '<input class="form-check-input" type="radio" onclick="handleClick(this)" name="schedule_time" value="' + data[i].id + '" ' + isChecked + ' ' + isDisabled + '>' +
                                '<span class="form-check-sign"></span>' +
                                slotTime +
                                '</label>' +
                                '</div>';
                            } else {
                                timeSlots += '<div class="form-check form-check-radio" ' + isDisabled + '>' +
                                '<label class="form-check-label ' + textColor + '">' +
                                '<input class="form-check-input" type="radio" onclick="handleClick(this)" name="schedule_time" value="' + data[i].id + '" ' + isChecked + ' ' + isDisabled + '>' +
                                '<span class="form-check-sign"></span>' +
                                slotTime +
                                '</label>' +
                                '</div>';
                            }

                            counter = 1;
                        }
                    @endif
                    }

                    if (counter < 1) {
                        timeSlots = "<div class='col-md-12'><p class='text-danger'>No available time slots</p></div>";
                    }

                } else {
                    timeSlots = "<div class='col-md-12'><p class='text-danger'>No available time slots</p></div>";
                }

                $('#time-slots').html(timeSlots);
                $('#time-slots-pm').html(timeSlotsPm);
            });
        }

        $(function()
        {
            var formAttach = $('[data-form-screen]');
            var formAttachLink = $('[data-form-screen]').attr('href');
            var paramStr = '';
            var attachFlag = false;
            var medHist = 0;

            var getParamData = function(key)
            {
                return (paramStr.split(key + '=')[1] || '').split('&')[0];
            };
            var updateParamData = function(key, val)
            {
                var keyVal = transformParamData(key, val);
                var oldParamStr = transformParamData(key, getParamData(key));
                return paramStr.indexOf(key) !== -1 ? paramStr.replace(oldParamStr, keyVal) : paramStr += keyVal;
            };
            var transformParamData = function(key, val)
            {
                return '&' + key + '=' + val;
            };

            $('input[type="text"], input[type="email"], input[type="number"], input[type="checkbox"], textarea, select, option').each(function()
            {
                var self = $(this);
                self.on('change', function()
                {
                    attachFlag = true;
                    if (self.attr('name') == "medical_history[]") {
                        if(self.is(':checked')) {
                            medHist |= self.val();
                        }else{
                            medHist -= self.val();
                            if (medHist < 0){
                                medHist = 0;
                            }
                        }
                        sessionStorage.setItem('medical_history', medHist);
                    }else{
                        formAttach.attr('href', formAttachLink + '?' + updateParamData(self.attr('name'), self.val()));
                    }
                });
                if (!attachFlag) {
                    if (self.attr('name') == "medical_history[]") {
                        if(self.is(':checked')) {
                            medHist |= self.val();
                        }else{
                            medHist -= self.val();
                            if (medHist < 0){
                                medHist = 0;
                            }
                        }
                    }else{
                        if (self.val() != '') {
                            formAttach.attr('href', formAttachLink + '?' + updateParamData(self.attr('name'), self.val()));
                        }
                    }
                }
                self.on('dp.change', function()
                {
                    var field_id = "#" + self.attr('id');
                    if (self.attr('id') == "bd-picker") {
                        var birthDateValue = $(field_id).val();
                        var today = new Date();
                        var birthDate = new Date(birthDateValue);
                        sessionStorage.setItem('bdate', birthDateValue);
                    } else {
                        sessionStorage.setItem(self.attr('name'), $(field_id).val());
                    }
                })
            });
            // Medicines Taken
            $('#med_name1').on('change', function(e){
                sessionStorage.setItem('med_name1', $('#med_name1').val());
            });
            $('#med_name2').on('change', function(e){
                sessionStorage.setItem('med_name2', $('#med_name2').val());
            });
            $('#med_name3').on('change', function(e){
                sessionStorage.setItem('med_name3', $('#med_name3').val());
            });
            $('#med_take_date1').on('change', function(e){
                sessionStorage.setItem('med_take_date1', $('#med_take_date1').val());
            });
            $('#med_take_date2').on('change', function(e){
                sessionStorage.setItem('med_take_date2', $('#med_take_date2').val());
            });
            $('#med_take_date3').on('change', function(e){
                sessionStorage.setItem('med_take_date3', $('#med_take_date3').val());
            });
            $('#med_take_time1').on('change', function(e){
                sessionStorage.setItem('med_take_time1', $('#med_take_time1').val());
            });
            $('#med_take_time2').on('change', function(e){
                sessionStorage.setItem('med_take_time2', $('#med_take_time2').val());
            });
            $('#med_take_time3').on('change', function(e){
                sessionStorage.setItem('med_take_time3', $('#med_take_time3').val());
            });

            // Maintenance Medicine
            $('#maintain_med_name1').on('change', function(e){
                sessionStorage.setItem('maintain_med_name1', $('#maintain_med_name1').val());
            });
            $('#maintain_med_name2').on('change', function(e){
                sessionStorage.setItem('maintain_med_name2', $('#maintain_med_name2').val());
            });
            $('#maintain_med_name3').on('change', function(e){
                sessionStorage.setItem('maintain_med_name3', $('#maintain_med_name3').val());
            });
            $('#frequency1').on('change', function(e){
                sessionStorage.setItem('frequency1', $('#frequency1').val());
            });
            $('#frequency2').on('change', function(e){
                sessionStorage.setItem('frequency2', $('#frequency2').val());
            });
            $('#frequency3').on('change', function(e){
                sessionStorage.setItem('frequency3', $('#frequency3').val());
            });
        });

        $('#create').on("click", function() {
            sessionStorage.clear();
        })

    </script>

@endsection