@extends('layouts.dashboard')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                                <i class="nc-icon nc-simple-remove"></i>
                            </button>
                            <span>
                                <b>
                                    {{ session('message') }}
                                </b>
                            </span>
                        </div>
                    @endif

                    @if(session()->has('screeningFormData'))
                        @php
                            Session::forget('screeningFormData');
                        @endphp
                    @endif

                    @if(session()->has('consentFormData'))
                        @php
                            Session::forget('consentFormData');
                        @endphp
                    @endif
                    @if($appointments)
                        <div class="card table-with-links">
                            <div class="card-header ">
                                <h4 class="card-title">Appointments</h4>
                            </div>
                            <div class="card-body table-responsive">
                                @if(auth()->user()->isAdministrator() || auth()->user()->isSuperUser())
                                <form  method="GET" action="{{ url('appointments/index') }}">
                                    <button type="submit" name="action" class="btn btn-fill btn-primary" value="EXCEL">Export to excel</button>
                                    <button type="submit" name="action" class="btn btn-fill btn-primary" value="CSV">Export to csv</button>
                                </form>
                                @endif
                                <table class="table table-hover table-bordered ">
                                    <thead>
                                        <tr class="success d-flex">
                                            <th class="text-center col-2">Name</th>
                                            <th class="text-center col-2">Date/time of Appointment</th>
                                            <th class="text-center col-2">Contact Number</th>
                                            <th class="text-center col-2">Reason/Purpose</th>
                                            <th class="text-center col-2">Status
                                                <form method="GET" role="form" action="{{ url('appointments/index') }}">
                                                    <div class="form-inline row d-flex justify-content-center text-center">
                                                        @if(auth()->user()->isAdministrator() || auth()->user()->isSuperUser())
                                                            <select class="form-control" style="width: 8rem; font-size: 12px;" onchange='if(this.value != 8) {this.form.submit();}' name="filter" id="filter" required>
                                                                <option value="">- Filter Status -</option>
                                                                <option value="7">Today</option>
                                                                <option value="8">All</option>
                                                                <option value="1">For Confirmation</option>
                                                                <option value="2">Confirmed</option>
                                                                {{--<option value="3">Checked-In</option>
                                                                <option value="4">In-Chair</option>
                                                                <option value="5">Completed</option>--}}
                                                                <option value="6">Cancelled</option>
                                                            </select>
                                                        @endif
                                                    </div>
                                                </form>
                                            </th>
                                            @if(auth()->user()->isAdministrator() || auth()->user()->isSuperUser())
                                            <th class="text-center col-1">COVID Form</th>
                                            <th class="text-center col-1">Actions</th>
                                            @else
                                            <th class="text-center col-2">Actions</th>
                                            @endif
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($appointments as $key => $appointment)
                                        <?php
                                            $var = [];

                                            if (!empty($appointment->patient->screening)) {
                                                if (($appointment->patient->screening->questions&1) == 1) {
                                                    $var['#1'] = 'Yes';
                                                }
                                                if (($appointment->patient->screening->questions&2) == 2) {
                                                    $var['#2'] = 'Yes';
                                                }
                                                if (($appointment->patient->screening->questions&4) == 4) {
                                                    $var['#3'] = 'Yes';
                                                }
                                                if (($appointment->patient->screening->questions&8) == 8) {
                                                    $var['#4'] = 'Yes';
                                                }
                                                if (($appointment->patient->screening->questions&16) == 16) {
                                                    $var['#5'] = 'Yes';
                                                }
                                                if (($appointment->patient->screening->questions&32) == 32) {
                                                    $var['#6'] = 'Yes';
                                                }
                                                if (($appointment->patient->screening->questions&64) == 64) {
                                                    $var['#7'] = 'Yes';
                                                }
                                                if (($appointment->patient->screening->questions&128) == 128) {
                                                    $var['#8'] = 'Yes';
                                                }
                                                if (($appointment->patient->screening->questions&256) == 256) {
                                                    $var['#9'] = 'Yes';
                                                }

                                                $var = json_encode($var);
                                                $myJsonString = str_replace(['{', '}', '"'], '', $var);
                                                $finalString = str_replace(':', ' - ', $myJsonString);
                                                $finalString = str_replace(',', ',  ', $finalString);
                                            }
                                        ?>
                                            <tr class="d-flex">
                                                <form method="post" action="{{ url('appointments/status', ['id' => $appointment->id]) }}">
                                                @csrf
                                                    <td class="text-center col-2">{{ $appointment->patient->first_name }} {{ $appointment->patient->last_name }} </td>
                                                    <td class="text-center col-2">{{ date('M-d-Y', strtotime($appointment->timeSlot->slot_date)) }} / {{ $appointment->timeSlot->slot_time }} </td>
                                                    <td class="text-center col-2">{{ $appointment->patient->contact_number }} </td>
                                                    <td class="text-center col-2">{{ Str::limit($appointment->requirements, 40) }}</td>
                                                    <td class="text-center col-2">
                                                        @if(auth()->user()->isAdministrator() || auth()->user()->isSuperUser())
                                                                <select name="status" onchange='if(this.value != 6) {this.form.submit();}' style="border:none;color:#888888" class="text-body">
                                                                @if($appointment->status == 'For Confirmation')
                                                                    <option value='0' selected="selected">{{ $appointment->status }}</option>
                                                                    <option value='1'>Confirmed</option>
                                                                    <option value='2'>Cancelled</option>
                                                                    {{--<option value='3'>Checked-In</option>--}}
                                                                    {{--<option value='4'>In-Chair</option>--}}
                                                                    {{--<option value='5'>Completed</option>--}}
                                                                @elseif($appointment->status == 'Confirmed')
                                                                    <option value='1' selected="selected">{{ $appointment->status }}</option>
                                                                    <option value='2'>Cancelled</option>
                                                                    {{--<option value='3'>Checked-In</option>--}}
                                                                    {{--<option value='4'>In-Chair</option>--}}
                                                                    {{--<option value='5'>Completed</option>--}}
                                                                    <option value='0'>For Confirmation</option>
                                                                @elseif($appointment->status == 'Cancelled')
                                                                    <option value='2' selected="selected">{{ $appointment->status }}</option>
                                                                    {{--<option value='3'>Checked-In</option>--}}
                                                                    {{--<option value='4'>In-Chair</option>--}}
                                                                    {{--<option value='5'>Completed</option>--}}
                                                                    <option value='0'>For Confirmation</option>
                                                                    <option value='1'>Confirmed</option>
                                                                {{--@elseif($appointment->status == 'Checked-In')
                                                                    <option value='3' selected="selected">{{ $appointment->status }}</option>
                                                                    <option value='4'>In-Chair</option>
                                                                    <option value='5'>Completed</option>
                                                                    <option value='0'>For Confirmation</option>
                                                                    <option value='1'>Confirmed</option>
                                                                    <option value='2'>Cancelled</option>
                                                                @elseif($appointment->status == 'In-Chair')
                                                                    <option value='4' selected="selected">{{ $appointment->status }}</option>
                                                                    <option value='5'>Completed</option>
                                                                    <option value='0'>For Confirmation</option>
                                                                    <option value='1'>Confirmed</option>
                                                                    <option value='2'>Cancelled</option>
                                                                    <option value='3'>Checked-In</option>
                                                                @else
                                                                    <option value='5' selected="selected">{{ $appointment->status }}</option>
                                                                    <option value='0'>For Confirmation</option>
                                                                    <option value='1'>Confirmed</option>
                                                                    <option value='2'>Cancelled</option>
                                                                    <option value='3'>Checked-In</option>
                                                                    <option value='4'>In-Chair</option>--}}
                                                                @endif
                                                        @else
                                                        <!--{{ $appointment->status }}-->
                                                        {{ $appointment->status }}
                                                        @endif
                                                    </td>
                                                    @if(auth()->user()->isAdministrator() || auth()->user()->isSuperUser())
                                                        @if (empty($appointment->patient->screening))
                                                            <td class="text-center col-1">
                                                                None
                                                            </td>
                                                        @else
                                                            <td class="text-center col-1">
                                                                {{ $appointment->patient->screening->questions == 0 ? 'Clear' :  $finalString  }}
                                                            </td>
                                                        @endif
                                                    @endif
                                                    @if(auth()->user()->isAdministrator() || auth()->user()->isSuperUser())
                                                    <td class="td-actions col-1 text-center">
                                                    @else
                                                    <td class="td-actions col-2 text-center">
                                                    @endif
                                                        <a href="{{ url('appointments/show', ['id' => $appointment->id]) }}" rel="tooltip" title="View Appointment" class="btn btn-success btn-link btn-xs">
                                                            <i class="fa fa-eye"></i>
                                                        </a>
                                                        <a href="{{ url('appointments/showscreen', ['id' => $appointment->id]) }}" rel="tooltip" title="COVID-19 Screening Form" class="btn btn-success btn-link btn-xs">
                                                            <i class="fa fa-newspaper-o"></i>
                                                        </a>
                                                        <a href="{{ url ('appointments/patienthistory', ['id' => $appointment->patient->id, 'referer' => 'index']) }}" rel="tooltip" title="Show Patient History" class="btn btn-success btn-link btn-xs">
                                                            <i class="fa fa-history"></i>
                                                        </a>
                                                    </td>
                                                </form>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    @else
                        <div class="card">
                            <div class="card-body">
                                No appointments. <a href="{{ url('appointments/create') }}">Click here to book an appointment!</a>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection


@section('javascript')
    <script type="text/javascript">
    jQuery.fn.extend({
      autoHeight: function () {
        function autoHeight_(element) {
          return jQuery(element).css({
            'height': 'auto',
            'overflow-y': 'hidden'
          }).height(element.scrollHeight);
        }
        return this.each(function () {
          autoHeight_(this).on('input', function () {
            autoHeight_(this);
          });
        });
      }
    });
    $('#covid_form').autoHeight();

    document.addEventListener('DOMContentLoaded', function() {
        sessionStorage.removeItem('selectedDate');

        sessionStorage.removeItem('UVWXYZ');
        sessionStorage.removeItem('PQRST');
        sessionStorage.removeItem('KLMNO');
        sessionStorage.removeItem('FGHIJ');
        sessionStorage.removeItem('ABCDE');
        
        sessionStorage.removeItem('bdate');
    });
    </script>
@endsection
