@extends('layouts.dashboard')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                                <i class="nc-icon nc-simple-remove"></i>
                            </button>
                            <span>
                                <b>
                                    {{ session('message') }}
                                </b>
                            </span>
                        </div>
                    @endif
                    @if ($errors->any())
                        <ul>
                        @foreach ($errors->all() as $message)
                            <li> {{ $message }} </li>
                        @endforeach
                        </ul>
                    @endif
                    <form method="GET" action="{{ url('appointments/create') }}">
                        @csrf
                        <input type="hidden" name="schedule_date">
                        <input type="hidden" name="schedule_time">
                        <input type="hidden" name="requirements" value="{{ app('request')->input('requirements') }}">
                        <input type="hidden" name="middle_name" value="{{ app('request')->input('middle_name') }}">
                        <input type="hidden" id="birthdate" name="birthdate" value="{{ app('request')->input('birthdate') }}">
                        <div class="card stacked-form">
                            <div class="card-header text-center" style="background-color:orange">
                                <h4 class="card-title text-light pb-2" ><strong>COVID-19 Pandemic Dental Consent Form<strong></h4>
                            </div>
                            <div class="card-body ">
                                <div class="row" style="background-color:silver">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-dark"><strong>First Name</strong> </label>
                                            <input type="text" placeholder="First Name" name="first_name" value="{{ old('first_name', app('request')->input('first_name')) }}" class="form-control @error('first_name') is-invalid @enderror" required <?php echo ((app('request')->input('first_name')) != '' ? 'readonly' : '');?>>
                                            @error('first_name')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-dark"><strong>Last Name</strong> </label>
                                            <input type="text" placeholder="Last Name" name="last_name" value="{{ old('last_name', app('request')->input('last_name')) }}" class="form-control @error('last_name') is-invalid @enderror" required <?php echo ((app('request')->input('last_name')) != '' ? 'readonly' : '');?>>
                                            @error('last_name')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4" style="background-color:silver">
                                        <div class="form-group">
                                            <label class="text-dark"><strong>Contact Number</strong> </label>
                                            <input type="number" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength = "11" placeholder="Contact Number" name="contact_number" value="{{ old('contact_number', app('request')->input('contact_number')) }}" class="form-control @error('contact_number') is-invalid @enderror" required <?php echo ((app('request')->input('contact_number')) != '' ? 'readonly' : '');?>>
                                            @error('contact_number')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12" style="background-color:silver">
                                        <div class="form-group">
                                            <label class="text-dark"><strong>Address</strong></label>
                                            <input type="text" placeholder="Address" name="address" value="{{ old('address', app('request')->input('address')) }}" class="form-control @error('address') is-invalid @enderror" required <?php echo ((app('request')->input('address')) != '' ? 'readonly' : '');?>>
                                            @error('address')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3" style="background-color:silver">
                                        <div class="form-group">
                                            <label class="text-dark"><strong>Age</strong></label>
                                            <input type="number" placeholder="Age" id="age" name="age" value="{{ old('age') }}" class="form-control @error('age') is-invalid @enderror" readonly>
                                            @error('age')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="background-color:silver">
                                        <div class="form-group">
                                            <label class="text-dark"><strong>Email</strong></label>
                                            <input type="text" placeholder="Email" name="email" value="{{ old('email', app('request')->input('email')) }}" class="form-control @error('email') is-invalid @enderror" required <?php echo ((app('request')->input('email')) != '' ? 'readonly' : '');?>>
                                            @error('email')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="background-color:silver">
                                        <div class="form-group">
                                            <label class="text-dark"><strong>Gender</strong></label>
                                            <select class="form-control @error('gender') is-invalid @enderror" name="gender" id="gender" required <?php echo ((app('request')->input('gender')) != '' ? 'readonly' : '');?>>
                                            @if(app('request')->input('gender') == "Male")
                                                <option value="" disabled>-- Select --</option>
                                                <option value="Male" selected>Male</option>
                                                <option value="Female" disabled>Female</option>
                                            @elseif( app('request')->input('gender') == "Female")
                                                <option value="" disabled>-- Select --</option>
                                                <Male value="Male" disabled>Male</option>
                                                <option value="Female" selected>Female</option>
                                            @else
                                                <option value="">-- Select --</option>
                                                <option value="Male">Male</option>
                                                <option value="Female">Female</option>
                                            @endif
                                            </select>
                                            @error('gender')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="background-color:silver">
                                        <div class="form-group">
                                            <label class="text-dark"><strong>Body Temperature: </strong></label>
                                            <input type="number" placeholder="Body Temperature" name="body_temperature" value="{{ old('body_temperature') }}" step="0.01" class="form-control @error('body_temperature') is-invalid @enderror">
                                            @error('body_temperature')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="background-color:#fffacd">
                                    <div class="col-md-12 pt-3">
                                        <div class="form-group font-weight-normal">
                                            <p class=>I, <input class="form" type="text" value="{{ old('first_name', app('request')->input('first_name')) }} {{ old('last_name', app('request')->input('last_name')) }}" style="border-style: none none solid none;background-color:#fffacd" required>, knowingly and willingly consent to have dental treatment completed during the COVID-19 pandemic.</p>
                                            <p>I understand the COVID-19 virus has a long incubation period during which carriers of the virus may not show symptoms and still be highly contagious.  It is impossible to determine who has it and who does not, given the current limits in virus testing.</p>
                                            <p>Dental procedures create water spray which is how disease is spread. The ultra-fine nature of the spray can linger in the air for minutes to sometimes hours, which can transmit the COVID-19 virus. </p>
                                            <ul>
                                                <li>I understand that due to the frequency of visits of other dental patients, the characteristics of the virus, and the characteristics of dental procedures, that I have an elevated risk of contracting the virus simply by being in the dental office, <input class="form" type="text" name="initial_1" style="border-style: none none solid none;background-color:#fffacd" required> (Initial)</li>
                                                <li>I have been made aware of the WHO ( World Health Organization ) & DOH ( Department of Health ) guidelines that under the current pandemic all non-urgent dental care is not recommended. Dental visits should be limited to the treatment of pain infection, conditions that significantly inhibit operation of the teeth and mouth and issues that may cause anything listed within the next 3-6 months. <input class="form" type="text" name="initial_2" style="border-style: none none solid none;background-color:#fffacd" required> (Initial)</li>
                                            </ul>
                                            <p>I confirm that I am not presenting any of the following symptoms of COVID-19 listed below:</p>
                                            <ul>
                                                <li>Fever</li>
                                                <li>Shortness of Breath</li>
                                                <li>Dry Cough</li>
                                                <li>Runny Nose</li>
                                                <li>Sore Throat</li>
                                            </ul>
                                            <p>I understand that air travel significantly increases my risk of contracting and transmitting the COVID-19 virus. And the WHO recommends physical distancing at least 6 feet for a period of 14 days to anyone who has, and this is not possible in dentistry. <input class="form" type="text" name="initial_3" style="border-style: none none solid none;background-color:#fffacd" required> (Initial)</p>
                                            <ul>
                                                <li>I verify that I have not traveled in the past 14 days to countries that have been affected by COVID-19. <input class="form" type="text" name="initial_4" style="border-style: none none solid none;background-color:#fffacd" required> (Initial)</li>
                                                <li>I verify that I have not traveled domestically within the Philippines by commercial airline, bus, train within the past 14 days. <input class="form" type="text" name="initial_5" style="border-style: none none solid none;background-color:#fffacd" required> (Initial) </li>
                                            </ul>
                                            <p>By submitting this form, I certify that the above statements are true, accurate and complete. </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer justify-content-right">
                                <button type="submit" name="action" class="btn btn-fill btn-primary" value="saveConsentForm">Save</button>
                                <a href="javascript:history.back()" class="btn btn-fill btn-danger">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">

        document.addEventListener('DOMContentLoaded', function() {
            var nowDate = new Date().getFullYear();
            var bDate = new Date($('#birthdate').val()).getFullYear();
            var a = nowDate - bDate;
            $('#age').val(a);
        });

    </script>
@endsection