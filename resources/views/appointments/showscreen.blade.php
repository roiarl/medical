@extends('layouts.dashboard')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="POST" action="{{ url('appointments/update', ['id' => $appointment->id]) }}">
                        @csrf
                        <input type="hidden" name="schedule_date" value="{{ $appointment->timeSlot->slot_date }}">
                        <input type="hidden" name="schedule_time" value="{{ $appointment->timeSlot->id }}">
                        <input type="hidden" name="requirements" value="{{ $appointment->requirements }}">
                        <input type="hidden" name="birthdate" value="{{ $appointment->patient->birthday }}">
                        <div class="card stacked-form">
                            <div class="card-header text-center" style="background-color:orange">
                                <h4 class="card-title text-light pb-2" ><strong>COVID-19 Screening Form<strong></h4>
                            </div>
                            @if($appointment->patient->screening)
                                <div class="card-body ">
                                    <fieldset <?php echo (($appointment->status == 'Cancelled' || $appointment->status == 'Completed') ? 'disabled="disabled"' : '');?>>
                                        <div class="row" style="background-color:silver">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="text-dark"><strong>First Name</strong> </label>
                                                    <input type="text" placeholder="First Name" value="{{ $appointment->patient->first_name }}" name="first_name" class="form-control @error('full_name') is-invalid @enderror" readonly>
                                                    @error('first_name')
                                                        <p class="text-danger">{{ $message }}</p>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="text-dark"><strong>Last Name</strong> </label>
                                                    <input type="text" placeholder="Last Name" value="{{ $appointment->patient->last_name }}" name="last_name" class="form-control @error('last_name') is-invalid @enderror" readonly>
                                                    @error('last_name')
                                                        <p class="text-danger">{{ $message }}</p>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-4" style="background-color:silver">
                                                <div class="form-group">
                                                    <label class="text-dark"><strong>Contact Number</strong> </label>
                                                    <input type="number" placeholder="Contact Number" value="{{ old('contact_number', $appointment->patient->contact_number) }}" name="contact_number" class="form-control @error('contact_number') is-invalid @enderror" readonly>
                                                    @error('contact_number')
                                                        <p class="text-danger">{{ $message }}</p>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12" style="background-color:silver">
                                                <div class="form-group">
                                                    <label class="text-dark"><strong>Address</strong></label>
                                                    <input type="text" placeholder="Address" value="{{ old('address', $appointment->patient->address) }}" name="address" class="form-control @error('address') is-invalid @enderror" readonly>
                                                    @error('address')
                                                        <p class="text-danger">{{ $message }}</p>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3" style="background-color:silver">
                                                <div class="form-group">
                                                    <label class="text-dark"><strong>Age</strong></label>
                                                    <input type="text" placeholder="Age" value="{{ old('age', $appointment->patient->age) }}" name="age" class="form-control @error('age') is-invalid @enderror" readonly>
                                                    @error('age')
                                                        <p class="text-danger">{{ $message }}</p>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-3" style="background-color:silver">
                                                <div class="form-group">
                                                    <label class="text-dark"><strong>Email</strong></label>
                                                    <input type="text" placeholder="Email" value="{{ old('email', $appointment->patient->email) }}" name="email" class="form-control @error('email') is-invalid @enderror" readonly>
                                                    @error('email')
                                                        <p class="text-danger">{{ $message }}</p>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-3" style="background-color:silver">
                                                <div class="form-group">
                                                    <label class="text-dark"><strong>Gender</strong></label>
                                                    <input type="text" placeholder="Gender" value="{{ old('gender', $appointment->patient->gender) }}" name="gender" class="form-control @error('gender') is-invalid @enderror" readonly>
                                                    @error('gender')
                                                        <p class="text-danger">{{ $message }}</p>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-3" style="background-color:silver">
                                                <div class="form-group">
                                                    <label class="text-dark"><strong>Body Temperature: </strong></label>
                                                    <input type="text" placeholder="Body Temperature" value="{{ old('body_temperature', $appointment->patient->screening->body_temperature) }}" name="body_temperature" class="form-control @error('body_temperature') is-invalid @enderror">
                                                    @error('body_temperature')
                                                        <p class="text-danger">{{ $message }}</p>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row font-weight-normal" style="background-color:#fffacd">
                                            <div class="col-md-6 form-check-inline bg-warning text-left pl-3">
                                                <p>1.  Have you visited any affected geographic area/contry within the last 30 days prior to your schedules appointment?</p>
                                            </div>
                                                <div class="col-md-5">
                                                    <div class="custom-control custom-checkbox custom-control-inline">
                                                        <input type="checkbox" class="custom-control-input @error('q1') is-invalid @enderror" name="q1" id="q1n" value="0" <?php echo (($appointment->patient->screening->questions&1) == 1 ? '' : 'checked');?> >
                                                        <label class="custom-control-label" for="q1n">No</label>
                                                    </div>
                                                    <div class="custom-control custom-checkbox custom-control-inline">
                                                        <input type="checkbox" class="custom-control-input @error('q1') is-invalid @enderror" name="q1" id="q1y" value="1" <?php echo (($appointment->patient->screening->questions&1) == 1 ? 'checked' : '');?> >
                                                        <label class="custom-control-label" for="q1y">Yes</label>
                                                    </div>
                                                    <div class="form-group pl-3" style="font-size:15px"> If yes, What Country? 
                                                        <input type="text" placeholder="" value="{{ $appointment->patient->screening->q1_country }}" name="q1_country" id="q1_country" style="border-style: none none solid none;background-color:#fffacd">
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="row font-weight-normal" style="background-color:#fffacd">
                                            <div class="col-md-6 form-check-inline bg-warning text-left pl-3">
                                                <p>2.  Have you been in close contact with someone whoe arrived form abroad within the last 30 days?</p>
                                            </div>
                                                <div class="col-md-5">
                                                    <div class="custom-control custom-checkbox custom-control-inline">
                                                        <input type="checkbox" class="custom-control-input" name="q2" id="q2n" value="0" <?php echo (($appointment->patient->screening->questions&2) == 2 ? '' : 'checked');?> >
                                                        <label class="custom-control-label" for="q2n">No</label>
                                                    </div>
                                                    <div class="custom-control custom-checkbox custom-control-inline">
                                                        <input type="checkbox" class="custom-control-input" name="q2" id="q2y" value="2" <?php echo (($appointment->patient->screening->questions&2) == 2 ? 'checked' : '');?> >
                                                        <label class="custom-control-label" for="q2y">Yes</label>
                                                    </div>
                                                    <div class="form-group pl-3" style="font-size:15px"> If yes, What Country? 
                                                        <input type="text" placeholder="" value="{{ $appointment->patient->screening->q2_country }}" name="q2_country"  id="q2_country" style="border-style: none none solid none;background-color:#fffacd">
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="row font-weight-normal" style="background-color:#fffacd">
                                            <div class="col-md-6 form-check-inline bg-warning text-left pl-3">
                                                <p>3.  Have you attended a mass gathering, reunion with relatives/friends or parties within a month prior to this visit?</p>
                                            </div>
                                                <div class="col-md-5">
                                                    <div class="custom-control custom-checkbox custom-control-inline">
                                                        <input type="checkbox" class="custom-control-input" name="q3" id="q3n" value="0" <?php echo (($appointment->patient->screening->questions&4) == 4 ? '' : 'checked');?> >
                                                        <label class="custom-control-label" for="q3n">No</label>
                                                    </div>
                                                    <div class="custom-control custom-checkbox custom-control-inline">
                                                        <input type="checkbox" class="custom-control-input" name="q3" id="q3y" value="4" <?php echo (($appointment->patient->screening->questions&4) == 4 ? 'checked' : '');?> >
                                                        <label class="custom-control-label" for="q3y">Yes</label>
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="row font-weight-normal" style="background-color:#fffacd">
                                            <div class="col-md-6 form-check-inline bg-warning text-left pl-3">
                                                <p>4.  Have you been in close contact with a COVID-19 positive patient?</p>
                                            </div>
                                                <div class="col-md-4">
                                                    <div class="custom-control custom-checkbox custom-control-inline">
                                                        <input type="checkbox" class="custom-control-input" name="q4" id="q4n" value="0" <?php echo (($appointment->patient->screening->questions&8) == 8 ? '' : 'checked');?> >
                                                        <label class="custom-control-label" for="q4n">No</label>
                                                    </div>
                                                    <div class="custom-control custom-checkbox custom-control-inline">
                                                        <input type="checkbox" class="custom-control-input" name="q4" id="q4y" value="8" <?php echo (($appointment->patient->screening->questions&8) == 8 ? 'checked' : '');?> >
                                                        <label class="custom-control-label" for="q4y">Yes</label>
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="row font-weight-normal" style="background-color:#fffacd">
                                            <div class="col-md-6 form-check-inline bg-warning text-left pl-3">
                                                <p>5.  Have you been in close contact with a person under investigation? (PUI)</p>
                                            </div>
                                                <div class="col-md-5">
                                                    <div class="custom-control custom-checkbox custom-control-inline">
                                                        <input type="checkbox" class="custom-control-input" name="q5" id="q5n" value="0" <?php echo (($appointment->patient->screening->questions&16) == 16 ? '' : 'checked');?> >
                                                        <label class="custom-control-label" for="q5n">No</label>
                                                    </div>
                                                    <div class="custom-control custom-checkbox custom-control-inline">
                                                        <input type="checkbox" class="custom-control-input" name="q5" id="q5y" value="16" <?php echo (($appointment->patient->screening->questions&16) == 16 ? 'checked' : '');?> >
                                                        <label class="custom-control-label" for="q5y">Yes</label>
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="row font-weight-normal" style="background-color:#fffacd">
                                            <div class="col-md-6 form-check-inline bg-warning text-left pl-3">
                                                <p>6.  Have you been in close contact with a person under monitoring? (PUM)</p>
                                            </div>
                                                <div class="col-md-5">
                                                    <div class="custom-control custom-checkbox custom-control-inline">
                                                        <input type="checkbox" class="custom-control-input" name="q6" id="q6n" value="0" <?php echo (($appointment->patient->screening->questions&32) == 32 ? '' : 'checked');?> >
                                                        <label class="custom-control-label" for="q6n">No</label>
                                                    </div>
                                                    <div class="custom-control custom-checkbox custom-control-inline">
                                                        <input type="checkbox" class="custom-control-input" name="q6" id="q6y" value="32" <?php echo (($appointment->patient->screening->questions&32) == 32 ? 'checked' : '');?> >
                                                        <label class="custom-control-label" for="q6y">Yes</label>
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="row font-weight-normal" style="background-color:#fffacd">
                                            <div class="col-md-6 form-check-inline bg-warning text-left pl-3">
                                                <p>7.  Do you have any flu-like/respiratory symptoms? Ex: Fever, cough, runny nose, sore throat, headache, shortness of breath, chills, general malaise, diarrhea</p>
                                            </div>
                                                <div class="col-md-5">
                                                    <div class="custom-control custom-checkbox custom-control-inline">
                                                        <input type="checkbox" class="custom-control-input" name="q7" id="q7n" value="0" <?php echo (($appointment->patient->screening->questions&64) == 64 ? '' : 'checked');?> >
                                                        <label class="custom-control-label" for="q7n">No</label>
                                                    </div>
                                                    <div class="custom-control custom-checkbox custom-control-inline">
                                                        <input type="checkbox" class="custom-control-input" name="q7" id="q7y" value="64" <?php echo (($appointment->patient->screening->questions&64) == 64 ? 'checked' : '');?> >
                                                        <label class="custom-control-label" for="q7y">Yes</label>
                                                    </div>
                                                    <div class="form-group pl-3" style="font-size:15px"> If yes, What Symptoms? 
                                                        <input type="text" placeholder="" value="" name="q7_country" id="q7_country" style="border-style: none none solid none;background-color:#fffacd">
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="row font-weight-normal" style="background-color:#fffacd">
                                            <div class="col-md-6 form-check-inline bg-warning text-left pl-3">
                                                <p>8.  Is there anything else we should know before treating you?</p>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="custom-control custom-checkbox custom-control-inline">
                                                    <input type="checkbox" class="custom-control-input" name="q8" id="q8n" value="0" <?php echo (($appointment->patient->screening->questions&128) == 128 ? '' : 'checked');?> >
                                                    <label class="custom-control-label" for="q8n">No</label>
                                                </div>
                                                <div class="custom-control custom-checkbox custom-control-inline">
                                                    <input type="checkbox" class="custom-control-input" name="q8" id="q8y" value="128" <?php echo (($appointment->patient->screening->questions&128) == 128 ? 'checked' : '');?> >
                                                    <label class="custom-control-label" for="q8y">Yes</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row font-weight-normal" style="background-color:#fffacd">
                                            <div class="col-md-6 form-check-inline bg-warning text-left pl-3">
                                                <p>9.  Are you currently experiencing a Dental Emergency?</p>
                                            </div>
                                                <div class="col-md-5">
                                                    <div class="custom-control custom-checkbox custom-control-inline">
                                                        <input type="checkbox" class="custom-control-input" name="q9" id="q9n" value="0" <?php echo (($appointment->patient->screening->questions&256) == 256 ? '' : 'checked');?> >
                                                        <label class="custom-control-label" for="q9n">No</label>
                                                    </div>
                                                    <div class="custom-control custom-checkbox custom-control-inline">
                                                        <input type="checkbox" class="custom-control-input" name="q9" id="q9y" value="256" <?php echo (($appointment->patient->screening->questions&256) == 256 ? 'checked' : '');?> >
                                                        <label class="custom-control-label" for="q9y">Yes</label>
                                                    </div>
                                                    @error('q9')
                                                            <p class="text-danger">{{ $message }}</p>
                                                    @enderror
                                                </div>
                                        </div>
                                        <div class="row font-weight-normal" style="background-color:#fffacd">
                                            <div class="col-md-12 form-check-inline bg-warning text-left pl-3">
                                                <p>By submitting this form, I hereby declare that the above statements are true, accurate and complete.</p>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                                <div class="card-footer justify-content-right">
                                @if($appointment->status != 'Cancelled' && $appointment->status != 'Completed')
                                    <button type="submit" class="btn btn-fill btn-primary" name="action" value="updateScreening">Update</button>
                                @endif
                                    <a href="{{ url('appointments/index') }}" class="btn btn-fill btn-default">Cancel</a>
                                </div>
                            @else
                                <div class="card-body ">
                                    No COVID-19 Screening Form. <p>Click <a href="{{ url ('appointments/screeningform') }}" data-form-screen> here </a>to fill-out the form</p> 
                                </div>
                                <div class="card-footer justify-content-right">
                                    <a href="{{ url()->previous() }}" class="btn btn-fill btn-default">Back</a>
                                </div>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('javascript')
    <script type="text/javascript">

        $('#q1n').change(function(){
            if($('#q1n').is(':checked')){
                $('#q1y').prop("checked", false);
            }else{
                $('#q1y').prop("checked", true);
            }
        })
        $('#q1y').change(function(){
            if($('#q1y').is(':checked')){
                $('#q1n').prop("checked", false);
            }else{
                $('#q1n').prop("checked", true);
            }
        })
        $('#q2n').change(function(){
            if($('#q2n').is(':checked')){
                $('#q2y').prop("checked", false);
            }else{
                $('#q2y').prop("checked", true);
            }
        })
        $('#q2y').change(function(){
            if($('#q2y').is(':checked')){
                $('#q2n').prop("checked", false);
            }else{
                $('#q2n').prop("checked", true);
            }
        })
        $('#q3n').change(function(){
            if($('#q3n').is(':checked')){
                $('#q3y').prop("checked", false);
            }else{
                $('#q3y').prop("checked", true);
            }
        })
        $('#q3y').change(function(){
            if($('#q3y').is(':checked')){
                $('#q3n').prop("checked", false);
            }else{
                $('#q3n').prop("checked", true);
            }
        })
        $('#q4n').change(function(){
            if($('#q4n').is(':checked')){
                $('#q4y').prop("checked", false);
            }else{
                $('#q4y').prop("checked", true);
            }
        })
        $('#q4y').change(function(){
            if($('#q4y').is(':checked')){
                $('#q4n').prop("checked", false);
            }else{
                $('#q4n').prop("checked", true);
            }
        })
        $('#q5n').change(function(){
            if($('#q5n').is(':checked')){
                $('#q5y').prop("checked", false);
            }else{
                $('#q5y').prop("checked", true);
            }
        })
        $('#q5y').change(function(){
            if($('#q5y').is(':checked')){
                $('#q5n').prop("checked", false);
            }else{
                $('#q5n').prop("checked", true);
            }
        })
        $('#q6n').change(function(){
            if($('#q6n').is(':checked')){
                $('#q6y').prop("checked", false);
            }else{
                $('#q6y').prop("checked", true);
            }
        })
        $('#q6y').change(function(){
            if($('#q6y').is(':checked')){
                $('#q6n').prop("checked", false);
            }else{
                $('#q6n').prop("checked", true);
            }
        })
        $('#q7n').change(function(){
            if($('#q7n').is(':checked')){
                $('#q7y').prop("checked", false);
            }else{
                $('#q7y').prop("checked", true);
            }
        })
        $('#q7y').change(function(){
            if($('#q7y').is(':checked')){
                $('#q7n').prop("checked", false);
            }else{
                $('#q7n').prop("checked", true);
            }
        })
        $('#q8n').change(function(){
            if($('#q8n').is(':checked')){
                $('#q8y').prop("checked", false);
            }else{
                $('#q8y').prop("checked", true);
            }
        })
        $('#q8y').change(function(){
            if($('#q8y').is(':checked')){
                $('#q8n').prop("checked", false);
            }else{
                $('#q8n').prop("checked", true);
            }
        })
        $('#q9n').change(function(){
            if($('#q9n').is(':checked')){
                $('#q9y').prop("checked", false);
            }else{
                $('#q9y').prop("checked", true);
            }
        })
        $('#q9y').change(function(){
            if($('#q9y').is(':checked')){
                $('#q9n').prop("checked", false);
            }else{
                $('#q9n').prop("checked", true);
            }
        })

        $(function()
        {
            var formAttach = $('[data-form-screen]');
            var formAttachLink = $('[data-form-screen]').attr('href');
            var paramStr = '';
            var attachFlag = false;
            var getParamData = function(key)
            {
                return (paramStr.split(key + '=')[1] || '').split('&')[0];
            };
            var updateParamData = function(key, val)
            {
                var keyVal = transformParamData(key, val);
                var oldParamStr = transformParamData(key, getParamData(key));
                return paramStr.indexOf(key) !== -1 ? paramStr.replace(oldParamStr, keyVal) : paramStr += keyVal;
            };
            var transformParamData = function(key, val)
            {
                return '&' + key + '=' + val;
            };

            $("p").mouseover(function(){
                formAttach.attr('href', formAttachLink + '?' + updateParamData("first_name", "{{ $appointment->patient->first_name }}" ));
                formAttach.attr('href', formAttachLink + '?' + updateParamData("last_name", "{{ $appointment->patient->last_name }}" ));
                formAttach.attr('href', formAttachLink + '?' + updateParamData("contact_number", "{{ $appointment->patient->contact_number }}" ));
                formAttach.attr('href', formAttachLink + '?' + updateParamData("address", "{{ $appointment->patient->address }}" ));
                formAttach.attr('href', formAttachLink + '?' + updateParamData("birthdate", "{{ $appointment->patient->birthday }}" ));
                formAttach.attr('href', formAttachLink + '?' + updateParamData("email", "{{ $appointment->patient->email }}" ));
                formAttach.attr('href', formAttachLink + '?' + updateParamData("gender", "{{ $appointment->patient->gender }}" ));
                formAttach.attr('href', formAttachLink + '?' + updateParamData("id", "{{ $appointment->patient->id }}" ));
            })
        });
    </script>
@endsection