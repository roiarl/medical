@extends('layouts.dashboard')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @if ($errors->any())
                        <ul>
                        @foreach ($errors->all() as $message)
                            <li> {{ $message }} </li>
                        @endforeach
                        </ul>
                    @endif
                    <form method="POST" action="{{ url('appointments/update', ['id' => $appointment->id]) }}">
                        @csrf
                        <input type="hidden" name="schedule_date" value="{{ $appointment->timeSlot->slot_date }}">
                        <input type="hidden" name="schedule_time" value="{{ $appointment->timeSlot->id }}">
                        <input type="hidden" name="requirements" value="{{ $appointment->requirements }}">
                        <input type="hidden" name="birthdate" value="{{ $appointment->patient->birthday }}">
                        <div class="card stacked-form">
                            <div class="card-header text-center" style="background-color:orange">
                                <h4 class="card-title text-light pb-2" ><strong>COVID-19 Pandemic Dental Consent Form<strong></h4>
                            </div>
                            <div class="card-body ">
                                <div class="row" style="background-color:silver">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-dark"><strong>First Name</strong> </label>
                                            <input type="text" placeholder="First Name" value="{{ old('first_name', $appointment->patient->first_name) }}" name="first_name" class="form-control @error('first_name') is-invalid @enderror" readonly>
                                            @error('first_name')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-dark"><strong>Last Name</strong> </label>
                                            <input type="text" placeholder="Last Name" value="{{ old('last_name', $appointment->patient->last_name) }}" name="last_name" class="form-control @error('last_name') is-invalid @enderror" readonly>
                                            @error('last_name')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4" style="background-color:silver">
                                        <div class="form-group">
                                            <label class="text-dark"><strong>Contact Number</strong> </label>
                                            <input type="number" placeholder="Contact Number" value="{{ old('contact_number', $appointment->patient->contact_number) }}" name="contact_number" class="form-control @error('contact_number') is-invalid @enderror" readonly>
                                            @error('contact_number')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12" style="background-color:silver">
                                        <div class="form-group">
                                            <label class="text-dark"><strong>Address</strong></label>
                                            <input type="text" placeholder="Address" value="{{ old('address', $appointment->patient->address) }}" name="address" class="form-control @error('address') is-invalid @enderror" readonly>
                                            @error('address')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3" style="background-color:silver">
                                        <div class="form-group">
                                            <label class="text-dark"><strong>Age</strong></label>
                                            <input type="text" placeholder="Age" value="{{ $appointment->patient->screening->age }}" name="age" class="form-control @error('age') is-invalid @enderror" readonly>
                                            @error('age')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="background-color:silver">
                                        <div class="form-group">
                                            <label class="text-dark"><strong>Email</strong></label>
                                            <input type="text" placeholder="Email" value="{{ old('email', $appointment->patient->email) }}" name="email" class="form-control @error('email') is-invalid @enderror" readonly>
                                            @error('email')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="background-color:silver">
                                        <div class="form-group">
                                            <label class="text-dark"><strong>Gender</strong></label>
                                            <input type="text" placeholder="Gender" value="{{ old('gender', $appointment->patient->gender) }}" name="gender" class="form-control @error('gender') is-invalid @enderror" readonly>
                                            @error('gender')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="background-color:silver">
                                        <div class="form-group">
                                            <label class="text-dark"><strong>Body Temperature: </strong></label>
                                            <input type="text" placeholder="Body Temperature" value="{{ $appointment->patient->screening->body_temperature }}" name="body_temperature" class="form-control @error('body_temperature') is-invalid @enderror" readonly>
                                            @error('body_temperature')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="background-color:#fffacd">
                                    <div class="col-md-12 pt-3">
                                        <div class="form-group font-weight-normal">
                                            <p class=>I, <input class="form" type="text" value="{{ $appointment['patient']['first_name'] }} {{ $appointment['patient']['last_name'] }}" style="border-style: none none solid none;background-color:#fffacd" readonly>,knowingly and willingly consent to have dental treatment completed during the COVID-19 pandemic.</p>
                                            <p>I understand the COVID-19 virus has a long incubation period during which carriers of the virus may not show symptoms and still be highly contagious.  It is impossible to determine who has it and whoe does not give the current limits in virus testing.</p>
                                            <p>Dental procedures create water spray which is how disease is spread. The ultra-fine nature of the spray can linger in the air for minutes to sometimes hours, which can transmit the COVID-19 virus. </p>
                                            <ul>
                                                <li>I understand that due to the frequency of visits of other dental patients, the characteristics of the virus, and the characteristics of dental procedures, that I have an elevated risk of contracting the virus simply by being in the dental office, <input class="form" type="text" name="initial_1" value="{{ $appointment->patient->consent->initial_1 }}" style="border-style: none none solid none;background-color:#fffacd"> (Initial)</li>
                                                <li>I have been made aware of the WHO ( World Health Organization ) & DOH ( Department of Health ) guidelines that under the current pandemic all non-urgent dental care is not recommended. Dental visits should be limited to the treatment of pain infection, conditions that significantly inhibit operation of the teeth and mouth and issues that may cause anything listed within the next 3-6 months. <input class="form" type="text" name="initial_2" value="{{ $appointment->patient->consent->initial_2 }}" style="border-style: none none solid none;background-color:#fffacd"> (Initial)</li>
                                            </ul>
                                            <p>I confirm that I am not presenting any of the following symptoms of COVID-19 listed below:</p>
                                            <ul>
                                                <li>Fever</li>
                                                <li>Shortness of Breath</li>
                                                <li>Dry Cough</li>
                                                <li>Runny Nose</li>
                                                <li>Sore Throat</li>
                                            </ul>
                                            <p>I understand that air travel significantly increases my risk of contracting and transmitting the COVID-19 virus. And the WHO recommends physical distancing at least 6 feet for a period of 14 days to anyone who has, and this is not possible in dentistry. <input class="form" type="text" name="initial_3" value="{{ $appointment->patient->consent->initial_3 }}" style="border-style: none none solid none;background-color:#fffacd"> (Initial)</p>
                                            <ul>
                                                <li>I verify that I have not traveled in the past 14 days to countries that have been affected by COVID-19. <input class="form" type="text" name="initial_4" value="{{ $appointment->patient->consent->initial_4 }}" style="border-style: none none solid none;background-color:#fffacd"> (Initial)</li>
                                                <li>I verify that I have not traveled domestically within the Philippines by commercial airline, bus, train within the past 14 days. <input class="form" type="text" name="initial_5" value="{{ $appointment->patient->consent->initial_5 }}" style="border-style: none none solid none;background-color:#fffacd"> (Initial) </li>
                                            </ul>
                                            <p>By submitting this form, I certify that the above statements are true, accurate and complete. </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer justify-content-right">
                                <button type="submit" class="btn btn-fill btn-primary" name="action" value="updateConsent">Update</button>
                                <a href="{{ url('appointments/index') }}" class="btn btn-fill btn-default">Back</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection