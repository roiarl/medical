@extends('layouts.dashboard')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="POST" action="{{ url('appointments/updateUserRole') }}" enctype="multipart/form-data">
                        @csrf

                        
                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                                    <i class="nc-icon nc-simple-remove"></i>
                                </button>
                                <span>
                                    <b>
                                        {{ session('message') }}
                                    </b>
                                </span>
                            </div>
                        @endif
                        <div class="card">
                            <div class="card-header ">
                                <h4 class="card-title">List of Users</h4>
                            </div>
                            @if($users->count() > 0)
                            <div class="card-body ">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-hover bg-light">
                                            <thead>
                                                <th>Email</th>
                                                <th>Role</th>
                                                <th>License No.</th>
                                                <th>Signature</th>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <select class="form-control" name="user" id="user" required>
                                                            @foreach ($users as $user)
                                                                <option value="{{ $user->email }}">{{ $user->email }}</option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select class="form-control" name="role" id="role" required>
                                                            <option value="doctor">Doctor</option>
                                                            <option value="admin">Admin</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <input type="number" id="license_no" name="license_no" class="form-control" required>
                                                    </td>
                                                    <td>
                                                        <div class="form-group d-flex flex-column">
                                                            <label for="image">Upload a Signature</label>
                                                            <input type="file" name="image" id="image">
                                                            <div>{{ $errors->first('image') }}</div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ">
                                <button type="submit" name="action" id="create" class="btn btn-fill btn-success" value="create">Update</button>
                            </div>
                            @else
                            <div class="card-body ">
                                <div class="row">
                                    <div class="col-md-12">
                                    There are no registered users.
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">

        document.addEventListener('DOMContentLoaded', function() {

            
        });
        $('#role').change(function(){
            if ($('#role').val() === "admin") {
                $('#license_no').attr('disabled', 'disabled');
                $('#image').attr('disabled', 'disabled');
            } else {
                $('#license_no').removeAttr('disabled');
                $('#image').removeAttr('disabled');
            }
        })

    </script>

@endsection