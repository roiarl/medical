<?php

namespace App\Providers;

use App\Repositories\AppointmentRepository;
use App\Repositories\AppointmentRepositoryInterface;
use App\Repositories\DoctorRepository;
use App\Repositories\DoctorRepositoryInterface;
use App\Repositories\PatientRepository;
use App\Repositories\PatientRepositoryInterface;
use App\Repositories\ServiceRepository;
use App\Repositories\ServiceRepositoryInterface;
use App\Repositories\TimeSlotRepository;
use App\Repositories\TimeSlotRepositoryInterface;
use App\Repositories\ScreeningRepository;
use App\Repositories\ScreeningRepositoryInterface;
use App\Repositories\ConsentRepository;
use App\Repositories\ConsentRepositoryInterface;
use App\Repositories\PrescriptionRepository;
use App\Repositories\PrescriptionRepositoryInterface;
use App\Repositories\PatientRecordRepository;
use App\Repositories\PatientRecordRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(AppointmentRepositoryInterface::class, AppointmentRepository::class);
        $this->app->bind(DoctorRepositoryInterface::class, DoctorRepository::class);
        $this->app->bind(PatientRepositoryInterface::class, PatientRepository::class);
        $this->app->bind(ServiceRepositoryInterface::class, ServiceRepository::class);
        $this->app->bind(TimeSlotRepositoryInterface::class, TimeSlotRepository::class);
        $this->app->bind(ScreeningRepositoryInterface::class, ScreeningRepository::class);
        $this->app->bind(ConsentRepositoryInterface::class, ConsentRepository::class);
        $this->app->bind(PrescriptionRepositoryInterface::class, PrescriptionRepository::class);
        $this->app->bind(PatientRecordRepositoryInterface::class, PatientRecordRepository::class);
    }
}
