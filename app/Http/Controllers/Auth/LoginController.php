<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Carbon\Carbon;
use Illuminate\Validation\ValidationException;
use App\Rules\Captcha;

use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    protected $maxAttempts = 3;
    protected $decayMinutes = 1;

    protected function authenticated(Request $request, $user)
    {
        $request->session()->forget('password_expired_id');

        $password_updated_at = $user->passwordSecurity->password_updated_at;
        $password_expiry_days = $user->passwordSecurity->password_expiry_days;
        $password_expiry_at = Carbon::parse($password_updated_at)->addDays($password_expiry_days);
        if($password_expiry_at->lessThan(Carbon::now())){
            $request->session()->put('password_expired_id',$user->id);
            auth()->logout();
            return redirect('/passwordExpiration')->with('message', "Your Password is expired, You need to change your password.");
        }
    
        if ( $user->isAdministrator() ){
            return redirect()->route('appointment.index');
        } elseif ( $user->isSuperUser() ) {
            return redirect()->route('appointments.superuser');
        } else {
            return redirect()->route('appointments.patientlist');
        }
    }
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function email()
    {
        return 'email';
    }

    public function validateLogin(Request $request)
    {
        $this->validate($request,[
            'email' => 'required',
            'password' => 'required',
            // 'captcha' => 'required|captcha',
            'g-recaptcha-response' => new Captcha(),
        ]);
    }
    protected function sendFailedLoginResponse(Request $request)
    {
        $user = \App\User::where($this->email(), $request->get('email'))->first();
        if (!$user) {
            // Custom error message with an href link.  succeding message is in login.blade.php
            throw ValidationException::withMessages([
                'email_reg' => 'Please ',
            ]);
        } else {
            throw ValidationException::withMessages([
                $this->username() => [trans('auth.failed')],
            ]);
        }
    }
}
