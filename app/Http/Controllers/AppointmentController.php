<?php

namespace App\Http\Controllers;

use App\Http\Requests\CalendarEvent;
use App\Http\Requests\CalendarTimeSlot;
use App\Http\Requests\StoreAppointment;
use App\Http\Resources\CalendarEvents;
use App\Repositories\AppointmentRepositoryInterface;
use App\Repositories\PatientRepositoryInterface;
use App\Repositories\TimeSlotRepositoryInterface;
use App\Repositories\ScreeningRepositoryInterface;
use App\Repositories\ConsentRepositoryInterface;
use App\Repositories\PrescriptionRepositoryInterface;
use App\Repositories\PatientRecordRepositoryInterface;
use App\Repositories\DoctorRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\PostsExport;
use App\User;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use DateTime;
use DatePeriod;
use DateInterval;

class AppointmentController extends Controller
{
    /**
     * @var \App\Repositories\AppointmentRepositoryInterface
     */
    private $appointmentRepository;
    private $doctorRepository;

    /**
     * @var \App\Repositories\PatientRepositoryInterface
     */
    private $patientRepository;

    private $timeSlotRepository;

    private $screeningRepository;

    private $consentRepository;

    private $prescriptionRepository;

    private $patientRecordRepository;


    public function __construct(
        AppointmentRepositoryInterface $appointmentRepository,
        PatientRepositoryInterface $patientRepository,
        TimeSlotRepositoryInterface $timeSlotRepository,
        ScreeningRepositoryInterface $screeningRepository,
        ConsentRepositoryInterface $consentRepository,
        PrescriptionRepositoryInterface $prescriptionRepository,
        PatientRecordRepositoryInterface $patientRecordRepository,
        DoctorRepositoryInterface $doctorRepository
    )
    {
        $this->appointmentRepository = $appointmentRepository;
        $this->patientRepository = $patientRepository;
        $this->timeSlotRepository = $timeSlotRepository;
        $this->screeningRepository = $screeningRepository;
        $this->consentRepository = $consentRepository;
        $this->prescriptionRepository = $prescriptionRepository;
        $this->patientRecordRepository = $patientRecordRepository;
        $this->doctorRepository = $doctorRepository;
    }

    /**
     * @return mixed
     */
    public function index(Request $request)
    {
        $page = 'My Appointments';
        if (auth()->user()->is_admin || auth()->user()->isSuperUser()) {
            $value = $request->get('filter');
            if ($value) {
                switch($value)
                {
                    case '8': {
                        $appointments = $this->appointmentRepository->getAll();
                        break;
                    }
                    case '1': {
                        $status = 'For Confirmation';
                        $appointments = $this->appointmentRepository->filterByStatus($status);
                        break;
                    }
                    case '2': {
                        $status = 'Confirmed';
                        $appointments = $this->appointmentRepository->filterByStatus($status);
                        break;
                    }
                    case '3': {
                        $status = 'Checked-In';
                        $appointments = $this->appointmentRepository->filterByStatus($status);
                        break;
                    }
                    case '4': {
                        $status = 'In-Chair';
                        $appointments = $this->appointmentRepository->filterByStatus($status);
                        break;
                    }
                    case '5': {
                        $status = 'Completed';
                        $appointments = $this->appointmentRepository->filterByStatus($status);
                        break;
                    }
                    case '6': {
                        $status = 'Cancelled';
                        $appointments = $this->appointmentRepository->filterByStatus($status);
                        break;
                    }
                    case '7': {
                        $status = 'Today';
                        $appointments = $this->appointmentRepository->filterByStatus($status);
                        break;
                    }
                }
            } else {
                $appointments = $this->appointmentRepository->getAllCurrent();
            }
        } else {
            $appointments = $this->appointmentRepository->getByUserId(auth()->user()->id);
        }

        switch($request->input('action'))
        {
            case 'EXCEL': {
                return Excel::download(new PostsExport, 'appointments.xlsx');
            }
            case 'CSV': {
                return Excel::download(new PostsExport, 'appointments.csv');
            }
        }

        return view('appointments.index', compact('appointments', 'page'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        $page = 'Book an Appointment';

        $id = $request->get('id');
        $date = $request->get('date-slot', '');
        $time = $request->get('time-slot', '');
        $action = $request->get('action');
        $screeningFormData = [];
        $consentFormData = [];

        // if $id is null, no patient record yet, save form data to session
        if (is_null($id))
        {
            if ($action == "saveScreeningForm") {
                $request->validate([
                    'q1' => 'required',
                    'q2' => 'required',
                    'q3' => 'required',
                    'q4' => 'required',
                    'q5' => 'required',
                    'q6' => 'required',
                    'q7' => 'required',
                    'q8' => 'required',
                    'q9' => 'required',
                ]);
                $screeningFormData = $request->all();
                Session::put('screeningFormData', $screeningFormData);
                Session()->save();
            }

            if ($action == "saveConsentForm") {
                $consentFormData = $request->all();
                Session::put('consentFormData', $consentFormData);
                Session()->save();
            }

            if (!$date) {
                $date = now()->format('Y-m-d');
            }

            return view('appointments.create', compact('date', 'time', 'page'));
        }
        else
        {
            // $id is not null. Save form data to database.
            
            if ($action == "saveScreeningForm") {
                $request->validate([
                    'q1' => 'required',
                    'q2' => 'required',
                    'q3' => 'required',
                    'q4' => 'required',
                    'q5' => 'required',
                    'q6' => 'required',
                    'q7' => 'required',
                    'q8' => 'required',
                    'q9' => 'required',
                ]);
                $screeningFormData = $request->only([
                    'q1',
                    'q2',
                    'q3',
                    'q4',
                    'q5',
                    'q6',
                    'q7',
                    'q8',
                    'q9',
                    'body_temperature',
                    'q1_country',
                    'q2_country',
                    'q7_country',
                ]);
                $q = 0;
                $q |= number_format($screeningFormData['q1']);
                $q |= number_format($screeningFormData['q2']);
                $q |= number_format($screeningFormData['q3']);
                $q |= number_format($screeningFormData['q4']);
                $q |= number_format($screeningFormData['q5']);
                $q |= number_format($screeningFormData['q6']);
                $q |= number_format($screeningFormData['q7']);
                $q |= number_format($screeningFormData['q8']);
                $q |= number_format($screeningFormData['q9']);

                $screening = [];
                $screening['questions'] = $q;
                if (array_key_exists('q1_country', $screeningFormData)) {
                    $screening['q1_country'] = $screeningFormData['q1_country'];
                }
                if (array_key_exists('q2_country', $screeningFormData)) {
                    $screening['q2_country'] = $screeningFormData['q2_country'];
                }
                if (array_key_exists('q7_country', $screeningFormData)) {
                    $screening['q7_country'] = $screeningFormData['q2_country'];
                }
                $screening['body_temperature'] = $screeningFormData['body_temperature'];
                $screening['patient_id'] = $request->get('id');
                $screening = $this->screeningRepository->add($screening);

                if (auth()->user()->is_admin || auth()->user()->isSuperUser()) {
                    $appointments = $this->appointmentRepository->getAll();
                } else {
                    $appointments = $this->appointmentRepository->getByUserId(auth()->user()->id);
                }
                $page = 'My Appointments';
                $request->session()->flash('message', 'Screening Form added successfully.');
                return view('appointments.index', compact('appointments', 'page'));
            }
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $page = 'Edit an Appointment';

        $appointment = $this->appointmentRepository->getById($id);

        return view('appointments.edit', compact('appointment', 'page'));
    }

    /**
     * @param \App\Http\Requests\StoreAppointment $request
     * @return mixed
     */
    public function store(StoreAppointment $request)
    {
        //dd($request->file('image')->getClientOriginalName());
        $doubleBookingMsg = 'You have already booked an appointment for ';
        $hasCovidForm = $request->session()->has('screeningFormData');
        if ($hasCovidForm) {
            $screeningFormData = $request->session()->get('screeningFormData');
            $q = 0;

            if (array_key_exists('q1', $screeningFormData)) {
                $q |= number_format($screeningFormData['q1']);
            }
            if (array_key_exists('q2', $screeningFormData)) {
                $q |= number_format($screeningFormData['q2']);
            }
            if (array_key_exists('q3', $screeningFormData)) {
                $q |= number_format($screeningFormData['q3']);
            }
            if (array_key_exists('q4', $screeningFormData)) {
                $q |= number_format($screeningFormData['q4']);
            }
            if (array_key_exists('q5', $screeningFormData)) {
                $q |= number_format($screeningFormData['q5']);
            }
            if (array_key_exists('q6', $screeningFormData)) {
                $q |= number_format($screeningFormData['q6']);
            }
            if (array_key_exists('q7', $screeningFormData)) {
                $q |= number_format($screeningFormData['q7']);
            }
            if (array_key_exists('q8', $screeningFormData)) {
                $q |= number_format($screeningFormData['q8']);
            }
            if (array_key_exists('q9', $screeningFormData)) {
                $q |= number_format($screeningFormData['q9']);
            }
        }
        
        // Create patient before we set the appointment
        $patient = $request->only([
            'first_name',
            'last_name',
            'middle_name',
            'age',
            'email',
            'contact_number',
            'gender',
            'birthdate',
            'address',
            'medical_history',
            'medical_history_others',
            'allergies',
            'insurance',
            'company_name',
            'company_address',
            'last_consultation',
            'last_consultation_reason',
            'last_hospitalization',
            'last_hospitalization_reason',
            'maintain_med_name',
            'maintain_frequency',
            'med_name',
            'med_take_date',
            'med_take_time'
        ]);
        $med_hist = 0;
        if (array_key_exists('medical_history', $patient)) {
            foreach ($patient['medical_history'] as $val)
            {
                $med_hist |= number_format($val);
            }
        }
        if ($request->has('image')) {
            $image = $request->file('image');
            $originalFilename = $image->getClientOriginalName();
            $image->storePubliclyAs('uploads', $originalFilename, 'public');
            $patient['image'] = $originalFilename;
        } else {
            $patient['image'] = null;
        }
        $patient['medical_history'] = $med_hist;
        $patient['created_by'] = auth()->user()->id;

        $selectedDate = $request->get('selectedDate');
        $patient = $this->patientRepository->add($patient, $selectedDate);

        if ($patient != 'Double Booking') {
            $appointment = $request->except([
                'first_name',
                'last_name',
                'middle_name',
                'age',
                'email',
                'contact_number',
                'gender',
                'birthdate',
                'address',
                'medical_history',
                'medical_history_others',
                'allergies',
                'insurance',
                'company_name',
                'company_address',
                'last_consultation',
                'last_consultation_reason',
                'last_hospitalization',
                'last_hospitalization_reason',
                'maintain_med_name',
                'maintain_frequency',
                'med_name',
                'med_take_date',
                'med_take_time'
            ]);
            $appointment['status'] = 'For Confirmation';
            $appointment['patient_id'] = $patient->id;
            $appointment['created_by'] = auth()->user()->id;
            $appointment = $this->appointmentRepository->add($appointment);
    
            // COVID-19 Screening Form
            if ($hasCovidForm) {
                $screening = [];
                $screening['questions'] = $q;
                if (array_key_exists('q1_country', $screeningFormData)) {
                    $screening['q1_country'] = $screeningFormData['q1_country'];
                }
                if (array_key_exists('q2_country', $screeningFormData)) {
                    $screening['q2_country'] = $screeningFormData['q2_country'];
                }
                if (array_key_exists('q7_country', $screeningFormData)) {
                    $screening['q7_country'] = $screeningFormData['q2_country'];
                }
                $screening['body_temperature'] = $screeningFormData['body_temperature'];
                $screening['patient_id'] = $appointment->patient->id;
                $screening = $this->screeningRepository->add($screening);

                $request->session()->forget('screeningFormData');
            }
    
        } else {
            $request->session()->flash('doubleBooking', $doubleBookingMsg . ' ' . $selectedDate);

            return redirect()->route('appointments.create');
        }

        // $request->session()->forget('consentFormData');

        switch($request->input('action'))
        {
            case 'consent': {

                return redirect()->route('appointments.consent', [
                    'id' => $appointment->id
                ]);
            }
            case 'screening': {

                return redirect()->route('appointments.screening', [
                    'id' => $appointment->id
                ]);
            }
            case 'create': {

                $request->session()->flash('message', 'Appointment created successfully.');

                return redirect()->route('appointments.show', [
                    'id' => $appointment->id
                ]);
            }
        }
    }

    public function update($id, StoreAppointment $request)
    {
        $newSlotSelected = $request->get('schedule_time');
        $appointment = $this->appointmentRepository->getById($id);
        $previousSlotSelected = $appointment->time_slot_id;
        
        $patientUpdate = $request->only([
            'first_name',
            'last_name',
            'middle_name',
            'email',
            'contact_number',
            'gender',
            'birthdate',
            'address',
            'medical_history',
            'medical_history_others',
            'allergies',
            'insurance',
            'company_name',
            'company_address',
            'last_consultation',
            'last_consultation_reason',
            'last_hospitalization',
            'last_hospitalization_reason',
            'maintain_med_name',
            'maintain_frequency',
            'med_name',
            'med_take_date',
            'med_take_time'
        ]);
        $med_hist = 0;
        if (array_key_exists('medical_history', $patientUpdate)) {
            foreach ($patientUpdate['medical_history'] as $val)
            {
                $med_hist |= number_format($val);
            }
        }
        $patientUpdate['image'] = null;
        if ($request->has('image')) {
            $image = $request->file('image');
            $originalFilename = $image->getClientOriginalName();
            $image->storePubliclyAs('uploads', $originalFilename, 'public');
            $patientUpdate['image'] = $originalFilename;
        }
        $patientUpdate['medical_history'] = $med_hist;
        $patientUpdate['created_by'] = auth()->user()->id;

        // Update time_slots
        if ($previousSlotSelected != $newSlotSelected) {
            $this->timeSlotRepository->edit($previousSlotSelected, $newSlotSelected+0);
        }

        $appointmentUpdate = $request->only([
            'schedule_time',
            'requirements'
        ]);

        $questions = $request->only([
            'q1',
            'q2',
            'q3',
            'q4',
            'q5',
            'q6',
            'q7',
            'q8',
            'q9',
        ]);
        $q = 0;
        if (array_key_exists('q1', $questions)) {
            $q |= number_format($questions['q1']);
        }
        if (array_key_exists('q2', $questions)) {
            $q |= number_format($questions['q2']);
        }
        if (array_key_exists('q3', $questions)) {
            $q |= number_format($questions['q3']);
        }
        if (array_key_exists('q4', $questions)) {
            $q |= number_format($questions['q4']);
        }
        if (array_key_exists('q5', $questions)) {
            $q |= number_format($questions['q5']);
        }
        if (array_key_exists('q6', $questions)) {
            $q |= number_format($questions['q6']);
        }
        if (array_key_exists('q7', $questions)) {
            $q |= number_format($questions['q7']);
        }
        if (array_key_exists('q8', $questions)) {
            $q |= number_format($questions['q8']);
        }
        if (array_key_exists('q9', $questions)) {
            $q |= number_format($questions['q9']);
        }
        $screeningUpdate = $request->only([
            'body_temperature',
            'q1_country',
            'q2_country',
            'q7_country',
        ]);

        $screeningUpdate['questions'] = $q;

        // Create patient before we set the appointment
        switch($request->input('action'))
        {
            case 'updateScreening': {
                $page = 'My Appointments';

                $this->screeningRepository->update($appointment->patient->screening->id, $screeningUpdate);
                if (auth()->user()->is_admin || auth()->user()->isSuperUser()) {
                    $appointments = $this->appointmentRepository->getAllCurrent();
                } else {
                    $appointments = $this->appointmentRepository->getByUserId(auth()->user()->id);
                }

                $request->session()->flash('message', 'Screening Form updated successfully.');
                return view('appointments.index', compact('appointments', 'page'));
            }
            case 'update': {

                $this->patientRepository->update($appointment->patient_id, $patientUpdate);
                $updatedAppointment = $this->appointmentRepository->update($id, $appointmentUpdate);

                $request->session()->flash('message', 'Appointment updated successfully.');
        
                return redirect()->route('appointments.show', [
                    'id' => $updatedAppointment->id
                ]);
            }
        }

    }
    public function status($id, Request $request)
    {
        $status = $request->only([
            'status'
        ]);

        switch ($status['status'])
        {
            case "0":
                $status['status'] = "For Confirmation";
                break;
            case "1":
                $status['status'] = "Confirmed";
                break;
            case "2":
                $status['status'] = "Cancelled";
                $timeSlot = $this->timeSlotRepository->update($id);
                break;
            case "3":
                $status['status'] = "Checked-In";
                break;
            case "4":
                $status['status'] = "In-Chair";
                break;
            case "5":
                $status['status'] = "Completed";
                break;
            default:
                $status['status'] = "For Confirmation";

        }
        $appointment =  $this->appointmentRepository->updateStatus($id, $status);

        return redirect()->action('AppointmentController@index');
        //return view('appointments.show', compact('appointments'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function screening($id)
    {
        $appointment = $this->appointmentRepository->getById($id);
        return view('appointments.showscreen', compact('appointment'));
    }
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function consent($id)
    {
        $appointment = $this->appointmentRepository->getById($id);
        return view('appointments.showconsent', compact('appointment'));
    }
    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $appointment = $this->appointmentRepository->getById($id);

        return view('appointments.show', compact('appointment'));
    }
    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showscreen($id)
    {
        $appointment = $this->appointmentRepository->getById($id);

        return view('appointments.showscreen', compact('appointment'));
    }
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function calendar(Request $request)
    {
        $page = "Doctor's Calendar";
        $time_slot_message = '';

        // Start date
        $timeSlotDate = $request->get('timeSlotDate');

        // End date
        $endDate = $request->get('endDate');

        $selectedDate = $request->get('selectedDateToTimeSlot');

        // Start time
        $start_time = strtotime($request->get('dateFrom'));

        // End time
        $end_time = strtotime($request->get('dateTo'));

        // Entire month checkbox
        $entireMonth = $request->get('entireNowMonth');

        switch($request->input('action'))
        {
            case 'ADD': {
                $start_date = substr($timeSlotDate,0,10);
                $end_date = substr($endDate,0,10);

                // Date Range
                if ($endDate) {
                    if ($end_date >= $start_date) {
                        $begin = new DateTime($timeSlotDate);
                        $end = new DateTime($endDate);
                        $end = $end->modify( '+1 day' );

                        $interval = DateInterval::createFromDateString('1 day');
                        $period = new DatePeriod($begin, $interval, $end);
                        
                        // Iterate over the period
                        foreach ($period as $date) {
                            $date = $date->format('Y-m-d');
                            $weekend = strtotime($date);
                            $weekend = date("l", $weekend);
                            $weekend = strtolower($weekend);

                            if ($weekend != "sunday") {
                                if ($start_time && $end_time) {
                                    $endTime = date("H:i:s",$end_time);
                                    for( $i=$start_time; $i<=$end_time; $i+=1800) {
                                        $startTime = date("H:i:s",$i);
                                        $time_slot_message = $this->timeSlotRepository->add($date, $startTime, $endTime);
                                    }
                                }

                                if ($start_time) {
                                    $startTime = date("H:i:s",$start_time);
                                    $time_slot_message = $this->timeSlotRepository->add($date, $startTime, $end_time);
                                }
                            }
                        }
                    }
                } else {
                    if ($start_time && $end_time) {
                        $endTime = date("H:i:s",$end_time);
                        for( $i=$start_time; $i<=$end_time; $i+=1800) {
                            $timeslot = date("H:i:s",$i);
                            $time_slot_message = $this->timeSlotRepository->add($timeSlotDate, $timeslot, $endTime);
                        }
                    }

                    if ($start_time) {
                        $timeslot = date("H:i:s",$start_time);
                        $time_slot_message = $this->timeSlotRepository->add($timeSlotDate, $timeslot, $end_time);
                    }
                }
                
                break;
            }
            case 'REMOVE': {
                if ($endDate) {
                    $begin = new DateTime($timeSlotDate);
                    $end = new DateTime($endDate);
                    $end = $end->modify( '+1 day' );

                    $interval = DateInterval::createFromDateString('1 day');
                    $period = new DatePeriod($begin, $interval, $end);

                    // Iterate over the period
                    foreach ($period as $date) {
                        $date = $date->format('Y-m-d');

                        if ($start_time && $end_time) {
                            for( $i=$start_time; $i<=$end_time; $i+=1800) {
                                $timeslot = date("H:i:s",$i);
                                $time_slot_message = $this->timeSlotRepository->remove($date, $timeslot);
                            }
                        }

                        if ($start_time) {
                            $timeslot = date("H:i:s",$start_time);
                            $time_slot_message = $this->timeSlotRepository->remove($date, $timeslot);
                        }
                        
                    }
                } else {
                    if ($start_time && $end_time) {
                        for( $i=$start_time; $i<=$end_time; $i+=1800) {
                            $timeslot = date("H:i:s",$i);
                            $time_slot_message = $this->timeSlotRepository->remove($timeSlotDate, $timeslot);
                        }
                    }
    
                    if ($start_time) {
                        $timeslot = date("H:i:s",$start_time);
                        $time_slot_message = $this->timeSlotRepository->remove($timeSlotDate, $timeslot);
                    }
                }          

                break;
            }
        }

        return view('appointments.calendar', compact('selectedDate', 'page'))->withErrors($time_slot_message);;
    }

    /**
     * Api to show calendar events
     * @param \App\Http\Requests\CalendarEvent $request
     * @return \App\Http\Resources\CalendarEvents
     */
    public function eventList(Request $request)
    {
        $appointments = $this->appointmentRepository->getAll();
        $events=[];
        foreach ($appointments as $val) {
            $arr = [
                'start' => $val->slot_date.' '.$val->slot_time,
                'title' => $val->patient->last_name
            ];
            array_push($events, $arr);
        }
        return response()->json($events);
    }

    public function slots(CalendarTimeSlot $request)
    {
        $slots = $this->timeSlotRepository->getByDate($request->get('date'));

        return response()->json($slots);
    }

    public function appointmentsByDate(Request $request)
    {
        if (auth()->user()->is_admin || auth()->user()->isSuperUser()) {
            $appointments = $this->appointmentRepository->getAllAndDate($request->get('date'));
        } else {
            $appointments = $this->appointmentRepository->getByUserIdAndDate(auth()->user()->id, $request->get('date'));
        }
        return response()->json($appointments);
    }

    public function patientlist()
    {
        $id = auth()->user()->id;
        $patients = $this->patientRepository->getCreatedBy($id);
        $patientsCount = count($patients);

        return view('appointments.patientlist', compact('patients', 'patientsCount'));
    }

    public function superuser(Request $request)
    {
        if (!auth()->user()->isSuperUser()) {
            return response()->json(['URL' => 'Not Found.'], 404);
        }

        $action = $request->get('action');


        $id = auth()->user()->id;
        $users = $this->patientRepository->getAllUsersEmail($id);

        return view('appointments.superuser', compact('users'));
    }

    public function updateUserRole(Request $request)
    {

        $email = $request->get('user');

        $doctorUpdate = [];
        $doctorUpdate['email'] = $email;
        $doctorUpdate['license'] = $request->get('license_no');
        $doctorUpdate['name'] = $this->patientRepository->getDoctorsNameByEmail($email)->first()->name;

        if ($request->has('image')) {
            $image = $request->file('image');
            $image = $image->store('uploads','public');
            $doctorUpdate['signature'] = $image;

        } else {
            $doctorUpdate['signature'] = null;
        }

        $userRole = $request->get('role');

        if ($userRole == 'admin') {
            User::where('email', $email)->update(array('is_admin' => 1));
        }

        if ($userRole == 'doctor') {
            User::where('email', $email)->update(array('is_admin' => 1));
            if ($this->doctorRepository->getByEmail($email)->isEmpty()) {
                $this->doctorRepository->add($doctorUpdate);
            } else {
                $this->doctorRepository->updateByEmail($email, $doctorUpdate);
            }
        }

        return redirect()->back()->with("message", "User Role successfully changed.");

    }

    public function patientdetails($id, Request $request)
    {
        $page = 'Book an Appointment';
        
        $patients = $this->patientRepository->getById($id);

        $date = $request->get('date-slot', '');
        $time = $request->get('time-slot', '');

        return view('appointments.patientdetail', compact('date', 'time', 'page', 'patients'));
        
    }
    
    public function storeExisting(StoreAppointment $request) {
        $appointment = $request->only([
            'patient_id',
            'requirements',
            'schedule_time'
        ]);
        $appointment['status'] = 'For Confirmation';
        $appointment['created_by'] = auth()->user()->id;
        $appointment = $this->appointmentRepository->add($appointment);

        $request->session()->flash('message', 'Appointment created successfully.');

        return redirect()->route('appointments.show', [
            'id' => $appointment->id
        ]);
    }

}
