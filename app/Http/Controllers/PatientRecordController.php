<?php

namespace App\Http\Controllers;
use App\Repositories\PatientRecordRepositoryInterface;
use App\Repositories\AppointmentRepositoryInterface;
use App\Repositories\PatientRepositoryInterface;
use App\Repositories\PrescriptionRepositoryInterface;

use Illuminate\Http\Request;

class PatientRecordController extends Controller
{
    /**
     * @var \App\Repositories\PatientRecordRepositoryInterface
     */
    private $appointmentRepository;
    private $patientRepository;
    private $prescriptionRepository;
    private $patientrecordRepository;

    public function __construct(
        AppointmentRepositoryInterface $appointmentRepository,
        PatientRepositoryInterface $patientRepository,
        PrescriptionRepositoryInterface $prescriptionRepository,
        PatientRecordRepositoryInterface $patientrecordRepository
    )
    {
        $this->appointmentRepository = $appointmentRepository;
        $this->patientRepository = $patientRepository;
        $this->patientrecordRepository = $patientrecordRepository;
        $this->prescriptionRepository = $prescriptionRepository;
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showpatientrecord($id, $referer)
    {
        $patientRecord = $this->patientrecordRepository->getById($id);

        return view('appointments.showpatientrecord', compact('patientRecord', 'referer'));
    }

    public function patientrecordupdate($id, $referer,  Request $request)
    {
        $patientRecord = $this->patientrecordRepository->getById($id);
        $patient_id = $patientRecord->patient->id;
        $patientRecordUpdate = $request->only([
            'date',
            'reason',
            'notes'
        ]);
        $this->patientrecordRepository->update($id, $patientRecordUpdate);
        return redirect()->route('appointments.patienthistory', [
            'id' => $patient_id,
            'referer' => $referer
        ])->with("message", "Patient Record has been updated successfully!");
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function patientrecord($id, $referer)
    {
        $page = "Create Patient Record";
        $appointment = $this->appointmentRepository->getById($id);
        return view('appointments.patientrecord', compact('appointment','page', 'referer'));
    }

    public function createpatientrecord($referer, Request $request)
    {
        $action = $request->get('action');

        $request->validate([
            'date' => 'required',
        ]);
        $patientRecord = [];
        $patientRecord['patient_id'] = $request->get('id');
        $patientRecord['reason'] = $request->get('reason');
        $patientRecord['date'] = $request->get('date');
        $patientRecord['notes'] = $request->get('notes');
        $patientRecord = $this->patientrecordRepository->add($patientRecord);

        if (auth()->user()->is_admin || auth()->user()->isSuperUser()) {
            $appointments = $this->appointmentRepository->getAllCurrent();
        } else {
            $appointments = $this->appointmentRepository->getByUserId(auth()->user()->id);
        }
        $page = 'My Appointments';
        $request->session()->flash('createpatientrecord', 'Patient Record has been added successfully.');

        if ($action == 'savePatientRecord') {
            $id = $request->get('id');
            $patient = $this->patientRepository->getById($id);
            $fullname = $patient->first_name.' '.$patient->last_name;
            $page = "Patient History: ".$fullname;
            return redirect()->route('appointments.patienthistory', ['id' => $id, 'referer' => $referer]);
        } else {
            return view('appointments.index', compact('appointments', 'page'));
        }
    }

}
