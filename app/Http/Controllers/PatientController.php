<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use App\Repositories\PatientRepositoryInterface;
use App\Repositories\PatientRecordRepositoryInterface;
use App\Repositories\PrescriptionRepositoryInterface;
use Illuminate\Support\Facades\DB;

class PatientController extends Controller
{
    /**
     * @var \App\Repositories\PatientRepositoryInterface
     */
    private $patientRepository;
    private $patientRecordRepository;
    private $prescriptionRepository;


    public function __construct(
        PatientRepositoryInterface $patientRepository,
        PatientRecordRepositoryInterface $patientRecordRepository,
        PrescriptionRepositoryInterface $prescriptionRepository
    )
    {
        $this->patientRepository = $patientRepository;
        $this->patientRecordRepository = $patientRecordRepository;
        $this->prescriptionRepository = $prescriptionRepository;
    }

    /**
     * @return mixed
     */
    public function index(Request $request)
    {
        $patients = [];
        switch($request->input('action'))
        {
            case 'ABCDE':
            default: {
                $patients = DB::table('patients')
                ->where('last_name', 'like', 'A%')
                ->orWhere('last_name', 'like', 'B%')
                ->orWhere('last_name', 'like', 'C%')
                ->orWhere('last_name', 'like', 'D%')
                ->orWhere('last_name', 'like', 'E%')
                ->orderBy('last_name', 'asc')
                ->get(['id', 'first_name', 'last_name', 'middle_name', 'contact_number', 'email', 'gender', 'updated_at']);

                break;
            }
            case 'FGHIJ': {
                $patients = DB::table('patients')
                ->where('last_name', 'like', 'F%')
                ->orWhere('last_name', 'like', 'G%')
                ->orWhere('last_name', 'like', 'H%')
                ->orWhere('last_name', 'like', 'I%')
                ->orWhere('last_name', 'like', 'J%')
                ->orderBy('last_name', 'asc')
                ->get(['id', 'first_name', 'last_name', 'middle_name', 'contact_number', 'email', 'gender', 'updated_at']);

                break;
            }
            case 'KLMNO': {
                $patients = DB::table('patients')
                ->where('last_name', 'like', 'K%')
                ->orWhere('last_name', 'like', 'L%')
                ->orWhere('last_name', 'like', 'M%')
                ->orWhere('last_name', 'like', 'N%')
                ->orWhere('last_name', 'like', 'O%')
                ->orderBy('last_name', 'asc')
                ->get(['id', 'first_name', 'last_name', 'middle_name', 'contact_number', 'email', 'gender', 'updated_at']);

                break;
            }
            case 'PQRST': {
                $patients = DB::table('patients')
                ->where('last_name', 'like', 'P%')
                ->orWhere('last_name', 'like', 'Q%')
                ->orWhere('last_name', 'like', 'R%')
                ->orWhere('last_name', 'like', 'S%')
                ->orWhere('last_name', 'like', 'T%')
                ->orderBy('last_name', 'asc')
                ->get(['id', 'first_name', 'last_name', 'middle_name', 'contact_number', 'email', 'gender', 'updated_at']);

                break;
            }
            case 'UVWXYZ': {
                $patients = DB::table('patients')
                ->where('last_name', 'like', 'U%')
                ->orWhere('last_name', 'like', 'V%')
                ->orWhere('last_name', 'like', 'W%')
                ->orWhere('last_name', 'like', 'X%')
                ->orWhere('last_name', 'like', 'Y%')
                ->orWhere('last_name', 'like', 'Z%')
                ->orderBy('last_name', 'asc')
                ->get(['id', 'first_name', 'last_name', 'middle_name', 'contact_number', 'email', 'gender', 'updated_at']);

                break;
            }
        }

        return view('appointments.patients', compact('patients'));
    }
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function patienthistory($patient_id, $referer)
    {
        $patient = $this->patientRepository->getById($patient_id);
        $fullname = $patient->first_name.' '.$patient->last_name;
        $page = "Patient History: ".$fullname;
        $prescriptions = $this->prescriptionRepository->getByPatientId($patient_id);
        $patientRecords = $this->patientRecordRepository->getByPatientId($patient_id);
        return view('appointments.patienthistory', compact('patient', 'prescriptions', 'patientRecords', 'page', 'referer'));
    }

    public function download($id)
    {
        $patient = $this->patientRepository->getById($id);
        $file = public_path().'/storage/uploads/'.$patient->image;
        $headers = array(
            'Content-Type: image/png'
        );

        return Response::download($file, $patient->image, $headers);
    }

    public function search(Request $request)
    {
        if ($request->ajax()) {
            $output = "";
            $patients = DB::table('patients')
            ->where('last_name', 'like', '%'.$request->search."%")
            ->orWhere('first_name', 'like', '%'.$request->search."%")
            ->orWhere('email', 'like', '%'.$request->search."%")
            ->get();
            if ($patients) {
                foreach ($patients as $key => $patient) {
                    $output.='<tr class="d-flex">'.
                    '<td class="text-center col-2">'.$patient->first_name.' '.$patient->last_name.'</td>'.
                    '<td class="text-center col-2">'.$patient->contact_number.'</td>'.
                    '<td class="text-center col-3">'.$patient->email.'</td>'.
                    '<td class="text-center col-1">'.$patient->gender.'</td>'.
                    '<td class="text-center col-3">'.date('M-d-Y h:m A', strtotime($patient->updated_at)).'</td>'.
                    '<td class="text-center col-1">'.
                    '<a href="patienthistory/'.$patient->id.'"'.' rel="tooltip" title="Show Patient History" class="btn btn-success btn-link btn-xs">'.
                    '<i class="fa fa-history"></i>'.
                    '</a></td>'.
                    '</tr>';
                }

                return response($output);
            }
        }
    }
}