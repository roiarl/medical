<?php

namespace App\Http\Controllers;
use App\Repositories\PrescriptionRepositoryInterface;
use App\Repositories\AppointmentRepositoryInterface;
use App\Repositories\DoctorRepositoryInterface;
use App\Repositories\PatientRepositoryInterface;
use App\Repositories\PatientRecordRepositoryInterface;

use Illuminate\Http\Request;

class PrescriptionController extends Controller
{
    /**
     * @var \App\Repositories\PrescriptionRepositoryInterface
     */
    private $appointmentRepository;
    private $patientRepository;
    private $prescriptionRepository;
    private $patientRecordRepository;
    private $doctorRepository;

    public function __construct(
        AppointmentRepositoryInterface $appointmentRepository,
        PatientRepositoryInterface $patientRepository,
        PrescriptionRepositoryInterface $prescriptionRepository,
        PatientRecordRepositoryInterface $patientRecordRepository,
        DoctorRepositoryInterface $doctorRepository
    )
    {
        $this->appointmentRepository = $appointmentRepository;
        $this->patientRepository = $patientRepository;
        $this->prescriptionRepository = $prescriptionRepository;
        $this->patientRecordRepository = $patientRecordRepository;
        $this->doctorRepository = $doctorRepository;
    }

    public function getByPatientId($patient_id)
    {
        return Patient::where('patient_id', $patient_id);
    }

    public function prescriptionupdate($id, $referer, Request $request)
    {
        $prescription = $this->prescriptionRepository->getById($id);
        $patient_id = $prescription->patient->id;
        $prescriptionUpdate = $request->only([
            'doctor_name',
            'license_no',
            'date',
            'signature',
            'rx',
            'sig',
            'dispensing_instructions'
        ]);

        $this->prescriptionRepository->update($id, $prescriptionUpdate);
        return redirect()->route('appointments.patienthistory', [
            'id' => $patient_id,
            'referer' => $referer
        ])->with("message", "Prescription form updated successfully!");
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showprescription($id, $referer)
    {
        $prescription = $this->prescriptionRepository->getById($id);

        return view('appointments.showprescription', compact('prescription', 'referer'));
    }
    
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function prescriptionform($id, $referer)
    {
        $page = "Create Prescription form";

        $user_email = auth()->user()->email;
        $doctors = $this->doctorRepository->getByEmail($user_email);


        $appointment = $this->appointmentRepository->getById($id);
        return view('appointments.prescriptionform', compact('appointment','page','doctors', 'referer'));
    }

    public function createprescription($referer, Request $request)
    {
        $action = $request->get('action');

        $request->validate([
            'doctor_name' => 'required',
            'license_no' => 'required',
            'sig' => 'required',
            'rx' => 'required',
        ]);
        $prescription = [];
        $prescription['patient_id'] = $request->get('id');
        $prescription['doctor_name'] = $request->get('doctor_name');
        $prescription['license_no'] = $request->get('license_no');
        $prescription['date'] = $request->get('date');
        $prescription['rx'] = $request->get('rx');
        $prescription['sig'] = $request->get('sig');
        $prescription['signature'] = $request->get('signature');
        $prescription['dispensing_instructions'] = $request->get('dispensing_instructions');
        $prescription = $this->prescriptionRepository->add($prescription);

        if (auth()->user()->is_admin || auth()->user->isSuperUser()) {
            $appointments = $this->appointmentRepository->getAllCurrent();
        } else {
            $appointments = $this->appointmentRepository->getByUserId(auth()->user()->id);
        }
        $page = 'My Appointments';
        $request->session()->flash('createprescription', 'Prescription Form added successfully.');

        if ($action == 'savePrescriptionForm') {
            $id = $request->get('id');
            $patient = $this->patientRepository->getById($id);
            $fullname = $patient->first_name.' '.$patient->last_name;
            $page = "Patient History: ".$fullname;
            return redirect()->route('appointments.patienthistory', ['id' => $id, 'referer' => $referer]);
        } else {
            return view('appointments.index', compact('appointments', 'page'));
        }
    }
}
