<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAppointment extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'schedule_date.required' => 'The date field is required.',
            'schedule_time.required'  => 'The time field is required.'
        ];
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'image' => 'file|image|max:500',
            'middle_name' => '',
            'email' => 'required',
            'contact_number' => 'required',
            'gender' => 'required',
            'address' => 'required',
            'birthdate' => 'required',
            'medical_history' => '',
            'schedule_date' => 'required', //|date_format:Y/m/d',
            'schedule_time' => 'required', //|date_format:h:i A',
            'requirements' => 'required',
            'med_name' => '',
            'med_take_date' => '',
            'med_take_time' => ''
        ];
    }
}
