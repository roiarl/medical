<?php

namespace App;

use App\Events\AppointmentCreated;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Appointment extends Model
{
    use Notifiable;

    protected $dispatchesEvents = [
        'created' => AppointmentCreated::class
    ];

    public function patient()
    {
        return $this->belongsTo('App\Patient');
    }

    public function timeSlot()
    {
        return $this->belongsTo('App\TimeSlot');
    }
}
