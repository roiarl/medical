<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    public function screening()
    {
        return $this->hasOne(Screening::class);
    }

    public function consent()
    {
        return $this->hasOne(Consent::class);
    }

    public function prescription()
    {
        return $this->hasMany(Prescription::class);
    }

    public function appointment()
    {
        return $this->hasMany(Appointment::class);
    }

    public function patientRecord()
    {
        return $this->hasMany(PatientRecord::class);
    }
}
