<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class TimeSlot extends Model
{
    public function getSlotDateAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d', $date)->format('Y-m-d');
    }

    public function getSlotTimeAttribute($date)
    {
        return Carbon::createFromFormat('H:i:s', $date)->format('h:i A');
    }
}
