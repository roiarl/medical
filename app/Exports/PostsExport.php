<?php

namespace App\Exports;

use App\Appointment;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PostsExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Appointment::join('time_slots', 'appointments.time_slot_id', '=', 'time_slots.id')
        ->join('patients', 'appointments.patient_id', '=', 'patients.id')
        ->select('patients.id', 'patients.first_name', 'patients.last_name', 'patients.middle_name', 'patients.gender',
        'appointments.status', 'time_slots.slot_date', 'time_slots.slot_time')
        ->orderBy('time_slots.slot_date', 'asc')
        ->get();
    }

    public function headings() : array{
        return [
            'Client ID',
            'Firstname',
            'Lastname',
            'Middlename',
            'Gender',
            'Status',
            'Date',
            'Time',
        ];
    }
}
