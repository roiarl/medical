<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Consent extends Model
{
    public function patient()
    {
        return $this->belongsTo(Patient::class);
    }
}
