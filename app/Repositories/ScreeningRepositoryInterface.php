<?php

namespace App\Repositories;

interface ScreeningRepositoryInterface
{
    public function getAll();

    public function getById($id);

    public function add($screening);

    public function update($id, $screeningUpdate);

    public function remove($id);
}