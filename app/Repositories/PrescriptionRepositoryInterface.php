<?php

namespace App\Repositories;

interface PrescriptionRepositoryInterface
{
    public function getAll();

    public function getById($id);

    public function add($prescription);

    public function update($id, $prescriptionUpdate);

    public function remove($id);
    public function getByPatientId($patient_id);
}