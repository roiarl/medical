<?php

namespace App\Repositories;

use App\Patient;
use Carbon\Carbon;
use App\Consent;

class ConsentRepository implements ConsentRepositoryInterface
{
    public function getAll()
    {
        // TODO: Implement getAll() method.
    }

    public function getById($id)
    {
        // TODO: Implement getById() method.
    }

    public function add($consent)
    {
        $model = new Consent;
        
        $model->patient_id = $consent['patient_id'];
        $model->initial_1 = $consent['initial_1'];
        $model->initial_2 = $consent['initial_2'];
        $model->initial_3 = $consent['initial_3'];
        $model->initial_4 = $consent['initial_4'];
        $model->initial_5 = $consent['initial_5'];

        $model->save();

        return $model;

    }

    public function update($id, $consentUpdate)
    {
        $model = Consent::find($id);

        $model->initial_1 = $consentUpdate['initial_1'];
        $model->initial_2 = $consentUpdate['initial_2'];
        $model->initial_3 = $consentUpdate['initial_3'];
        $model->initial_4 = $consentUpdate['initial_4'];
        $model->initial_5 = $consentUpdate['initial_5'];

        $model->save();
    }

    public function remove($id)
    {
        // TODO: Implement remove() method.
    }
}