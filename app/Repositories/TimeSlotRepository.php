<?php
namespace App\Repositories;

use App\TimeSlot;
use App\Appointment;
use Carbon\Carbon;

class TimeSlotRepository implements TimeSlotRepositoryInterface
{
    public function getAll()
    {

    }

    public function getById($id)
    {

    }

    public function getAllTimeSlotByMonth($date)
    {
        $dateObj = strtotime($date);
        $month = date("m",$dateObj);

        return TimeSlot::whereMonth('slot_date', $month)
            ->where('slot_count', 1)
            ->get();

    }
    public function getByDate($date)
    {
        return TimeSlot::whereDate('slot_date', $date)->get();
    }

    public function add($slot_date, $slot_time, $end_time)
    {
        $errorDateTime = 'Cannot add timeslot earlier than the current date/time.';
        $invalidTimeRange = 'Invalid time range, please select time properly.';
        $existingTimeSlot = 'Selected time slot is already existing.';
        $addedSuccessful = 'Time slot successfully added.';

        $dateSet = $slot_date.' '.$slot_time;
        $datenow = date('yy-m-d H:i:s', time());

        if ($dateSet < $datenow) {
            return $errorDateTime;
        }

        if ($end_time) {
            if (is_int($end_time)) {
                $end_time = date("H:i:s",$end_time);
            }
            
            if ($slot_time > $end_time) {

                return $invalidTimeRange;
            }

            //$model = TimeSlot::where('slot_date', $slot_date)
            //            ->whereRaw("'$slot_time' BETWEEN slot_time AND slot_time_to")
            //            ->get();
        
            //$model1 = TimeSlot::where('slot_date', $slot_date)
            //            ->whereRaw("'$end_time' BETWEEN slot_time AND slot_time_to")
            //            ->get();
        
            $model2 = TimeSlot::where('slot_date', $slot_date)
                        ->where('slot_time', $slot_time)
                        ->get();

            if ($model2->isEmpty()) {
                $model = new TimeSlot();
                $model->slot_date = $slot_date;
                $model->slot_time = $slot_time;
                $model->slot_count = '2';
                $model->slot_status = 'NULL';
                $model->is_disabled = 0;
                $model->created_at = Carbon::now();
                $model->updated_at = Carbon::now();

                $model->save();
            }
        } else {
            $model = TimeSlot::where('slot_date', $slot_date)
                                ->where('slot_time', $slot_time)
                                ->get();

            //$model1 = TimeSlot::where('slot_date', $slot_date)
            //                    ->whereRaw("'$slot_time' BETWEEN slot_time AND slot_time_to")
            //                    ->get();

            if ($model->isEmpty()) {
                $model = new TimeSlot();
                $model->slot_date = $slot_date;
                $model->slot_time = $slot_time;
                $model->slot_count = '2';
                $model->slot_status = 'NULL';
                $model->is_disabled = 0;
                $model->created_at = Carbon::now();
                $model->updated_at = Carbon::now();

                $model->save();
            }
        }
    }

    public function update($id)
    {
        $appointmentModel = Appointment::find($id);

        $time_slot_id = $appointmentModel->time_slot_id;

        $time_slot_model = TimeSlot::find($time_slot_id);

        $time_slot_model->slot_count = '2';

        $time_slot_model->save();
    }

    public function edit($previousSlotSelected, $newSlotSelected)
    {
        // Free up previous slot selected
        $time_slot_model = TimeSlot::find($previousSlotSelected);
        $time_slot_model->slot_count = '2';
        $time_slot_model->save();
        
        // Reserved slot selected
        $time_slot_model = TimeSlot::find($newSlotSelected);
        $time_slot_model->slot_count = '1';
        $time_slot_model->save();
    }

    public function remove($timeSlotDate, $fromDate)
    {
        $deleteSuccessful = 'Time slots successfully deleted';
        $noTimeSlotsAvailable = 'Time slot selected is not existing';

        $model = TimeSlot::where('slot_date', $timeSlotDate)
                    ->where('slot_time', $fromDate)
                    ->get();

        //$model1 = TimeSlot::where('slot_date', $timeSlotDate)
        //                    ->whereRaw("'$fromDate' BETWEEN slot_time AND slot_time_to")
        //                    ->get();

        if ($model->isNotEmpty()) {
            foreach ($model as $post) {
                $post->delete();
            }
            return $deleteSuccessful;
        }

        //if ($model1->isNotEmpty()) {
        //    foreach ($model as $post) {
        //        $post->delete();
        //    }
        //    return $deleteSuccessful;
        //}
    }
}