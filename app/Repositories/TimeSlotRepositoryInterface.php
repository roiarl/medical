<?php

namespace App\Repositories;

interface TimeSlotRepositoryInterface
{
    public function getAll();

    public function getById($id);
    public function getAllTimeSlotByMonth($date);

    public function getByDate($date);

    public function add($slot_date, $timeslot, $end_time);

    public function update($id);

    public function remove($timeSlotDate, $fromDate);

    public function edit($previousSlotSelected, $newSlotSelected);
}