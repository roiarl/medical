<?php

namespace App\Repositories;

use App\Patient;
use Carbon\Carbon;
use App\Screening;

class ScreeningRepository implements ScreeningRepositoryInterface
{
    public function getAll()
    {
        // TODO: Implement getAll() method.
    }

    public function getById($id)
    {
        // TODO: Implement getById() method.
    }

    public function add($screening)
    {
        $model = new Screening;
        
        $model->patient_id = $screening['patient_id'];
        $model->questions = $screening['questions'];
        $model->body_temperature = $screening['body_temperature'];
        $model->q1_country = $screening['q1_country'];
        $model->q2_country = $screening['q2_country'];
        $model->q7_country = $screening['q7_country'];

        $model->save();

        return $model;

    }

    public function update($id, $screeningUpdate)
    {
        $model = Screening::find($id);

        $model->questions = $screeningUpdate['questions'];
        $model->body_temperature = $screeningUpdate['body_temperature'];
        $model->q1_country = $screeningUpdate['q1_country'];
        $model->q2_country = $screeningUpdate['q2_country'];
        $model->q7_country = $screeningUpdate['q7_country'];

        $model->save();
    }

    public function remove($id)
    {
        // TODO: Implement remove() method.
    }
}