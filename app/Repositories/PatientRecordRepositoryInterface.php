<?php

namespace App\Repositories;

interface PatientRecordRepositoryInterface
{
    public function getAll();

    public function getById($id);

    public function add($patientRecord);

    public function update($id, $patientRecordUpdate);

    public function remove($id);
    public function getByPatientId($patient_id);
}