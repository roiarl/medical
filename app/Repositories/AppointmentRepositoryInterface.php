<?php

namespace App\Repositories;

interface AppointmentRepositoryInterface
{
    public function getAll();

    public function getAllCurrent();

    public function getByUserId($userId);

    public function getByUserIdAndDate($userId, $date);
    public function getAllAndDate($date);

    public function getById($id);

    public function add($appointment);

    public function update($id, $appointmentUpdate);
    public function updateStatus($d, $statusUpdate);
    
    public function remove($id);

    public function getAppointmentsToday();

    public function countAppointmentsToday();

    public function countAppointmentsThisWeek();

    public function countAppointmentsThisMonth();

    public function getEvents($start, $end);

    public function filterByStatus($status);
}