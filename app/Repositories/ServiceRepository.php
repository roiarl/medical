<?php

namespace App\Repositories;

use App\Service;

class ServiceRepository implements ServiceRepositoryInterface
{
    public function getAll()
    {
        return Service::all();
    }

    public function getById($id)
    {
        // TODO: Implement getById() method.
    }

    public function add($service)
    {
        // TODO: Implement add() method.
    }

    public function update($id)
    {
        // TODO: Implement update() method.
    }

    public function remove($id)
    {
        // TODO: Implement remove() method.
    }
}