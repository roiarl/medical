<?php

namespace App\Repositories;

use App\Patient;
use App\User;
use App\Doctor;
use Carbon\Carbon;
use DB;

class PatientRepository implements PatientRepositoryInterface
{
    public function getAll()
    {
        // TODO: Implement getAll() method.
    }

    public function getById($id)
    {
        return Patient::find($id);
    }

    public function add($patient, $selectedDate)
    {
        $doubleBooking = 'Double Booking';
        //$date = Carbon::now()->toDateString();

        $model = Patient::join('appointments', 'appointments.patient_id', '=', 'patients.id')
                    ->join('time_slots', 'appointments.time_slot_id', '=', 'time_slots.id')
                    ->where('patients.first_name', $patient['first_name'])
                    ->where('patients.last_name', $patient['last_name'])
                    ->where('patients.email', $patient['email'])
                    ->where('patients.contact_number', $patient['contact_number'])
                    ->where('time_slots.slot_date', $selectedDate)
                    ->get();

        //$model = Patient::whereDate('created_at', $date)
        //            ->where('first_name', $patient['first_name'])
        //            ->where('last_name', $patient['last_name'])
        //            ->where('email', $patient['email'])
        //            ->where('contact_number', $patient['contact_number'])
        //            ->get();

        if ($model->isEmpty()) {
            $model = new Patient;

            $model->first_name = $patient['first_name'];
            $model->last_name = $patient['last_name'];
            $model->middle_name = $patient['middle_name'];
            $model->image = $patient['image'];
            //$model->age = $patient['age'];
            $model->contact_number = $patient['contact_number'];
            $model->gender = $patient['gender'];
            $model->email = $patient['email'];
            $model->birthday = Carbon::parse($patient['birthdate'])->format('Y-m-d');
            $model->address = $patient['address'];
            $model->medical_history = $patient['medical_history'];
            $model->medical_history_others = $patient['medical_history_others'];
            $model->allergies = $patient['allergies'];
            $model->insurance = $patient['insurance'];
            $model->company_name = $patient['company_name'];
            $model->company_address = $patient['company_address'];
            $model->last_consultation = $patient['last_consultation'];
            $model->last_consultation_reason = $patient['last_consultation_reason'];
            $model->last_hospitalization = $patient['last_hospitalization'];
            $model->last_hospitalization_reason = $patient['last_hospitalization_reason'];
            $model->maintain_med_name = implode(',', $patient['maintain_med_name']);
            $model->maintain_frequency = implode(',', $patient['maintain_frequency']);
            $model->med_name = implode(',', $patient['med_name']);
            $model->med_take_date = implode(',', $patient['med_take_date']);
            $model->med_take_time = implode(',', $patient['med_take_time']);
            $model->created_by = $patient['created_by'];

            $model->save();

            return $model;
        } else {
            return $doubleBooking;
        }        
    }

    public function update($id, $patientUpdate)
    {
        $model = Patient::find($id);

        $model->first_name = $patientUpdate['first_name'];
        $model->last_name = $patientUpdate['last_name'];
        $model->middle_name = $patientUpdate['middle_name'];
        $model->image = $patientUpdate['image'];
        $model->contact_number = $patientUpdate['contact_number'];
        $model->gender = $patientUpdate['gender'];
        $model->email = $patientUpdate['email'];
        $model->company_name = $patientUpdate['company_name'];
        $model->company_address = $patientUpdate['company_address'];
        $model->insurance = $patientUpdate['insurance'];
        $model->birthday = Carbon::parse($patientUpdate['birthdate'])->format('Y-m-d');
        $model->address = $patientUpdate['address'];
        $model->medical_history = $patientUpdate['medical_history'];
        $model->medical_history_others = $patientUpdate['medical_history_others'];
        $model->med_name = implode(',', $patientUpdate['med_name']);
        $model->med_take_date = implode(',', $patientUpdate['med_take_date']);
        $model->med_take_time = implode(',', $patientUpdate['med_take_time']);

        $model->save();

        return $model;
    }

    public function remove($id)
    {
        // TODO: Implement remove() method.
    }

    public function getCreatedBy($id)
    {
        $model = Patient::select("*", DB::raw("CONCAT(first_name,' ',last_name) AS full_name"))
                    ->where('created_by', $id)
                    ->groupBy('full_name')
                    ->get();

        return $model;
    }

    public function getAllUsersEmail($id)
    {
        $users = User::where('id', '!=', $id)->get();

        return $users;
    }

    public function getDoctorsNameByEmail($email)
    {
        $usersname = User::select('name')
                    ->where('email', $email)->get();

        return $usersname;
    }

}