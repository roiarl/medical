<?php

namespace App\Repositories;

interface UserRepositoryInterface
{
    public function getAll();

    public function getById($id);

    public function add($user);

    public function update($id);

    public function remove($id);
}