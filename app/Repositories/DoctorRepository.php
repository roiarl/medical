<?php

namespace App\Repositories;

use App\Doctor;
use Carbon\Carbon;

class DoctorRepository implements DoctorRepositoryInterface
{
    public function getAll()
    {
        return Doctor::all();
    }

    public function getById($id)
    {
        return Doctor::find($id);
    }

    public function getByEmail($email)
    {
        return Doctor::where('email', $email)
            ->get();
    }

    public function add($doctor)
    {
        $model = new Doctor();
        $model->email = $doctor['email'];
        $model->name = $doctor['name'];
        $model->license = $doctor['license'];
        $model->signature = $doctor['signature'];
        $model->created_at = Carbon::now();
        $model->updated_at = Carbon::now();

        $model->save();

        return $model;
    }

    public function update($id)
    {
        // TODO: Implement update() method.
    }

    public function updateByEmail($email, $doctorUpdate)
    {
        $doctor = Doctor::where('email', $email)->get();

        $model = Doctor::find($doctor->first()->id);

        $model->name = $doctorUpdate['name'];
        $model->license = $doctorUpdate['license'];
        $model->signature = $doctorUpdate['signature'];
        $model->updated_at = Carbon::now();

        $model->save();

        return $model;

    }
    public function remove($id)
    {
        // TODO: Implement remove() method.
    }
}