<?php

namespace App\Repositories;

interface ServiceRepositoryInterface
{
    public function getAll();

    public function getById($id);

    public function add($service);

    public function update($id);

    public function remove($id);
}