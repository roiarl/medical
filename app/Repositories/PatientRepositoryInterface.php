<?php

namespace App\Repositories;

interface PatientRepositoryInterface
{
    public function getAll();

    public function getById($id);

    public function add($patient, $selectedDate);

    public function update($id, $patientUpdate);

    public function remove($id);

    public function getCreatedBy($id);

    public function getAllUsersEmail($id);

    public function getDoctorsNameByEmail($email);

}