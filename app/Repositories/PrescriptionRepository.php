<?php

namespace App\Repositories;

use App\Patient;
use Carbon\Carbon;
use App\Prescription;

class PrescriptionRepository implements PrescriptionRepositoryInterface
{
    public function getAll()
    {
        // TODO: Implement getAll() method.
    }

    public function getById($id)
    {
        return Prescription::find($id);
    }

    public function add($prescription)
    {
        $model = new Prescription;
        
        $model->patient_id = $prescription['patient_id'];
        $model->doctor_name = $prescription['doctor_name'];
        $model->license_no = $prescription['license_no'];
        $model->date = $prescription['date'];
        $model->rx = $prescription['rx'];
        $model->sig = $prescription['sig'];
        $model->signature = $prescription['signature'];
        $model->dispensing_instructions = $prescription['dispensing_instructions'];

        $model->save();

        return $model;

    }

    public function update($id, $prescriptionUpdate)
    {
        $model = Prescription::find($id);

        $model->doctor_name = $prescriptionUpdate['doctor_name'];
        $model->license_no = $prescriptionUpdate['license_no'];
        $model->date = $prescriptionUpdate['date'];
        $model->rx = $prescriptionUpdate['rx'];
        $model->sig = $prescriptionUpdate['sig'];
        $model->signature = $prescriptionUpdate['signature'];
        $model->dispensing_instructions = $prescriptionUpdate['dispensing_instructions'];

        $model->save();
    }

    public function remove($id)
    {
        // TODO: Implement remove() method.
    }
    public function getByPatientId($patient_id)
    {
        return Prescription::where('patient_id', $patient_id)
            ->orderBy('id', 'desc')
            ->get();
    }
}