<?php

namespace App\Repositories;

use App\Appointment;
use App\TimeSlot;
use Carbon\Carbon;

class AppointmentRepository implements AppointmentRepositoryInterface
{

    public function getAllCurrent()
    {
        return Appointment::join('time_slots', 'appointments.time_slot_id', '=', 'time_slots.id')
        ->select('time_slots.*', 'appointments.*')
        ->orderBy('time_slots.slot_date', 'asc')
        ->orderBy('time_slots.slot_time')
        ->where('time_slots.slot_date', '>=' , date('Y-m-d'))
        ->get();
    }
    public function getAll()
    {
        return Appointment::join('time_slots', 'appointments.time_slot_id', '=', 'time_slots.id')
        ->select('time_slots.*', 'appointments.*')
        ->orderBy('time_slots.slot_date', 'desc')
        ->orderBy('time_slots.slot_time')
        ->get();
    }

    public function getAllAndDate($date)
    {
        return Appointment::with(['timeSlot', 'patient'])
            ->join('time_slots', 'appointments.time_slot_id', '=', 'time_slots.id')
            ->select('time_slots.*', 'appointments.*')
            ->whereDate('time_slots.slot_date', $date)
            ->orderBy('time_slots.slot_date', 'desc')
            ->orderBy('time_slots.slot_time')
            ->get();
    }

    public function getByUserIdAndDate($userId, $date)
    {
        return Appointment::with(['timeSlot', 'patient'])
            ->join('time_slots', 'appointments.time_slot_id', '=', 'time_slots.id')
            ->select('time_slots.*', 'appointments.*')
            ->where('appointments.created_by', $userId)
            ->whereDate('time_slots.slot_date', $date)
            ->orderBy('time_slots.slot_date', 'desc')
            ->orderBy('time_slots.slot_time')
            ->get();
    }

    public function getByUserId($userId)
    {
        return Appointment::join('time_slots', 'appointments.time_slot_id', '=', 'time_slots.id')
            ->select('time_slots.*', 'appointments.*')
            ->where('appointments.created_by', $userId)
            ->orderBy('time_slots.slot_date', 'asc')
            ->orderBy('time_slots.slot_time')
            ->get();
    }

    public function getById($id)
    {
        return Appointment::find($id);
    }

    public function add($appointment)
    {
        $model = new Appointment;

        $model->patient_id = $appointment['patient_id'];
        $model->time_slot_id = $appointment['schedule_time'];
        $model->requirements = $appointment['requirements'];
        $model->created_by = $appointment['created_by'];
        $model->status = $appointment['status'];

        $model->save();

        return $model;
    }

    public function update($id, $appointmentUpdate)
    {
        $model = Appointment::find($id);

        $model->time_slot_id = $appointmentUpdate['schedule_time'];
        $model->requirements = $appointmentUpdate['requirements'];

        $model->save();

        return $model;
    }

    public function updateStatus($id, $statusUpdate)
    {
        $model = Appointment::find($id);

        $model->status = $statusUpdate['status'];

        $model->save();

        return $model;

    }
    public function remove($id)
    {
        // TODO: Implement remove() method.
    }

    public function getAppointmentsToday()
    {
        $slotsToday = TimeSlot::whereDate('slot_date', Carbon::today())->pluck('id');

        return Appointment::with('timeSlot')->whereIn('time_slot_id', $slotsToday)->get();
    }

    public function countAppointmentsToday()
    {
        return Appointment::whereDate('schedule_date', Carbon::today())->count();
    }

    public function countAppointmentsThisWeek()
    {
        return Appointment::whereBetween('schedule_date', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();
    }

    public function countAppointmentsThisMonth()
    {
        return Appointment::whereMonth('schedule_date', Carbon::today()->month)->count();
    }

    public function getEvents($start, $end)
    {
        return Appointment::whereBetween('time_slot_id', [$start, $end])->get();
    }

    public function filterByStatus($status)
    {
        if ($status == 'Today') {
            $now = Carbon::now()->format('Y-m-d');
            return Appointment::join('time_slots', 'appointments.time_slot_id', '=', 'time_slots.id')
            ->select('time_slots.*', 'appointments.*')
            ->orderBy('time_slots.slot_date', 'asc')
            ->orderBy('time_slots.slot_time')
            ->where('time_slots.slot_date', $now)
            ->get();
        } else {
            return Appointment::join('time_slots', 'appointments.time_slot_id', '=', 'time_slots.id')
            ->select('time_slots.*', 'appointments.*')
            ->orderBy('time_slots.slot_date', 'asc')
            ->orderBy('time_slots.slot_time')
            ->where('appointments.status', $status)
            ->get();
        }
    }
}