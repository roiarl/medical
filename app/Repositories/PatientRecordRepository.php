<?php

namespace App\Repositories;

use App\Patient;
use Carbon\Carbon;
use App\PatientRecord;

class PatientRecordRepository implements PatientRecordRepositoryInterface
{
    public function getAll()
    {
        // TODO: Implement getAll() method.
    }

    public function getById($id)
    {
        return PatientRecord::find($id);
    }

    public function add($patientRecord)
    {
        $model = new PatientRecord;
        
        $model->patient_id = $patientRecord['patient_id'];
        $model->date = $patientRecord['date'];
        $model->reason = $patientRecord['reason'];
        $model->notes = $patientRecord['notes'];

        $model->save();

        return $model;

    }

    public function update($id, $patientRecordUpdate)
    {
        $model = PatientRecord::find($id);

        $model->date = $patientRecordUpdate['date'];
        $model->reason = $patientRecordUpdate['reason'];
        $model->notes = $patientRecordUpdate['notes'];

        $model->save();
    }

    public function remove($id)
    {
        // TODO: Implement remove() method.
    }
    public function getByPatientId($patient_id)
    {
        return PatientRecord::where('patient_id', $patient_id)
            ->orderBy('id', 'desc')
            ->get();
    }
}