<?php

namespace App\Repositories;

interface ConsentRepositoryInterface
{
    public function getAll();

    public function getById($id);

    public function add($consent);

    public function update($id, $consentUpdate);

    public function remove($id);
}