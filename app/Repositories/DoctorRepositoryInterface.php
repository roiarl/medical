<?php

namespace App\Repositories;

interface DoctorRepositoryInterface
{
    public function getAll();

    public function getById($id);

    public function getByEmail($email);

    public function add($doctor);

    public function update($id);
    public function updateByEmail($email, $doctorUpdate);

    public function remove($id);
}