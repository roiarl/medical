<?php

namespace App\Events;

use App\Appointment;
use Illuminate\Queue\SerializesModels;

class AppointmentCreated
{
    use SerializesModels;

    public $appointment;

    /**
     * AppointmentCreated constructor.
     *
     * @param \App\Appointment $appointment
     */
    public function __construct(Appointment $appointment)
    {
        $this->appointment = $appointment;
    }
}