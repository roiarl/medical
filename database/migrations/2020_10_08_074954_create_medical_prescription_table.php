<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedicalPrescriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('prescriptions')) {
            Schema::create('prescriptions', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('patient_id')->unsigned();
                $table->string('doctor_name');
                $table->integer('license_no')->unsigned();
                $table->date('date')->nullable();
                $table->text('rx')->nullable();
                $table->text('sig')->nullable();
                $table->string('signature')->nullable();
                $table->text('dispensing_instructions')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prescription');
    }
}
