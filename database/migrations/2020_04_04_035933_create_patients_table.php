<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('patients')) {
            Schema::create('patients', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('first_name');
                $table->string('last_name');
                $table->string('image')->nullable();
                $table->string('middle_name')->nullable();
                $table->integer('age')->nullable();
                $table->text('address')->nullable();
                $table->string('email')->nullable();
                $table->string('contact_number')->nullable();
                $table->date('birthday')->nullable();
                $table->enum('gender', ['Male', 'Female'])->default('Male');
                $table->string('occupation')->nullable();
                $table->string('referred_by')->nullable();
                $table->integer('medical_history')->nullable();
                $table->string('medical_history_others')->nullable();
                $table->string('allergies')->nullable();
                $table->string('insurance')->nullable();
                $table->string('company_name')->nullable();
                $table->text('company_address')->nullable();
                $table->date('last_consultation')->nullable();
                $table->text('last_consultation_reason')->nullable();
                $table->date('last_hospitalization')->nullable();
                $table->text('last_hospitalization_reason')->nullable();
                $table->string('maintain_med_name')->nullable();
                $table->string('maintain_frequency')->nullable();
                $table->string('med_name')->nullable();
                $table->string('med_take_date')->nullable();
                $table->string('med_take_time')->nullable();
                $table->integer('created_by')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients');
    }
}
