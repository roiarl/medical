<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('appointments')) {
            Schema::create('appointments', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('patient_id')->unsigned();
                $table->integer('time_slot_id')->nullable();
                $table->text('complaints')->nullable();
                $table->text('requirements')->nullable();
                $table->text('remarks')->nullable();
                $table->text('status')->nullable();
                $table->dateTime('paid_at')->nullable();
                $table->integer('created_by')->nullable();

                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
