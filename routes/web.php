<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
})->middleware('guest');

Route::get('/refresh_captcha', 'Auth\RegisterController@refreshCaptcha')->name('refresh');

Route::get('/passwordExpiration','Auth\PwdExpirationController@showPasswordExpirationForm');
Route::post('/passwordExpiration','Auth\PwdExpirationController@postPasswordExpiration')->name('passwordExpiration');

Route::get('/changePassword','HomeController@showChangePasswordForm');
Route::post('/changePassword','HomeController@changePassword')->name('changePassword');

Auth::routes(['verify' => true]);

Route::middleware(['auth' => 'verified'])->group(function () {

    Route::prefix('appointments')->group(function () {

        Route::get('/', 'AppointmentController@create')->name('appointments.create');
        Route::get('index', 'AppointmentController@index')->name('appointment.index');
        Route::get('create', 'AppointmentController@create')->name('appointments.create');
        Route::post('store', 'AppointmentController@store');
        Route::post('storeExisting', 'AppointmentController@storeExisting');
        Route::get('edit/{id}', 'AppointmentController@edit')->name('appointments.edit');
        Route::post('update/{id}', 'AppointmentController@update');
        Route::get('show/{id}', 'AppointmentController@show')->name('appointments.show');
        Route::get('screening/{id}', 'AppointmentController@screening')->name('appointments.screening');
        Route::post('status/{id}', 'AppointmentController@status')->name('appointments.status');

        Route::get('calendar', 'AppointmentController@calendar')->name('appointments.calendar');

        Route::get('eventList', 'AppointmentController@eventList')->name('appointments.eventList');
        Route::get('slots', 'AppointmentController@slots');
        Route::get('byDate', 'AppointmentController@appointmentsByDate');

        Route::get('showscreen/{id}', 'AppointmentController@showscreen')->name('appointments.showscreen');
        Route::get('showconsent/{id}', 'AppointmentController@showconsent')->name('appointments.showconsent');

        Route::get('showprescription/{id}/{referer}', 'PrescriptionController@showprescription')->name('appointments.showprescription');
        Route::get('prescriptionform/{id}/{referer}', 'PrescriptionController@prescriptionform')->name('appointments.prescriptionform');
        Route::post('prescriptionupdate/{id}/{referer}', 'PrescriptionController@prescriptionupdate');
        Route::get('createprescription/{referer}', 'PrescriptionController@createprescription')->name('appointments.createprescription');

        Route::get('patientrecord/{id}/{referer}', 'PatientRecordController@patientrecord')->name('appointments.patientrecord');
        Route::post('createpatientrecord/{referer}', 'PatientRecordController@createpatientrecord')->name('appointments.createpatientrecord');
        Route::get('showpatientrecord/{id}/{referer}', 'PatientRecordController@showpatientrecord')->name('appointments.showpatientrecord');
        Route::post('patientrecordupdate/{id}/{referer}', 'PatientRecordController@patientrecordupdate');

        Route::get('patienthistory/{id}/{referer}', 'PatientController@patienthistory')->name('appointments.patienthistory');
        Route::get('download/{id}', 'PatientController@download');
        
        Route::get('screeningform', function () {
            return view('appointments.screeningform');
        });

        Route::get('consentform', function () {
            return view('appointments.consentform');
        });

        Route::get('patients', 'PatientController@index')->name('appointments.patients');
        Route::get('search', 'PatientController@search')->name('appointments.search');

        Route::get('patientlist', 'AppointmentController@patientList')->name('appointments.patientlist');
        Route::get('patientdetail/{id}', 'AppointmentController@patientdetails')->name('appointments.patientdetail');

        Route::get('superuser', 'AppointmentController@superuser')->name('appointments.superuser');
        Route::post('updateUserRole', 'AppointmentController@updateUserRole');

    });
});
